# Setup

## Dependencies

- Jansson (JSON Library)
http://www.digip.org/jansson/

- SFML 2.0 (Graphic Library)
https://www.sfml-dev.org/download.php

## Run

    cmake cmake-build-debug
    cd cmake-build-debug/Sources/Game
    ./Bremerhaven
    
### Play

Q,Z,S,D - Move

left mouse - Fire

R - Recharge
