//
// Created by Pierre Roy on 30/03/18.
//

#ifndef BASEENGINE_COLLISIONMANAGER_HPP
#define BASEENGINE_COLLISIONMANAGER_HPP

#include "../../Game/Utils/types.h"

// Return result of Object to Object collision detection
class CollisionManager
{
public:
    typedef std::vector<DrawableItem> ItemVector;
    typedef std::reference_wrapper<const MapSystem::Tile> ConstTileRef;
    typedef std::vector<ConstTileRef> ConstTileRefVector;

public:
    CollisionManager() = default;

    ConstTileRefVector creatureToWalls(const EntitySystem::Creature&, const MapSystem::TileMap*);
    std::shared_ptr<MapSystem::Door> creatureToDoors(const EntitySystem::Creature&,
                                                   const std::vector<MapSystem::Door>&);
    ItemVector playerToItems(const EntitySystem::Player&, const ItemVector&);
    std::vector<EntitySystem::Creature*> creatureToCreatures(const EntitySystem::Creature&, CreaturePool*);
    ConstTileRefVector projectileOnMap(const Projectile&, const MapSystem::TileMap&);
    int creatureToProjectile(const EntitySystem::Creature&, ProjectilePool*);
};

#endif //BASEENGINE_COLLISIONMANAGER_HPP
