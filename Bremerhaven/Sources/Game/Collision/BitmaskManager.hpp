//
// Created by Pierre Roy on 04/05/18.
//

#ifndef BREMERHAVEN_BITMASKMANAGER_HPP
#define BREMERHAVEN_BITMASKMANAGER_HPP

#include <SFML/Graphics/Texture.hpp>
#include <map>

namespace Collision
{
    class BitmaskManager
    {
    private:
        std::map<const sf::Texture*, sf::Uint8*> Bitmasks;

    public:
        ~BitmaskManager();

        sf::Uint8 GetPixel(const sf::Uint8* mask, const sf::Texture* tex,
                            unsigned int x, unsigned int y);
        sf::Uint8* GetMask(const sf::Texture* tex);
        sf::Uint8* CreateMask(const sf::Texture* tex, const sf::Image& img);
    };
}

#endif //BREMERHAVEN_BITMASKMANAGER_HPP
