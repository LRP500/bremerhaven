//
// Created by Pierre Roy on 16/05/18.
//

#ifndef BREMERHAVEN_RECTANGLECOLLIDER_HPP
#define BREMERHAVEN_RECTANGLECOLLIDER_HPP

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

// base class for collider
// defines active state
class Collider
{
private:
    bool m_active;

public:
    Collider() : m_active(true) {}
    virtual ~Collider() = default;

    bool isActive() const { return m_active; }
    void setActive(bool state) { if (m_active != state) m_active = state; }
};

// Rectangle Collider attaching to object
// rect defines collider size
// rect position relative to collidee. position (0,0) is collidee origin
class RectangleCollider : public Collider, public sf::RectangleShape
{
public:
    explicit RectangleCollider();

    ~RectangleCollider() override = default;
};

#endif //BREMERHAVEN_RECTANGLECOLLIDER_HPP
