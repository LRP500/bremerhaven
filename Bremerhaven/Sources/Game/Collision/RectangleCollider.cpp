//
// Created by Pierre Roy on 16/05/18.
//

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

#include "RectangleCollider.hpp"

RectangleCollider::RectangleCollider()
{
    setFillColor(sf::Color::Transparent);
    setOutlineColor(sf::Color::Red);
    setOutlineThickness(1);
}