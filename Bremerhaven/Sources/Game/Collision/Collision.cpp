//
// Created by Pierre Roy on 06/04/18.
//

#include <map>

#include <SFML/Graphics/RectangleShape.hpp>

#include "Collision.h"

//Collision::BitmaskManager Bitmasks;

namespace Collision
{
    BitmaskManager& GetBitmaskManager()
    {
        static Collision::BitmaskManager Bitmasks;
        return Bitmasks;
    }

    bool PixelPerfectTest(const sf::Sprite& Object1, const sf::Sprite& Object2, sf::Uint8 AlphaLimit)
    {
        sf::FloatRect Intersection;
        if (Object1.getGlobalBounds().intersects(Object2.getGlobalBounds(), Intersection))
        {
            auto& bmgr = GetBitmaskManager();
            sf::IntRect O1SubRect = Object1.getTextureRect();
            sf::IntRect O2SubRect = Object2.getTextureRect();
            sf::Uint8* mask1 = bmgr.GetMask(Object1.getTexture());
            sf::Uint8* mask2 = bmgr.GetMask(Object2.getTexture());

            // Loop through our pixels
            for (int i = Intersection.left; i < Intersection.left+Intersection.width; i++)
            {
                for (int j = Intersection.top; j < Intersection.top+Intersection.height; j++)
                {
                    sf::Vector2f o1v = Object1.getInverseTransform().transformPoint(i, j);
                    sf::Vector2f o2v = Object2.getInverseTransform().transformPoint(i, j);

                    // Make sure pixels fall within the sprite's subrect
                    if (o1v.x > 0 && o1v.y > 0 && o2v.x > 0 && o2v.y > 0 &&
                        o1v.x < O1SubRect.width && o1v.y < O1SubRect.height &&
                        o2v.x < O2SubRect.width && o2v.y < O2SubRect.height)
                    {
                        if (bmgr.GetPixel(mask1, Object1.getTexture(),
                                          (int)(o1v.x) + O1SubRect.left,
                                          (int)(o1v.y) + O1SubRect.top) > AlphaLimit &&
                            bmgr.GetPixel(mask2, Object2.getTexture(),
                                          (int)(o2v.x) + O2SubRect.left,
                                          (int)(o2v.y) + O2SubRect.top) > AlphaLimit)
                            return true;
                    }
                }
            }
        }
        return false;
    }

    bool CreateTextureAndBitmask(sf::Texture& LoadInto, const std::string& Filename)
    {
        sf::Image img;
        if (!img.loadFromFile(Filename)) return false;
        if (!LoadInto.loadFromImage(img)) return false;
        GetBitmaskManager().CreateMask(&LoadInto, img);
        return true;
    }

    sf::Vector2f GetSpriteCenter(const sf::Sprite& Object)
    {
        sf::FloatRect AABB = Object.getGlobalBounds();
        return sf::Vector2f(AABB.left + AABB.width / 2.f, AABB.top + AABB.height / 2.f);
    }

    sf::Vector2f GetSpriteSize(const sf::Sprite& Object)
    {
        sf::IntRect OriginalSize = Object.getTextureRect();
        sf::Vector2f Scale = Object.getScale();
        return sf::Vector2f(OriginalSize.width * Scale.x, OriginalSize.height * Scale.y);
    }

    bool CircleTest(const sf::Sprite& Object1, const sf::Sprite& Object2)
    {
        sf::Vector2f Obj1Size = GetSpriteSize(Object1);
        sf::Vector2f Obj2Size = GetSpriteSize(Object2);
        float Radius1 = (Obj1Size.x + Obj1Size.y) / 4;
        float Radius2 = (Obj2Size.x + Obj2Size.y) / 4;
        sf::Vector2f Distance = GetSpriteCenter(Object1) - GetSpriteCenter(Object2);
        return (Distance.x * Distance.x + Distance.y * Distance.y <= (Radius1 + Radius2) * (Radius1 + Radius2));
    }
}