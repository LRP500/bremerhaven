//
// Created by Pierre Roy on 04/05/18.
//

#ifndef BREMERHAVEN_ORIENTEDBOUNDINGBOX_HPP
#define BREMERHAVEN_ORIENTEDBOUNDINGBOX_HPP

#include <SFML/Graphics/Sprite.hpp>

// Create boundingbox from Sprite, Rectangle or IntRect collider
class OrientedBoundingBox
{
public:
    sf::Vector2f Points[4];

public:
    // Calculate the four points of the OBB from a transformed (scaled, rotated...) sprite
    explicit OrientedBoundingBox(const sf::Sprite& Object);
    explicit OrientedBoundingBox(const sf::RectangleShape& Object);

    // Project all four points of the OBB onto the given axis
    // return the dotproducts of the two outermost points
    void ProjectOntoAxis (const sf::Vector2f& Axis, float& Min, float& Max);
};


#endif //BREMERHAVEN_ORIENTEDBOUNDINGBOX_HPP
