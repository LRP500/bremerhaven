//
// Created by Pierre Roy on 30/03/18.
//

#include "CollisionManager.hpp"
#include "Collision.h"
#include "../Player/Player.hpp"
#include "../Item/Weapon/Projectile.hpp"
#include "../../Graphics/Map/Door.hpp"
#include "../../Graphics/Map/Tile.hpp"
#include "../../Graphics/Map/TileMap.hpp"
#include "../../Graphics/GUI/Anchor.hpp"
#include "../../Graphics/Drawable/DrawableItem.hpp"
#include "../../Graphics/Raycast/Daisy.hpp"
#include "../Item/Weapon/Projectile.hpp"
#include "../FSM/States/GSGameplay.hpp"

using namespace MapSystem;
using EntitySystem::Player;
using EntitySystem::Creature;

CollisionManager::ConstTileRefVector
CollisionManager::creatureToWalls(const Creature& player, const MapSystem::TileMap* map)
{
    // get overlapped tiles
    ConstTileRefVector corners;
    using namespace Anchor::Global;
    corners.emplace_back(map->getTileFromWorldPos(getTopLeft(player.getCollider())));
    corners.emplace_back(map->getTileFromWorldPos(getTopRight(player.getCollider())));
    corners.emplace_back(map->getTileFromWorldPos(getBottomLeft(player.getCollider())));
    corners.emplace_back(map->getTileFromWorldPos(getBottomRight(player.getCollider())));

    ConstTileRefVector collidedTiles;
    for (auto &corner : corners)
    {
        if (corner.get().getType() == TileType::Wall)
            collidedTiles.emplace_back(corner);
    }
    return collidedTiles;
}

CollisionManager::ItemVector
CollisionManager::playerToItems(const Player& player, const ItemVector& items)
{
    ItemVector collided;
    for (auto&& item : items)
    {
        if (player.getCollider().getGlobalBounds().intersects(item.getCollider().getGlobalBounds()))
        {
            collided.emplace_back(item);
        }
    }
    return collided;
}

std::shared_ptr<MapSystem::Door>
CollisionManager::creatureToDoors(const Creature& player, const std::vector<Door>& doors)
{
    for (const auto& door : doors)
    {
        if (!door.getData()->closed) continue;
        if (player.getCollider().getGlobalBounds().intersects(door.getCollider().getGlobalBounds()))
            return std::shared_ptr<MapSystem::Door>(new MapSystem::Door(door));
    }
    return std::shared_ptr<MapSystem::Door>();
}

CollisionManager::ConstTileRefVector
CollisionManager::projectileOnMap(const Projectile& projectile, const TileMap& map)
{
    // get overlapped tiles
    ConstTileRefVector corners;
    using namespace Anchor::Global;
    corners.emplace_back(map.getTileFromWorldPos(getTopLeft(projectile.getCollider())));
    corners.emplace_back(map.getTileFromWorldPos(getTopRight(projectile.getCollider())));
    corners.emplace_back(map.getTileFromWorldPos(getBottomLeft(projectile.getCollider())));
    corners.emplace_back(map.getTileFromWorldPos(getBottomRight(projectile.getCollider())));

    ConstTileRefVector collidedTiles;
    for (auto &corner : corners)
    {
        if (corner.get().getType() == TileType::Wall)
        {
            collidedTiles.emplace_back(corner);
        }
        else
        {
            // check if colliding with closed door
            auto door = map.getDoor(corner.get().getPosition());
            if (door != map.getDoors().cend() && door->getData()->closed)
            {
                collidedTiles.emplace_back(corner);
            }
        }
    }
    return collidedTiles;
}

std::vector<Creature*>
CollisionManager::creatureToCreatures(const Creature& creature, CreaturePool* pool)
{
    // pool of collided objects
    std::vector<Creature*> collided;

    // collisions with enemies
    for (auto& enemy : *pool)
    {
        // do not check inactive colliders
        if (!enemy.getCollider().isActive()) continue;

        // do not check self
        if (&enemy == &creature) continue;

        // run collision
        if (creature.getCollider().getGlobalBounds().intersects(enemy.getCollider().getGlobalBounds()))
            collided.emplace_back(&enemy);
    }

    // collisions with player
    if (creature.getId() != "player")
    {
        Creature* player = const_cast<Creature&>(creature).getWorld()->getPlayer().get();
        if (creature.getCollider().getGlobalBounds().intersects(player->getCollider().getGlobalBounds()))
            collided.emplace_back(player);
    }

    return collided;
}

int CollisionManager::creatureToProjectile(const Creature& creature, ProjectilePool* projectiles)
{
    int damagetaken = 0;

    // no damage taken if dashing or recovering
    if (!creature.getCollider().isActive())
        return damagetaken;

    for (auto it = (*projectiles).begin(); it != (*projectiles).end();)
    {
        if (it->getCollider().getGlobalBounds().intersects(creature.getCollider().getGlobalBounds()))
        {
            damagetaken += it->getDamage();
            projectiles->erase(it);
        }
        else ++it;
    }

    return damagetaken;
}
