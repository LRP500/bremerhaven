//
// Created by Pierre Roy on 21/11/17.
//

#include "FSM/GSManager.hpp"
#include "FSM/States/GSTitleMenu.hpp"
#include "../System/Logger/Logger.hpp"

int main()
{
    FSM::GSManager game;

    LOGGER_START(Logger::INFO, "")

    game.pushState(new FSM::GSTitleMenu(&game));
    game.run();

    LOGGER_STOP()

    return EXIT_SUCCESS;
}