//
// Created by Pierre Roy on 25/03/18.
//

#include <iostream>
#include "GSManager.hpp"
#include "../../Graphics/Utils/style.h"
#include "../../Graphics/Utils/FrameClock.hpp"
#include "../../Graphics/Utils/ClockHUD.hpp"
#include "../../Graphics/GUI/Menu.hpp"

const std::string FSM::GSManager::JsonDirectoryPath = "../../../JSON/";
const std::string FSM::GSManager::AssetDirectoryPath = "../../../Assets";

FSM::GSManager::GSManager()
        : m_store(std::make_shared<SharedStore>()),
          m_settings()
{
    // read and parse config file
    // abort program if file not found
    if (!m_settings.loadFromFile("../../../Storage/bremerhaven.config")) return ;

    // fetch parsed settings
    auto settings = m_settings.getData();

    // define window mode & style
    sf::VideoMode mode(settings.windowedResolution.x, settings.windowedResolution.y);
    auto style = settings.fullscreenMode ? sf::Style::Fullscreen : (sf::Style::Titlebar | sf::Style::Close);

    // create window
    m_window.create(mode, settings.windowTitle, style);

    // configure window
    m_window.setFramerateLimit(settings.framerateLimit);
    m_window.setVerticalSyncEnabled(settings.verticalSyncEnabled);
    m_window.setMouseCursorGrabbed(settings.mouseCursorGrabbed);
    m_window.setMouseCursorVisible(settings.mouseCursorVisible);

    // load assets
    loadTextures();
    loadFonts();
    loadSounds();
}

FSM::GSManager::~GSManager()
{
    while (!m_states.empty())
    {
        popState();
    }
}

void FSM::GSManager::run()
{
    Utils::FrameClock frameClock;
    Utils::ClockHUD hud(frameClock, *m_fonts.getRef(GUI::DefaultFont));

    frameClock.setSampleDepth(100);

    sf::Vector2f pos = sf::Vector2f(m_window.getSize());
    m_hudView.setSize(pos);
    m_hudView.setCenter(pos / 2.0f);

    while (m_window.isOpen())
    {
        frameClock.beginFrame();
        sf::Time elapsed = frameClock.getLastFrameTime();

        if (peekState() == nullptr)
            continue;

        peekState()->handleInput();
        peekState()->update(elapsed);
        m_window.clear(Style::CustomColor::DARK_GREY);
        peekState()->render(elapsed);

        m_window.setView(m_hudView);
        m_window.draw(hud);
        m_window.display();

        frameClock.endFrame();
    }
}

void FSM::GSManager::pushState(GameState *state)
{
    m_states.push(state);
}

void FSM::GSManager::changeState(GameState *state)
{
    if (!m_states.empty())
    {
        popState();
    }
    pushState(state);
}

void FSM::GSManager::popState()
{
    delete m_states.top();
    m_states.pop();
}

FSM::GameState* FSM::GSManager::peekState()
{
    return (m_states.empty()) ? nullptr : m_states.top();
}

sf::RenderWindow& FSM::GSManager::getWindow()
{
    return const_cast<sf::RenderWindow&>(static_cast<const GSManager*>(this)->getWindow());
}

const sf::RenderWindow &FSM::GSManager::getWindow() const
{
    return m_window;
}

void FSM::GSManager::goToState(StateType state)
{
    while (!m_states.empty() && m_states.top()->getType() != state)
    {
        popState();
    }
}
