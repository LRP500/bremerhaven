//
// Created by Pierre Roy on 25/03/18.
//

#ifndef BASEENGINE_GAME_HPP
#define BASEENGINE_GAME_HPP

#include <stack>

#include <SFML/Graphics/RenderWindow.hpp>

#include "GameState.hpp"
#include "Shared/SharedStore.hpp"
#include "../../Game/Utils/types.h"
#include "../../Graphics/Resources/ResourceHolder.hpp"
#include "../../System/Settings/ConfigFileParser.hpp"

namespace FiniteStateMachine
{
    // Game game class
    // Manage states and hold variables common to all of them
    class GSManager
    {
    public:
        static const std::string JsonDirectoryPath; // json files root folder path
        static const std::string AssetDirectoryPath; // assets root folder path

    public:
        typedef std::stack<GameState*> GameStateStack;

    private:
        System::ConfigFileParser m_settings; // data parsed from config file
        sf::View m_hudView; // debug hud view
        sf::RenderWindow m_window; // main window instance
        GameStateStack m_states; // states
        std::shared_ptr<SharedStore> m_store; // holds persistent data

    private:
        ResourceHolder<sf::Font> m_fonts;
        ResourceHolder<sf::Texture> m_textures;
        ResourceHolder<sf::SoundBuffer> m_sounds;

    public:
        GSManager();
        ~GSManager();

        void run(); // main game loop

        const sf::RenderWindow& getWindow() const;
        sf::RenderWindow& getWindow();

        const ResourceHolder<sf::Font>& getFonts() {return m_fonts; }
        const ResourceHolder<sf::Texture>& getTextures() { return m_textures; }
        const std::shared_ptr<SharedStore>& getStore() const { return m_store; }
        const System::ConfigFileParser& getSettings() const { return m_settings; }

    public: // FSM machine methods
        void pushState(GameState* state);
        void changeState(GameState* state);
        void popState();
        void goToState(StateType);
        GameState* peekState();

    private:
        void loadTextures();
        void loadFonts();
        void loadSounds();
    };
}

#endif //BASEENGINE_GAME_HPP
