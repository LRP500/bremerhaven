//
// Created by Pierre Roy on 26/03/18.
//

#ifndef BASEENGINE_GSTITLEMENU_HPP
#define BASEENGINE_GSTITLEMENU_HPP

#include "../fsmfwd.h"

#include "../GameState.hpp"
#include "../../../Graphics/GUI/Menu.hpp"

namespace FiniteStateMachine
{
    class GSTitleMenu : public GameState
    {
    private:
        sf::View m_view;
        GUI::Menu m_menu;

    public:
        explicit GSTitleMenu(GSManager*);

        void handleInput() override;
        void update(const sf::Time dt) override;
        void render(const sf::Time dt) override;

    private:
        void newGame();
        void loadGame();
    };
}
#endif //BASEENGINE_GSTITLEMENU_HPP
