//
// Created by Pierre Roy on 25/03/18.
//

#ifndef BASEENGINE_GAMESTATEGAMEPLAY_HPP
#define BASEENGINE_GAMESTATEGAMEPLAY_HPP

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>

#include "../GameState.hpp"
#include "../../Atlas/Atlas.hpp"
#include "../../Player/Player.hpp"

namespace FiniteStateMachine
{
    class GSGameplay : public GameState
    {
    public:
        static const unsigned int DefaultDepth = 1; // initial level (DEBUG)
        static const unsigned int EndOfAmusement = 10; // maximum depth
        static const unsigned int GuiPaddingPercentage = 4; // in percentage
        static const char GuiAlpha = 50;
        static constexpr float GuiScale = 0.4f;

    private:
        sf::View m_gameView; // player camera
        sf::View m_guiView; // in-game gui view
        SharedMap m_map; // textured tile map
        EntitySystem::Atlas m_atlas; // entity atlas
        SharedPlayer m_player; // da cube boi

        ProjectilePool m_playerProjectiles;
        ProjectilePool m_enemyProjectiles;
        CreaturePool m_enemies;

        sf::Vector2f m_guiPadding;
        sf::Sprite m_guiKey;
        sf::Sprite m_guiKeyArmory;
        sf::Sprite m_guiKeyInfirmary;
        sf::Text m_guiClip;
        GUI::ArmorGUIPanel m_guiArmor;

    public:
        explicit GSGameplay(GSManager*);

    public:
        void handleInput() override;
        void update(sf::Time dt) override;
        void render(sf::Time dt) override;

        SharedPlayer getPlayer() { return m_player; }
        const EntitySystem::Atlas& getAtlas() const { return m_atlas; }
        MapSystem::TileMap* getMap() { return m_map.get(); }
        CreaturePool* getEnemyPool() { return &m_enemies; }
        ProjectilePool* getPlayerProjectilePool() { return &m_playerProjectiles; }
        ProjectilePool* getEnemyProjectilePool() { return &m_enemyProjectiles; }
        GSManager* getManager() const { return m_game; }

    private:
        void initGUI();
        void initMap();
        void initPlayer();
        void initWeapons();
        void updateGUI();
        void generateEnemies();
        void loadEntities();
        void pickUpItems();
        void hasGameEnded();
        void nextLevel();
        void endGame();
    };
}

#endif //BASEENGINE_GAMESTATEGAMEPLAY_HPP
