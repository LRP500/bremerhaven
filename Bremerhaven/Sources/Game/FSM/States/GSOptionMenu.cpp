//
// Created by Pierre Roy on 03/04/18.
//

#include "GSOptionMenu.hpp"
#include "../GSManager.hpp"
#include "../../../Graphics/GUI/TextItem.hpp"

using namespace FiniteStateMachine;

GSOptionMenu::GSOptionMenu(GSManager *game)
        : GameState(game, StateType::OptionMenu),
          m_menu(game->getFonts().getRef(GUI::DefaultFont))
{
    // Set view size to screen size and center it
    sf::Vector2f pos = sf::Vector2f(m_game->getWindow().getSize());
    m_view.setSize(pos);
    m_view.setCenter(pos / 2.0f);

    // Initialize menu choices
    m_menu.addItem("Back");
    m_menu.setDimensions(sf::Vector2f(m_game->getWindow().getSize().x,
                                      m_game->getWindow().getSize().y / 3));
    m_menu.setPosition(sf::Vector2f(0, m_game->getWindow().getSize().y - m_menu.getDimensions().y));
    m_menu.setPadding(m_game->getSettings().getData().guiScreenSidePadding);
}

void GSOptionMenu::handleInput()
{
    sf::Event event {};
    while (m_game->getWindow().pollEvent(event))
    {
        switch (event.type)
        {
            case sf::Event::Closed:
                m_game->getWindow().close();
                break;
            case sf::Event::Resized:break;
            case sf::Event::LostFocus:break;
            case sf::Event::GainedFocus:break;
            case sf::Event::TextEntered:break;
            case sf::Event::KeyPressed:
                switch (event.key.code)
                {
                    case sf::Keyboard::Up:
                        m_menu.moveUp();
                        break;
                    case sf::Keyboard::Down:
                        m_menu.moveDown();
                        break;
                    case sf::Keyboard::Return:
                        switch (m_menu.getCurrentIndex())
                        {
                            case 0:
                                m_game->popState();
                                return; // crash if no return statement here
                            default:
                                break;
                        }
                }
            case sf::Event::KeyReleased:break;
            case sf::Event::MouseWheelMoved:break;
            case sf::Event::MouseWheelScrolled:break;
            case sf::Event::MouseButtonPressed:break;
            case sf::Event::MouseButtonReleased:break;
            case sf::Event::MouseMoved:break;
            case sf::Event::MouseEntered:break;
            case sf::Event::MouseLeft:break;
            case sf::Event::JoystickButtonPressed:break;
            case sf::Event::JoystickButtonReleased:break;
            case sf::Event::JoystickMoved:break;
            case sf::Event::JoystickConnected:break;
            case sf::Event::JoystickDisconnected:break;
            case sf::Event::TouchBegan:break;
            case sf::Event::TouchMoved:break;
            case sf::Event::TouchEnded:break;
            case sf::Event::SensorChanged:break;
            case sf::Event::Count:break;
        }
    }
}

void GSOptionMenu::update(const sf::Time dt)
{
    m_menu.update(dt);
}

void GSOptionMenu::render(const sf::Time dt)
{
    m_game->getWindow().setView(m_view); // set view
    m_menu.draw(m_game->getWindow()); // draw menu
}
