//
// Created by Pierre Roy on 09/04/18.
//

#ifndef BREMERHAVEN_GSESCAPEMENU_HPP
#define BREMERHAVEN_GSESCAPEMENU_HPP

#include "../GameState.hpp"
#include "../../../Graphics/GUI/Menu.hpp"

namespace  FiniteStateMachine
{
    class GSEscapeMenu : public GameState
    {
    private:
        sf::View m_view;
        GUI::Menu m_menu;

    public:
        explicit GSEscapeMenu(GSManager *);

        void handleInput() override;
        void update(const sf::Time dt) override;
        void render(const sf::Time dt) override;
    };
}

#endif //BREMERHAVEN_GSESCAPEMENU_HPP
