//
// Created by Pierre Roy on 03/04/18.
//

#ifndef BREMERHAVEN_GSNEWGAME_HPP
#define BREMERHAVEN_GSNEWGAME_HPP

#include "../GameState.hpp"
#include "../../Player/Player.hpp"
#include "../../../Graphics/GUI/Canvas.hpp"

namespace FiniteStateMachine
{
    class GSNewGame : public GameState
    {
    private:
        sf::View m_view;
        GUI::Canvas m_canvas;
        EntitySystem::PlayerClass m_playerClass;

    public:
        explicit GSNewGame(GSManager*);

        void handleInput() override;
        void update(const sf::Time dt) override;
        void render(const sf::Time dt) override;

        void startgame();
    };
}

#endif //BREMERHAVEN_GSNEWGAME_HPP
