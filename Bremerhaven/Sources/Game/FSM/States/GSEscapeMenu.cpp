//
// Created by Pierre Roy on 09/04/18.
//

#include "GSEscapeMenu.hpp"
#include "../GSManager.hpp"
#include "../../../Graphics/GUI/TextItem.hpp"

FSM::GSEscapeMenu::GSEscapeMenu(FSM::GSManager *game)
        : GameState(game, StateType::EscapeMenu),
          m_menu(game->getFonts().getRef(GUI::DefaultFont),
                 "escape_menu",
                 sf::Vector2f(m_game->getWindow().getSize().x, m_game->getWindow().getSize().y),
                 sf::Vector2f(0, 0),
                 m_game->getSettings().getData().guiScreenSidePadding / 2.0f)
{
    sf::Vector2f pos = sf::Vector2f(m_game->getWindow().getSize());
    m_view.setSize(pos);
    m_view.setCenter(pos / 2.0f);

    m_menu.addItem("Resume");
    m_menu.addItem("Options (coming soom)");
    m_menu.addItem("Main Menu");
    m_menu.getItem("Options (coming soom)")->lock();
}

void FSM::GSEscapeMenu::handleInput()
{
    sf::Event event{};


    while (m_game->getWindow().pollEvent(event))
    {
        switch (event.type)
        {
            case sf::Event::Closed:
                m_game->getWindow().close();
                return ;
            case sf::Event::Resized:break;
            case sf::Event::LostFocus:break;
            case sf::Event::GainedFocus:break;
            case sf::Event::TextEntered:break;
            case sf::Event::KeyPressed:
                switch (event.key.code)
                {
                    case sf::Keyboard::Escape: m_game->popState(); return ;
                    case sf::Keyboard::Up: m_menu.moveUp(); break;
                    case sf::Keyboard::Down: m_menu.moveDown(); break;
                    case sf::Keyboard::Return:
                        std::string item = m_menu.getCurrent()->getText()->getString();
                        if (!std::strncmp(item.c_str(), "Options", 7))
                        {
                            // navigate to a ingame options menu
                        }
                        else if (!std::strncmp(item.c_str(), "Resume", 6))
                        {
                            m_game->popState();
                            return ;
                        }
                        else if (!std::strncmp(item.c_str(), "Main Menu", 10))
                        {
                            m_game->goToState(StateType::TitleMenu);
                            return ;
                        }
                }
            case sf::Event::KeyReleased:break;
            case sf::Event::MouseWheelMoved:break;
            case sf::Event::MouseWheelScrolled:break;
            case sf::Event::MouseButtonPressed:break;
            case sf::Event::MouseButtonReleased:break;
            case sf::Event::MouseMoved:break;
            case sf::Event::MouseEntered:break;
            case sf::Event::MouseLeft:break;
            case sf::Event::JoystickButtonPressed:break;
            case sf::Event::JoystickButtonReleased:break;
            case sf::Event::JoystickMoved:break;
            case sf::Event::JoystickConnected:break;
            case sf::Event::JoystickDisconnected:break;
            case sf::Event::TouchBegan:break;
            case sf::Event::TouchMoved:break;
            case sf::Event::TouchEnded:break;
            case sf::Event::SensorChanged:break;
            case sf::Event::Count:break;
        }
    }
}

void FSM::GSEscapeMenu::update(const sf::Time dt)
{
    m_menu.update(dt);
}

void FSM::GSEscapeMenu::render(const sf::Time dt)
{
    m_game->getWindow().setView(m_view);
    m_menu.draw(m_game->getWindow());
}
