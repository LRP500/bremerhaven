//
// Created by Pierre Roy on 03/04/18.
//

#ifndef BREMERHAVEN_GSOPTIONMENU_HPP
#define BREMERHAVEN_GSOPTIONMENU_HPP

#include "../GameState.hpp"
#include "../../../Graphics/GUI/Menu.hpp"

namespace FiniteStateMachine
{
    class GSOptionMenu : public GameState
    {
    private:
        sf::View m_view;
        GUI::Menu m_menu;

    public:
        explicit GSOptionMenu(GSManager *);

        void handleInput() override;
        void update(const sf::Time dt) override;
        void render(const sf::Time dt) override;
    };
}

#endif //BREMERHAVEN_GSOPTIONMENU_HPP
