//
// Created by Pierre Roy on 26/03/18.
//

#include <iostream>
#include "GSTitleMenu.hpp"
#include "GSGameplay.hpp"
#include "GSOptionMenu.hpp"
#include "GSLoadGame.hpp"
#include "GSNewGame.hpp"
#include "../GSManager.hpp"
#include "../../../Graphics/GUI/TextItem.hpp"

using namespace FiniteStateMachine;

GSTitleMenu::GSTitleMenu(GSManager *game)
        : GameState(game, StateType::TitleMenu),
          m_menu(game->getFonts().getRef(GUI::DefaultFont))
{
    // Set view size to screen size and center it
    sf::Vector2f pos = sf::Vector2f(m_game->getWindow().getSize());
    m_view.setSize(pos);
    m_view.setCenter(pos / 2.0f);

    // Initialize menu choices
    m_menu.setDimensions(m_game->getWindow().getSize().x, m_game->getWindow().getSize().y);
    m_menu.setPadding(m_game->getSettings().getData().guiScreenSidePadding);
    m_menu.addItem("New Game");
    m_menu.addItem("Load Game (coming soon)");
    m_menu.addItem("Options (coming soon)");
    m_menu.addItem("Quit");

    m_menu.getItem("Load Game (coming soon)")->lock();
    m_menu.getItem("Options (coming soon)")->lock();
}

void GSTitleMenu::handleInput()
{
    sf::Event event {};
    while (m_game->getWindow().pollEvent(event))
    {
        switch (event.type)
        {
            case sf::Event::Closed: m_game->getWindow().close(); break;
            case sf::Event::Resized:break;
            case sf::Event::LostFocus:break;
            case sf::Event::GainedFocus:break;
            case sf::Event::TextEntered:break;
            case sf::Event::KeyPressed:
                switch (event.key.code)
                {
                    case sf::Keyboard::Up: m_menu.moveUp(); break;
                    case sf::Keyboard::Down: m_menu.moveDown(); break;
                    case sf::Keyboard::Return:
                        switch (m_menu.getCurrentIndex())
                        {
                            case 0: newGame();break;
                            case 1: loadGame(); break;
                            case 2: m_game->pushState(new GSOptionMenu(m_game)); break;
                            case 3: m_game->getWindow().close(); break;
                            default: break;
                        }
                }
            case sf::Event::KeyReleased:break;
            case sf::Event::MouseWheelMoved:break;
            case sf::Event::MouseWheelScrolled:break;
            case sf::Event::MouseButtonPressed:break;
            case sf::Event::MouseButtonReleased:break;
            case sf::Event::MouseMoved:break;
            case sf::Event::MouseEntered:break;
            case sf::Event::MouseLeft:break;
            case sf::Event::JoystickButtonPressed:break;
            case sf::Event::JoystickButtonReleased:break;
            case sf::Event::JoystickMoved:break;
            case sf::Event::JoystickConnected:break;
            case sf::Event::JoystickDisconnected:break;
            case sf::Event::TouchBegan:break;
            case sf::Event::TouchMoved:break;
            case sf::Event::TouchEnded:break;
            case sf::Event::SensorChanged:break;
            case sf::Event::Count:break;
        }
    }
}

void GSTitleMenu::update(const sf::Time dt)
{
    m_menu.update(dt);
}

void GSTitleMenu::render(const sf::Time dt)
{
    m_game->getWindow().setView(m_view); // set view

    m_menu.draw(m_game->getWindow()); // draw menu
}

void GSTitleMenu::loadGame()
{
    m_game->pushState(new GSLoadGame(m_game));
}

void GSTitleMenu::newGame()
{
    m_game->pushState(new GSNewGame(m_game));
}
