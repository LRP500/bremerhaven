//
// Created by Pierre Roy on 25/03/18.
//

#include <SFML/Graphics/CircleShape.hpp>
#include "GSGameplay.hpp"
#include "GSTitleMenu.hpp"
#include "GSLevelLoading.hpp"
#include "GSEscapeMenu.hpp"
#include "GSGameOverScreen.hpp"
#include "../GSManager.hpp"
#include "../../Item/Weapon/Weapon.hpp"
#include "../../Item/Weapon/Projectile.hpp"
#include "../../Item/Weapon/Behaviors/PistolFiringBehavior.hpp"
#include "../../Item/Weapon/Behaviors/ShotgunFiringBehavior.hpp"
#include "../../Creature/Creature.hpp"
#include "../../Collision/CollisionManager.hpp"
#include "../../AI/AttackTarget.hpp"
#include "../../AI/WanderRandomly.hpp"
#include "../../AI/WanderAndAttack.hpp"
#include "../../../Graphics/Raycast/Daisy.hpp"
#include "../../../Graphics/Map/Tile.hpp"
#include "../../../Graphics/Map/Door.hpp"
#include "../../../Graphics/Map/TileMap.hpp"
#include "../../../Graphics/Map/RoomData.hpp"
#include "../../../Graphics/Drawable/DrawableItem.hpp"
#include "../../../Graphics/GUI/Anchor.hpp"
#include "../../Item/Items/Medkit.hpp"

using MapSystem::TileSize;
using EntitySystem::Creature;
using EntitySystem::PlayerClass;
using namespace FiniteStateMachine;

GSGameplay::GSGameplay(GSManager* game)
        : GameState(game, StateType::Gameplay),
          m_map(std::make_shared<MapSystem::TileMap>(m_game->getWindow().getSize())),
          m_player(m_game->getStore()->getPlayer()),
          m_playerProjectiles(), m_enemyProjectiles(),
          m_guiPadding(sf::Vector2f()),
          m_guiKey(),
          m_guiClip()
{
    loadEntities();
    initWeapons();
    initMap();
    initPlayer();
    initGUI();
    generateEnemies();
}

void GSGameplay::handleInput()
{
    // poll events for Gui
    sf::Event event {};
    while (m_game->getWindow().pollEvent(event))
    {
        switch (event.type)
        {
            case sf::Event::Closed:
                m_game->getWindow().close();
                break;
            case sf::Event::Resized:break;
            case sf::Event::LostFocus:
                m_game->pushState(new GSEscapeMenu(m_game)); break;
            case sf::Event::GainedFocus:break;
            case sf::Event::TextEntered:break;
            case sf::Event::KeyPressed:
                switch (event.key.code)
                {
                    case sf::Keyboard::Escape:
                        m_game->pushState(new GSEscapeMenu(m_game));
                        break;
                }
            case sf::Event::KeyReleased:break;
            case sf::Event::MouseWheelMoved:break;
            case sf::Event::MouseWheelScrolled:break;
            case sf::Event::MouseButtonPressed:break;
            case sf::Event::MouseButtonReleased:break;
            case sf::Event::MouseMoved:break;
            case sf::Event::MouseEntered:break;
            case sf::Event::MouseLeft:break;
            case sf::Event::JoystickButtonPressed:break;
            case sf::Event::JoystickButtonReleased:break;
            case sf::Event::JoystickMoved:break;
            case sf::Event::JoystickConnected:break;
            case sf::Event::JoystickDisconnected:break;
            case sf::Event::TouchBegan:break;
            case sf::Event::TouchMoved:break;
            case sf::Event::TouchEnded:break;
            case sf::Event::SensorChanged:break;
            case sf::Event::Count:break;
        }
    }
}

void GSGameplay::update(const sf::Time dt)
{
    pickUpItems();

    m_player->handleInput(dt);
    m_player->update(dt);

    updateGUI();

    // camera follows player
    m_gameView.setCenter(m_player->getSprite().getPosition());

    // screen shake test
    // m_gameView.setCenter(m_gameView.getCenter() + sf::Vector2f(std::rand() % 2, std::rand() % 2));

    // update live projectiles
    for (auto& proj : m_playerProjectiles) proj.update(dt, *m_map);
    for (auto& proj : m_enemyProjectiles) proj.update(dt, *m_map);
    // then destroy projectiles marked expired
    std::function<bool (const Projectile&)> func = [](const auto& p) { return p.isExpired(); };
    m_playerProjectiles.erase(std::remove_if(
            m_playerProjectiles.begin(),
            m_playerProjectiles.end(), func), m_playerProjectiles.end());
    m_enemyProjectiles.erase(std::remove_if(
            m_enemyProjectiles.begin(),
            m_enemyProjectiles.end(), func), m_enemyProjectiles.end());

    // update enemies and destroy dead ones
    for (auto& enemy : m_enemies) if (enemy.isAlive()) enemy.update(dt);

    hasGameEnded();
}

void GSGameplay::render(const sf::Time dt)
{
    // set player camera view
    m_game->getWindow().setView(m_gameView);

    // draw all tiles
    m_game->getWindow().draw(*m_map);

    // draw player field of view polygons
//    m_player->drawFOV(m_game->getWindow());

    // draw map contained elements
    m_map->drawDoors(m_game->getWindow());
    m_map->drawItems(m_game->getWindow());

    // draw all projectiles
    for (const auto& p : m_playerProjectiles) m_game->getWindow().draw(p);
    for (const auto& p : m_enemyProjectiles) m_game->getWindow().draw(p);


    // draw creatures
    m_player->draw(m_game->getWindow());
    for (auto& enemy : m_enemies)
    {
        if (enemy.isAlive()) enemy.draw(m_game->getWindow());
    }

    // draw GUI elements
    m_game->getWindow().setView(m_guiView);
    m_game->getWindow().draw(m_guiKey);
    m_game->getWindow().draw(m_guiKeyInfirmary);
    m_game->getWindow().draw(m_guiKeyArmory);
    m_game->getWindow().draw(m_guiClip);
    m_guiArmor.draw(m_game->getWindow());
}

void GSGameplay::hasGameEnded()
{
    // create potential frustration by killing player before checking for exit **maniacal laughter**
    if (!m_player->getHealth()) endGame();

    // check if player reached exit
    // last action on state update to avoid manipulation of freed objects
    auto exitTile = m_map->getTile(m_map->getTilePos(MapSystem::TileAscii::DownStairs));
    if (m_player->getCollider().getGlobalBounds().intersects(exitTile.getGlobalBounds()))
    {
        nextLevel();
    }
}

void GSGameplay::nextLevel()
{
    if (m_game->getStore()->getLevelDepth() < EndOfAmusement)
    {
        m_game->getStore()->setLevelDepth(m_game->getStore()->getLevelDepth() + 1);
        m_game->changeState(new GSLevelLoading(m_game));
    }
    else { m_game->popState(); }
}

void GSGameplay::endGame()
{
    m_player->reset();
    m_game->pushState(new GSGameOverScreen(m_game));
}

void GSGameplay::pickUpItems()
{
    // check collision with nearby items
    static CollisionManager cmgr;
    std::vector<DrawableItem> collidedItems = cmgr.playerToItems(*m_player, m_map->getItems());

    // this is a very nasty way to keep history of items which where already colliding
    static std::vector<DrawableItem> colliding;
    for (int i = 0; i < colliding.size(); ++i)
    {
        // remove items that are not colliding anymore
        if (std::find(collidedItems.begin(),
                      collidedItems.end(),
                      colliding[i]) == collidedItems.end())
            colliding.erase(colliding.begin() + i);
    }

    // process collided items
    for (auto item : collidedItems)
    {
        // generic item loot not possible due to access to restricted access to shared ressources
        // items can't directly access the resources from atlas so action can't be processed internally

        // loot items
        if (item.getId().substr(0, std::string("item").size()) == "item")
        {
            m_player->getInventory().add(m_atlas.getSharedEntity<Item>(item.getId()), 1);
            m_map->removeDrop(item);
        }

        // consume medkits
        else if (item.getId().substr(0, std::string("medkit").size()) == "medkit")
        {
            // TODO time to dump DrawableItem and make base class Item DrawableAndTransformable
            if (m_atlas.getEntity<Medkit>(item.getId())->use(m_player.get()))
                m_map->removeDrop(item);
        }

        // equip armor
        else if (item.getId().substr(0, std::string("armor").size()) == "armor")
        {
            if (m_player->getArmor() < 3)
            {
                m_player->setArmor(m_player->getArmor() + 1);
                m_map->removeDrop(item);
            }
        }

        // equip weapons
        else if (item.getId().substr(0, std::string("weapon").size()) == "weapon")
        {
            if (item.getId() != m_player->getEquippedWeapon()->getId())
            {
                if (std::find(colliding.begin(), colliding.end(), item) != colliding.end())
                    continue;

                // drop current weapon && equip new one
                auto old = DrawableItem(
                        m_atlas.getEntity<Weapon>(m_player->getEquippedWeapon()->getId()),
                        m_game->getTextures().get(m_player->getEquippedWeapon()->getId()),
                        TileSize, item.getSprite().getPosition());

                m_player->equipWeapon(m_atlas.getSharedEntity<Weapon>(item.getId()));

                m_map->removeDrop(item); // destroy item picked up
                m_map->dropItem(old); // drop old weapon
                colliding.emplace_back(old); // add to collision history to avoid repicking dropped loot
            }
        }
    }
}

void GSGameplay::updateGUI()
{
    // update armor panel
    m_guiArmor.setArmor(m_player->getArmor());

    // key slot
    sf::Color alpha(255, 255, 255, GuiAlpha);
    m_guiKey.setColor((m_player->getInventory().count("item_key")) ? sf::Color::Black : alpha);
    m_guiKeyArmory.setColor((m_player->getInventory().count("item_key_armory")) ? sf::Color::Black : alpha);
    m_guiKeyInfirmary.setColor((m_player->getInventory().count("item_key_infirmary")) ? sf::Color::Black : alpha);

    // weapon panel
    auto weapon = m_player->getEquippedWeapon();
    if (weapon && m_guiClip.getFont())
    {
        m_guiClip.setFillColor(m_player->getEquippedWeapon()->isReloading() ?
                               sf::Color(0, 0, 0, GuiAlpha) : sf::Color::Black);
        m_guiClip.setString(std::to_string(weapon->getClip()) + "/" +
                            std::to_string(weapon->getClipCapacity()));
        auto lb = m_guiClip.getLocalBounds();
        m_guiClip.setPosition(m_game->getWindow().getSize().x - lb.width - m_guiPadding.x,
                              m_game->getWindow().getSize().y - lb.height - m_guiPadding.y);
    }
}

void GSGameplay::initGUI()
{
    // initialize views
    sf::Vector2f viewPos = sf::Vector2f(m_game->getWindow().getSize());
    m_guiView.setSize(viewPos);
    m_guiView.setCenter(viewPos / 2.0f);
    m_gameView.setSize(viewPos / m_game->getSettings().getData().zoomLevel);
    m_gameView.setCenter(viewPos / 2.0f);

    // set padding size depending on screen dimensions
    m_guiPadding = sf::Vector2f((m_game->getWindow().getSize().x * GuiPaddingPercentage / 100),
                                (m_game->getWindow().getSize().y * GuiPaddingPercentage / 100));

    // weapon clip
    m_guiClip = sf::Text("000/000", m_game->getFonts().get(GUI::DefaultFont), 80);
    m_guiClip.setStyle(sf::Text::Bold);
    m_guiClip.setFillColor(sf::Color::Black);
    m_guiClip.setOrigin(Anchor::Global::getTopLeft(m_guiClip));
    auto lb = m_guiClip.getLocalBounds();
    m_guiClip.setPosition(
            m_game->getWindow().getSize().x - lb.width - m_guiPadding.x,
            m_game->getWindow().getSize().y - lb.height - m_guiPadding.y);

    // exit key
    m_guiKey = sf::Sprite(m_game->getTextures().get("item_key"));
    m_guiKey.setScale(GuiScale, GuiScale);
    m_guiKey.setOrigin(Anchor::Global::getCenter(m_guiKey));
    m_guiKey.setColor(sf::Color(255, 255, 255, GuiAlpha));
    float spacing = m_guiPadding.y * 1.4f;
    auto gb = m_guiKey.getGlobalBounds();
    sf::Vector2f pos(m_game->getWindow().getSize().x - gb.width - m_guiPadding.x + (gb.width / 1.7f),
                     m_game->getWindow().getSize().y - (gb.height * 0.8f) - m_guiClip.getGlobalBounds().height - spacing);
    m_guiKey.setPosition(pos);

    // infirmary key
    m_guiKeyInfirmary = sf::Sprite(m_game->getTextures().get("item_key_infirmary"));
    m_guiKeyInfirmary.setScale(GuiScale, GuiScale);
    m_guiKeyInfirmary.setOrigin(Anchor::Global::getCenter(m_guiKeyInfirmary));
    m_guiKeyInfirmary.setColor(sf::Color(255, 255, 255, GuiAlpha));
    pos.x -= gb.width - (gb.width * 0.08);
    m_guiKeyInfirmary.setPosition(pos);

    // infirmary key
    m_guiKeyArmory = sf::Sprite(m_game->getTextures().get("item_key_armory"));
    m_guiKeyArmory.setScale(GuiScale, GuiScale);
    m_guiKeyArmory.setOrigin(Anchor::Global::getCenter(m_guiKeyArmory));
    m_guiKeyArmory.setColor(sf::Color(255, 255, 255, GuiAlpha));
    pos.x -= gb.width;
    m_guiKeyArmory.setPosition(pos);

    // init armor panel
    m_guiArmor.setScale(GuiScale, GuiScale);
    m_guiArmor.setPosition(m_guiPadding.x * 0.5, m_game->getWindow().getSize().y - (m_guiPadding.y * 0.5));
    m_guiArmor.init(m_game->getTextures().get("armor_default"));
}

void GSGameplay::initPlayer()
{
    // feed world and position player
    m_player->feedWorld(this);
    auto upStairsPos = m_map->gridToWorldPos(m_map->getTilePos(MapSystem::TileAscii::UpStairs));
    m_player->setPosition(upStairsPos + (sf::Vector2f(TileSize, TileSize) / 2.0f));

    // Delete keys at start of each level
    // TODO design keys system
    m_player->getInventory().remove("item_key", 1);
    m_player->getInventory().remove("item_key_armory", 1);
    m_player->getInventory().remove("item_key_infirmary", 1);

    // equip weapon depending on starting class
    if (!m_player->getEquippedWeapon())
    {
        std::string weaponName;
        switch (m_player->getClass())
        {
            case PlayerClass::Gunner: weaponName = "weapon_smg"; break;
            case PlayerClass::Outlaw: weaponName = "weapon_revolver"; break;
            case PlayerClass::Deprived: weaponName = "weapon_shotgun"; break;
            case PlayerClass::Undefined: weaponName = "weapon_pistol"; break;
        }
        m_player->equipWeapon(m_atlas.getSharedEntity<Weapon>(weaponName));
    }
    else m_player->getEquippedWeapon()->refillClip();
}

void GSGameplay::initMap()
{
    // init map, keys and path
    m_map->setTexture(m_game->getTextures().getRef("tile_map_sheet"));
    m_map->generate(m_game->getStore()->getLevelDepth());
    m_map->generateKeys(m_game->getTextures(), &m_atlas);
    m_map->initSpecialRooms(&m_atlas, m_game->getTextures());
    m_map->print();
}

void GSGameplay::initWeapons()
{
    // weapon and projectile textures
    auto projectileText = m_game->getTextures().getRef("base_projectile");
    sf::Vector2f projectileSize(10, 4);

    auto weapon = m_atlas.getEntity<Weapon>("weapon_enemy_default");
    weapon->getProjectile().init(projectileText, projectileSize);

    weapon = m_atlas.getEntity<Weapon>("weapon_enemy_smg");
    weapon->getProjectile().init(projectileText, projectileSize);

    weapon = m_atlas.getEntity<Weapon>("weapon_enemy_shotgun");
    weapon->getProjectile().init(projectileText, projectileSize);

    weapon = m_atlas.getEntity<Weapon>("weapon_pistol");
    weapon->getProjectile().init(projectileText, projectileSize);

    weapon = m_atlas.getEntity<Weapon>("weapon_revolver");
    weapon->getProjectile().init(projectileText, projectileSize);

    weapon = m_atlas.getEntity<Weapon>("weapon_smg");
    weapon->getProjectile().init(projectileText, projectileSize);

    weapon = m_atlas.getEntity<Weapon>("weapon_sawed_off");
    weapon->getProjectile().init(projectileText, projectileSize);

    weapon = m_atlas.getEntity<Weapon>("weapon_shotgun");
    weapon->getProjectile().init(projectileText, projectileSize);
}

void GSGameplay::generateEnemies()
{
    auto drifter = m_atlas.getEntity<Creature>("creature_drifter");
    auto swindler= m_atlas.getEntity<Creature>("creature_swindler");
    auto marauder = m_atlas.getEntity<Creature>("creature_marauder");
    drifter->init(m_game->getTextures());
    swindler->init(m_game->getTextures());
    marauder->init(m_game->getTextures());

    for (unsigned int i = 0; i < (m_map->getRooms().size()); ++i)
    {
        // instanciate creature
        // TODO use factory/replicate method to instanciate
        if (i % 8 == 0) m_enemies.emplace_back(Creature(*swindler));
        else if (i % 5 == 0) m_enemies.emplace_back(Creature(*marauder));
        else m_enemies.emplace_back(Creature(*drifter));

        auto& creature = m_enemies.back();
        creature.setAIModule(std::make_shared<AI::WanderAndAttack>(creature, &(*m_player)));
        creature.feedWorld(this);

        // position creature in map
        sf::Vector2f pos;
        while ((pos = m_map->placeEnemy()) == sf::Vector2f());
        creature.setPosition(m_map->gridToWorldPos(pos) + (sf::Vector2f(TileSize, TileSize) / 2.f));

        // remove if detecting player at first sight
        if (creature.detect(m_player->getSprite().getPosition())) m_enemies.pop_back();
    }
}