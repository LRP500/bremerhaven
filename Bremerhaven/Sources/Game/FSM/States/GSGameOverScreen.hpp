//
// Created by Pierre Roy on 06/06/18.
//

#ifndef BREMERHAVEN_GSGAMEOVERSCREEN_HPP
#define BREMERHAVEN_GSGAMEOVERSCREEN_HPP

#include <SFML/Graphics/Text.hpp>

#include "../GameState.hpp"
#include "../../../Graphics/GUI/Menu.hpp"

namespace FiniteStateMachine
{
    class GSGameOverScreen : public GameState
    {
    private:
        sf::View m_view;
        GUI::Menu m_menu;
        sf::Text m_text;

    public:
        explicit GSGameOverScreen(GSManager*);

        void handleInput() override;
        void update(const sf::Time dt) override;
        void render(const sf::Time dt) override;

    private:
        void startGame();
    };
}

#endif //BREMERHAVEN_GSGAMEOVERSCREEN_HPP
