//
// Created by Pierre Roy on 08/04/18.
//

#ifndef BREMERHAVEN_GSLEVELLOADING_HPP
#define BREMERHAVEN_GSLEVELLOADING_HPP

#include <SFML/System/Clock.hpp>
#include <SFML/Graphics/Text.hpp>

#include "../GameState.hpp"

namespace FiniteStateMachine
{
    class GSLevelLoading : public GameState
    {
    private:
        static const unsigned int Lifetime = 1; // in seconds

    private:
        sf::View m_view;
        sf::Text m_text;
        sf::Clock m_clock;

    public:
        explicit GSLevelLoading(GSManager *);

        void handleInput() override;
        void update(const sf::Time dt) override;
        void render(const sf::Time dt) override;
    };
}

#endif //BREMERHAVEN_GSLEVELLOADING_HPP
