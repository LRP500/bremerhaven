//
// Created by Pierre Roy on 03/04/18.
//

#include "GSNewGame.hpp"
#include "GSLevelLoading.hpp"
#include "GSTitleMenu.hpp"
#include "../GSManager.hpp"
#include "../../Player/Player.hpp"
#include "../../../Graphics/GUI/Menu.hpp"
#include "../../../Graphics/GUI/TextItem.hpp"

using namespace FiniteStateMachine;

GSNewGame::GSNewGame(GSManager *game)
        : GameState(game, StateType::NewGame),
          m_playerClass(EntitySystem::PlayerClass::Undefined)
{
    // set view size to screen size and center it
    sf::Vector2f pos = sf::Vector2f(m_game->getWindow().getSize());
    m_view.setSize(pos);
    m_view.setCenter(pos / 2.0f);

    // menu bottom
    GUI::Menu bottomMenu(game->getFonts().getRef(GUI::DefaultFont), "bottom_navigation",
                         sf::Vector2f(m_game->getWindow().getSize().x, m_game->getWindow().getSize().y / 3),
                         sf::Vector2f(0, m_game->getWindow().getSize().y - m_game->getWindow().getSize().y / 2.6),
                         m_game->getSettings().getData().guiScreenSidePadding, true);
    bottomMenu.addItem("Back");
    bottomMenu.addItem("Start");
    bottomMenu.focus(false);

    // menu character select
    GUI::Menu classSelectMenu(
            game->getFonts().getRef(GUI::DefaultFont), "class_select",
            sf::Vector2f(m_game->getWindow().getSize().x,
                         m_game->getWindow().getSize().y - (bottomMenu.getDimensions().y / 2)),
            sf::Vector2f(0, 0),
            m_game->getSettings().getData().guiScreenSidePadding);
    classSelectMenu.addItem("Gunner");
    classSelectMenu.addItem("Outlaw");
    classSelectMenu.addItem("Deprived");

    classSelectMenu.addTransition(GUI::Down, "bottom_navigation");
    bottomMenu.addTransition(GUI::Up, "class_select");

    m_canvas.addItem(classSelectMenu);
    m_canvas.addItem(bottomMenu);
}

void GSNewGame::handleInput()
{
    sf::Event event {};
    while (m_game->getWindow().pollEvent(event))
    {
        switch (event.type)
        {
            case sf::Event::Closed:
                m_game->getWindow().close();
                break;
            case sf::Event::Resized:break;
            case sf::Event::LostFocus:break;
            case sf::Event::GainedFocus:break;
            case sf::Event::TextEntered:break;
            case sf::Event::KeyPressed:
                switch (event.key.code)
                {
                    case sf::Keyboard::Up:
                        m_canvas.moveUp();
                        break;
                    case sf::Keyboard::Down:
                        m_canvas.moveDown();
                        break;
                    case sf::Keyboard::Left:
                        m_canvas.moveLeft();
                        break;
                    case sf::Keyboard::Right:
                        m_canvas.moveRight();
                        break;
                    case sf::Keyboard::Return:
                        if (m_canvas.getCurrentId() == "bottom_navigation")
                        {
                            switch (m_canvas.getCurrent().getCurrentIndex())
                            {
                                case 0: m_game->changeState(new GSTitleMenu(m_game)); return;
                                case 1: if (!m_canvas.getCurrent().getCurrent()->isLocked()) startgame(); break;
                                default: break;
                            }
                        }
                        else if (m_canvas.getCurrentId() == "class_select")
                        {
                            if (!m_canvas.getCurrent().isMultiChoice())
                                m_canvas.getCurrent().resetSelection();
                            m_canvas.getCurrent().getCurrent()->select(true);

                            // set player class
                            const std::string choice = m_canvas.getCurrent().getCurrent()->getText()->getString();
                            if (choice == "Gunner") m_playerClass = EntitySystem::PlayerClass::Gunner;
                            else if (choice == "Outlaw") m_playerClass = EntitySystem::PlayerClass::Outlaw;
                            else if (choice == "Deprived") m_playerClass = EntitySystem::PlayerClass::Deprived;

                            // manual transition for special cases
                            m_canvas.getCurrent().setCurrent(2);
                            m_canvas.navigateTo("bottom_navigation");
                            m_canvas.getCurrent().setCurrent(1);
                        }
                }
            case sf::Event::KeyReleased:break;
            case sf::Event::MouseWheelMoved:break;
            case sf::Event::MouseWheelScrolled:break;
            case sf::Event::MouseButtonPressed:break;
            case sf::Event::MouseButtonReleased:break;
            case sf::Event::MouseMoved:break;
            case sf::Event::MouseEntered:break;
            case sf::Event::MouseLeft:break;
            case sf::Event::JoystickButtonPressed:break;
            case sf::Event::JoystickButtonReleased:break;
            case sf::Event::JoystickMoved:break;
            case sf::Event::JoystickConnected:break;
            case sf::Event::JoystickDisconnected:break;
            case sf::Event::TouchBegan:break;
            case sf::Event::TouchMoved:break;
            case sf::Event::TouchEnded:break;
            case sf::Event::SensorChanged:break;
            case sf::Event::Count:break;
        }
    }
}

void GSNewGame::update(const sf::Time dt)
{
    m_canvas.update(dt);
    if (m_playerClass == EntitySystem::PlayerClass::Undefined)
        m_canvas.getItem("bottom_navigation")->getItem("Start")->lock();
    else m_canvas.getItem("bottom_navigation")->getItem("Start")->unlock();
}

void GSNewGame::render(const sf::Time dt)
{
    m_game->getWindow().setView(m_view); // set view
    m_canvas.draw(m_game->getWindow()); // draw menus
}

void GSNewGame::startgame()
{
    // reset depth
    m_game->getStore()->setLevelDepth(1);

    // reset player in case of restart
    m_game->getStore()->setPlayer(nullptr);

    // init player
    auto player = std::make_shared<EntitySystem::Player>("", EntitySystem::PlayerClass::Undefined, 1);
    player->init(m_game->getTextures());
    player->setClass(m_playerClass);

    // store player for gameplay manager to access later
    m_game->getStore()->setPlayer(player);

    // launch game
    m_game->pushState(new GSLevelLoading(m_game));
}