//
// Created by Pierre Roy on 03/04/18.
//

#ifndef BREMERHAVEN_GSLOADSAVE_HPP
#define BREMERHAVEN_GSLOADSAVE_HPP

#include "../fsmfwd.h"
#include "../GameState.hpp"
#include "../../../Graphics/GUI/Menu.hpp"

namespace FiniteStateMachine
{
    class GSLoadGame : public GameState
    {
    private:
        sf::View m_view;
        GUI::Menu m_menu;

    public:
        explicit GSLoadGame(GSManager *);

        void handleInput() override;
        void update(const sf::Time dt) override;
        void render(const sf::Time dt) override;
    };
}

#endif //BREMERHAVEN_GSLOADSAVE_HPP
