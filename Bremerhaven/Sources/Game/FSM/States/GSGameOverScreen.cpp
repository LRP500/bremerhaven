//
// Created by Pierre Roy on 06/06/18.
//

#include "GSGameOverScreen.hpp"
#include "GSLevelLoading.hpp"
#include "GSGameplay.hpp"
#include "GSTitleMenu.hpp"
#include "../GSManager.hpp"
#include "../../../Graphics/GUI/TextItem.hpp"
#include "../../../Graphics/GUI/Anchor.hpp"
#include "GSNewGame.hpp"

using namespace FiniteStateMachine;

GSGameOverScreen::GSGameOverScreen(GSManager *game)
        : GameState(game, StateType::GameOverScreen),
          m_menu(game->getFonts().getRef(GUI::DefaultFont))
{
    // init view
    sf::Vector2f pos = sf::Vector2f(m_game->getWindow().getSize());
    m_view.setSize(pos);
    m_view.setCenter(pos / 2.0f);

    // init text
    m_text.setFont(m_game->getFonts().get(GUI::DefaultFont));
    m_text.setString("YOU DIED");
    m_text.setCharacterSize(300);
    m_text.setFillColor(sf::Color::White);
    m_text.setOrigin(Anchor::Global::getCenter(m_text));
    m_text.setPosition(m_game->getWindow().getSize().x / 2,
                       m_game->getWindow().getSize().y / 3);

    // init menu choices
    m_menu.addItem("Quick Restart");
    m_menu.addItem("Change Class");
    m_menu.addItem("Main Menu");

    // init menu transform
    m_menu.setDimensions(sf::Vector2f(
            m_game->getWindow().getSize().x,
            m_game->getWindow().getSize().y / 1.7f));
    m_menu.setPosition(sf::Vector2f(
            0, m_game->getWindow().getSize().y - m_menu.getDimensions().y));
    m_menu.setPadding(m_game->getSettings().getData().guiScreenSidePadding);

    // reset gameplay
    m_game->getStore()->setLevelDepth(GSGameplay::DefaultDepth);
}

void GSGameOverScreen::handleInput()
{
    sf::Event event {};
    while (m_game->getWindow().pollEvent(event))
    {
        switch (event.type)
        {
            case sf::Event::Closed: m_game->getWindow().close(); break;
            case sf::Event::Resized:break;
            case sf::Event::LostFocus:break;
            case sf::Event::GainedFocus:break;
            case sf::Event::TextEntered:break;
            case sf::Event::KeyPressed:
                switch (event.key.code)
                {
                    case sf::Keyboard::Up: m_menu.moveUp(); break;
                    case sf::Keyboard::Down: m_menu.moveDown(); break;
                    case sf::Keyboard::Return:
                        switch (m_menu.getCurrentIndex())
                        {
                            case 0: startGame(); return;
                            case 1: m_game->changeState(new GSNewGame(m_game)); return;
                            case 2: m_game->changeState(new GSTitleMenu(m_game)); return;
                            default: break;
                        }
                }
            case sf::Event::KeyReleased:break;
            case sf::Event::MouseWheelMoved:break;
            case sf::Event::MouseWheelScrolled:break;
            case sf::Event::MouseButtonPressed:break;
            case sf::Event::MouseButtonReleased:break;
            case sf::Event::MouseMoved:break;
            case sf::Event::MouseEntered:break;
            case sf::Event::MouseLeft:break;
            case sf::Event::JoystickButtonPressed:break;
            case sf::Event::JoystickButtonReleased:break;
            case sf::Event::JoystickMoved:break;
            case sf::Event::JoystickConnected:break;
            case sf::Event::JoystickDisconnected:break;
            case sf::Event::TouchBegan:break;
            case sf::Event::TouchMoved:break;
            case sf::Event::TouchEnded:break;
            case sf::Event::SensorChanged:break;
            case sf::Event::Count:break;
        }
    }
}

void GSGameOverScreen::update(const sf::Time dt)
{
    m_menu.update(dt);
}

void GSGameOverScreen::render(const sf::Time dt)
{
    // set view
    m_game->getWindow().setView(m_view);

    // draw game over text
    m_game->getWindow().draw(m_text);

    // draw menu
    m_menu.draw(m_game->getWindow());
}

void GSGameOverScreen::startGame()
{
    m_game->pushState(new GSLevelLoading(m_game));
}