//
// Created by Pierre Roy on 08/04/18.
//

#include "GSLevelLoading.hpp"
#include "GSGameplay.hpp"
#include "../GSManager.hpp"
#include "../../../Graphics/GUI/Menu.hpp"

FSM::GSLevelLoading::GSLevelLoading(FiniteStateMachine::GSManager *game)
        : GameState(game, StateType::LevelLoading)
{
    // Set view size to screen size and center it
    sf::Vector2f pos = sf::Vector2f(m_game->getWindow().getSize());
    m_view.setSize(pos);
    m_view.setCenter(pos / 2.0f);

    // loading screen text
    m_text.setFont(m_game->getFonts().get(GUI::DefaultFont));
    m_text.setCharacterSize(GUI::DefaultFontSize);
    m_text.setString("Depth  " + std::to_string(m_game->getStore()->getLevelDepth()));
    m_text.setOrigin(m_text.getLocalBounds().width / 2, m_text.getLocalBounds().height / 2);
    m_text.setPosition(m_game->getWindow().getSize().x / 2, m_game->getWindow().getSize().y / 2);

    // star loading screen timer
    m_clock.restart();
}

void FiniteStateMachine::GSLevelLoading::handleInput()
{
    sf::Event event {};
    while (m_game->getWindow().pollEvent(event))
    {
        switch (event.type)
        {
            case sf::Event::Closed:
                m_game->getWindow().close();
                break;
            case sf::Event::Resized:break;
            case sf::Event::LostFocus:break;
            case sf::Event::GainedFocus:break;
            case sf::Event::TextEntered:break;
            case sf::Event::KeyPressed:
            case sf::Event::KeyReleased:break;
            case sf::Event::MouseWheelMoved:break;
            case sf::Event::MouseWheelScrolled:break;
            case sf::Event::MouseButtonPressed:break;
            case sf::Event::MouseButtonReleased:break;
            case sf::Event::MouseMoved:break;
            case sf::Event::MouseEntered:break;
            case sf::Event::MouseLeft:break;
            case sf::Event::JoystickButtonPressed:break;
            case sf::Event::JoystickButtonReleased:break;
            case sf::Event::JoystickMoved:break;
            case sf::Event::JoystickConnected:break;
            case sf::Event::JoystickDisconnected:break;
            case sf::Event::TouchBegan:break;
            case sf::Event::TouchMoved:break;
            case sf::Event::TouchEnded:break;
            case sf::Event::SensorChanged:break;
            case sf::Event::Count:break;
        }
    }
}

void FiniteStateMachine::GSLevelLoading::update(const sf::Time dt)
{
    if (m_clock.getElapsedTime().asSeconds() >= Lifetime)
    {
        auto scene = new GSGameplay(m_game);
        m_game->changeState(scene);
        scene->update(dt); // update scene manually so it can be ready for first render
    }
}

void FiniteStateMachine::GSLevelLoading::render(const sf::Time dt)
{
    m_game->getWindow().setView(m_view); // set view
    m_game->getWindow().draw(m_text);
}

