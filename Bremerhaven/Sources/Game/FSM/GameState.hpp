//
// Created by Pierre Roy on 25/03/18.
//

#ifndef BASEENGINE_GAMESTATE_HPP
#define BASEENGINE_GAMESTATE_HPP

#include "fsmfwd.h"

#include "../../Game/Utils/types.h"

namespace FiniteStateMachine
{
    enum StateType
    {
        None,
        SplashScreen,
        GameOverScreen,
        TitleMenu,
        OptionMenu,
        NewGame,
        LoadSave,
        Gameplay,
        LevelLoading,
        EscapeMenu,
        Exit
    };

    // GameStates base class
    class GameState
    {
    protected:
        GSManager* m_game; // Pointer to state manager
        StateType m_type;

    public:
        // Virtual DTOR
        explicit GameState(GSManager* game, StateType type)
                : m_game(game), m_type(type) {}
        virtual ~GameState() = default;

        // Pure virtual methods
        virtual void handleInput() = 0;
        virtual void update(const sf::Time dt) = 0;
        virtual void render(const sf::Time dt) = 0;

        const StateType& getType() const { return m_type; }
    };
}

#endif //BASEENGINE_GAMESTATE_HPP
