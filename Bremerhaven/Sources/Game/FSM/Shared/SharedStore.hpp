//
// Created by Pierre Roy on 06/04/18.
//

#ifndef BREMERHAVEN_SHAREDSTORE_HPP
#define BREMERHAVEN_SHAREDSTORE_HPP

#include "../../Utils/types.h"

namespace FiniteStateMachine
{
    class SharedStore
    {
    private:
        SharedPlayer m_player;
        unsigned int m_levelDepth;

    public:
        SharedStore();

        void setPlayer(SharedPlayer player) { m_player = std::move(player); }
        SharedPlayer getPlayer() { return m_player; }

        void setLevelDepth(unsigned int depth) { m_levelDepth = depth; }
        unsigned int getLevelDepth() const { return m_levelDepth; }
    };
}

#endif //BREMERHAVEN_SHAREDSTORE_HPP
