//
// Created by Pierre Roy on 06/04/18.
//

#include "SharedStore.hpp"
#include "../States/GSGameplay.hpp"

using EntitySystem::Player;
using EntitySystem::PlayerClass;

FiniteStateMachine::SharedStore::SharedStore()
        : m_player(nullptr),
          m_levelDepth(GSGameplay::DefaultDepth)
{}
