//
// Created by Pierre Roy on 23/05/18.
//

#ifndef BREMERHAVEN_FSMFWD_H
#define BREMERHAVEN_FSMFWD_H

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/View.hpp>

namespace FiniteStateMachine
{
    class GSManager;
}

#endif //BREMERHAVEN_FSMFWD_H
