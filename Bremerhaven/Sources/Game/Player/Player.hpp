//
// Created by Pierre Roy on 23/11/17.
//

#ifndef DANKESTDUNGEON_HERO_HPP
#define DANKESTDUNGEON_HERO_HPP

#include <SFML/System/Clock.hpp>

#include "../Creature/Creature.hpp"
#include "../Item/Weapon/Reticule.hpp"
#include "../FOV/FOV.hpp"

#include "../../Game/Utils/types.h"

namespace EntitySystem
{
    // Playable Classes
    enum class PlayerClass
    {
        Undefined,
        Gunner,
        Outlaw,
        Deprived
    };

    // Playable Character
    class Player : public Creature
    {
    private:
        static const unsigned int RollSpeed = 200;
        static constexpr float RollInterval = 0.5f;
        static constexpr float RollTime = 0.4f;
        static constexpr float HitRecoveryTime = 2.5f;

    private:
        FOV m_fov;
        PlayerClass m_class;

        // current level
        unsigned short m_level;

        WeaponSystem::Reticule m_reticule; // shooting reticule

        sf::Clock m_clock; // timer assigned to dash and hit recovering
        bool m_isRecovering;

        // armor amount
        unsigned int m_armor;

    public:
        Player(const std::string& name, PlayerClass className, unsigned short level);

        void init(const TextureHolder&) override;

        void update(const sf::Time) override;

        void draw(sf::RenderWindow &window) override;
        void drawFOV(sf::RenderWindow &window);

        void handleInput(const sf::Time);

        void feedWorld(FSM::GSGameplay*) override;

        void reset(); // reset player sprite and weapon

        void setClass(PlayerClass newClass) { m_class = newClass; }
        const PlayerClass& getClass() const { return m_class; }

        std::string getClassName() const;

        ProjectilePool* getProjectilePool() const override;

        void setArmor(unsigned int armor) { m_armor = armor; }
        unsigned int getArmor() const { return m_armor; }

    private:
        bool handleCollision() override;
        void handleMovementInput(const sf::Time);
        void handleShootingInput(const sf::Time);

        void updateReticule(sf::RenderWindow& window);
        void updateWeapon(const sf::Time dt) override;

        void dash(bool state = true);
        void setInvulnerable(bool state);

        // json
        void load(json_t*, EntitySystem::Atlas*) override;
        int save(EntitySystem::Atlas*);
        json_t* toJson() override;

        bool levelUp();
        unsigned int xpToLevel(unsigned short level) const;
    };
}

#endif //DANKESTDUNGEON_HERO_HPP
