//
// Created by Pierre Roy on 23/11/17.
//

#include <SFML/Window/Mouse.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Graphics/ConvexShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

#include "Player.hpp"
#include "../Utils/Utils.h"
#include "../Item/Item.hpp"
#include "../Item/Weapon/Weapon.hpp"
#include "../Collision/CollisionManager.hpp"
#include "../FSM/States/GSGameplay.hpp"
#include "../../Graphics/Map/Door.hpp"
#include "../../Graphics/Map/Tile.hpp"
#include "../../Graphics/Map/TileMap.hpp"
#include "../../Graphics/GUI/Anchor.hpp"
#include "../../Graphics/Raycast/Daisy.hpp"
#include "../../Graphics/Animation/AnimatedSprite.hpp"
#include "../../Graphics/Resources/ResourceHolder.hpp"
#include "../../RPGSS/Stat/Vital/RPGVital.hpp"
#include "../../RPGSS/Collection/Collections/RPGStrengthCollection.hpp"
#include "../../RPGSS/Collection/Collections/RPGAgilityCollection.hpp"
#include "../../RPGSS/Collection/Collections/RPGDefaultCollection.hpp"

using namespace RPGStatSystem;
using namespace EntitySystem;
using namespace Movement;

Player::Player(const std::string& name,
               PlayerClass classID,
               unsigned short level)
        : Creature("player", name, 0),
          m_fov(),
          m_class(classID),
          m_level(level),
          m_reticule(),
          m_clock(),
          m_isRecovering(false),
          m_armor(0)
{
    setXp(xpToLevel(m_level));
    switch (m_class)
    {
        case PlayerClass::Undefined: m_stats = new RPGStatSystem::RPGDefaultCollection(); break;
        case PlayerClass::Gunner: m_stats = new RPGStatSystem::RPGStrengthCollection(); break;
        case PlayerClass::Outlaw: m_stats = new RPGStatSystem::RPGAgilityCollection(); break;
        case PlayerClass::Deprived: m_stats = new RPGStatSystem::RPGDefaultCollection(); break;
    }
}

void Player::init(const TextureHolder& textureHolder)
{
    Creature::init(textureHolder);

    // init reticule
    m_reticule.init(textureHolder.getRef("reticule"));

    // log success message
    Logger::Write(Logger::INFO, "player initialized");
}

void Player::update(const sf::Time dt)
{
    // update hold weapon
    updateWeapon(dt);

    // process projectile hits and update health
    auto health = m_stats->getStat<RPGVital>(RPGStat::Health);
    int damageTaken = m_cmgr.creatureToProjectile(*this, m_world->getEnemyProjectilePool());
    if (damageTaken)
    {
        // armor ignore damage amount and losing armor does not trigger recovery state
        if (m_armor) m_armor -= 1;
        else
        {
            health->setCurrentValue(health->getCurrentValue() - damageTaken);

            // update state if player still alive
            if (health->getCurrentValue())
            {
                setInvulnerable(true);
                m_isRecovering = true;
            }
        }
    }

    // update health bar
    m_healthBar.setHealth(health->getValue(), health->getCurrentValue());
    m_healthBar.setPosition(Anchor::Global::getTopCenter(m_sprite));

    // update transparency on dash and hit recovery
    if (m_isDashing) m_sprite.setTransparency(100);
    else if (m_isRecovering)
    {
        m_sprite.alphaBlink(100, 150, 0.15);
        if (m_clock.getElapsedTime().asSeconds() >= HitRecoveryTime)
        {
            setInvulnerable(false);
            m_isRecovering = false;
        }

    }
    else m_sprite.setTransparency(255);

    // update animation
    m_sprite.update(dt); // next frame
    if (m_isMoving) m_sprite.play(); // play if moving
    else m_sprite.stop(); // else stop and reset

    // update FOV polygons
//    m_fov.update();
}

void Player::updateReticule(sf::RenderWindow& window)
{
    // get window relative mouse position
    auto mousePos = window.mapPixelToCoords(sf::Mouse::getPosition(window));

    // update position
    if (mousePos != m_reticule.getPosition())
    {
        // update reticule depending on mouse position
        m_reticule.setPosition(mousePos);
    }
}

void Player::updateWeapon(const sf::Time dt)
{
    if (!m_equippedWeapon || !m_weaponWrapper) return;

    // update weapon data
    m_equippedWeapon->update(dt);

    // update rotation
    sf::Vector2f unit(m_reticule.getPosition() - m_sprite.getPosition());
    auto angle = (std::atan2(unit.y, unit.x) * 180 / M_PI);
    m_weaponWrapper->getSprite().setRotation(angle);

    // calculate position depending on reticule angle
    using namespace Utils;
    float radius = 0.f;
    angle = toRadians(getAngle(m_sprite.getPosition(), m_reticule.getPosition()));
    sf::Vector2f pos(std::cos(angle) * radius, std::sin(angle) * radius);
    m_weaponWrapper->getSprite().setPosition(m_sprite.getPosition() + pos);

    radius = m_equippedWeapon->getCanonLength();
    angle = toRadians(getAngle(m_sprite.getPosition(), m_reticule.getPosition()));
    sf::Vector2f anchor(std::cos(angle) * radius, std::sin(angle) * radius);
    m_equippedWeapon->setCanonAnchor(m_sprite.getPosition() + anchor);

    // adjust rotation side
    angle = toDegrees(angle) + 180;
    using MapSystem::TextureScale;
    if (angle <= 90 || angle > 270)
        m_weaponWrapper->getSprite().setScale(TextureScale, -TextureScale);
    else m_weaponWrapper->getSprite().setScale(TextureScale, TextureScale);
}

void Player::draw(sf::RenderWindow& window)
{
    // update reticule position and player orientation
    updateReticule(window);
    updateOrientation(m_reticule.getPosition());

    // draw sprites
    window.draw(m_sprite);
    window.draw(m_collider);
    window.draw(m_weaponWrapper->getSprite());
    window.draw(m_reticule);

    // draw health bar if hurt
    if (!m_stats->getStat<RPGVital>(RPGStat::Health)->full())
        m_healthBar.draw(window);
}

void Player::drawFOV(sf::RenderWindow &window)
{
    window.draw(m_fov);
}

void Player::handleInput(const sf::Time dt)
{
    handleMovementInput(dt);
    handleShootingInput(dt);
}

void Player::handleMovementInput(const sf::Time dt)
{
    // identify movement inputs
    std::bitset<4> flags;
    using sf::Keyboard;
    flags.set((int)(Direction::Left), (Keyboard::isKeyPressed(Keyboard::Left) || Keyboard::isKeyPressed(Keyboard::A)));
    flags.set((int)(Direction::Right), (Keyboard::isKeyPressed(Keyboard::Right) || Keyboard::isKeyPressed(Keyboard::D)));
    flags.set((int)(Direction::Up), (Keyboard::isKeyPressed(Keyboard::Up) || Keyboard::isKeyPressed(Keyboard::W)));
    flags.set((int)(Direction::Down), (Keyboard::isKeyPressed(Keyboard::Down) || Keyboard::isKeyPressed(Keyboard::S)));

    //identity forces
    static const std::vector<MovementData> movements = {
            MovementData(Direction::Left, sf::Vector2f(-dt.asSeconds(), 0.f)),
            MovementData(Direction ::Right, sf::Vector2f(dt.asSeconds(), 0.f)),
            MovementData(Direction::Up, sf::Vector2f(0.f, -dt.asSeconds())),
            MovementData(Direction::Down, sf::Vector2f(0.f, dt.asSeconds()))
    };

    // apply movement
    // divide movement into vertical and horizontal to allow wall sliding
    m_isMoving = false;
    sf::Vector2f moveDirection = sf::Vector2f();
    for (const auto& m : movements)
    {
        if (flags[static_cast<int>(m.direction)])
        {
            if (!m_isDashing) m_isMoving = move(m.force, m_speed);
            moveDirection += m.force;
        }
    }

    // process dash
    static sf::Vector2f direction;
    if (Keyboard::isKeyPressed(Keyboard::Space) && !m_isDashing &&
        m_clock.getElapsedTime().asSeconds() >= RollInterval)
    {
        dash();
        direction = flags.none() ? getDirectionTo(m_reticule.getPosition()) * dt.asSeconds() : moveDirection;
    }

    if (m_isDashing)
    {
        if (m_clock.getElapsedTime().asSeconds() < RollTime)
        {
            move(sf::Vector2f(direction.x, 0), RollSpeed);
            move(sf::Vector2f(0, direction.y), RollSpeed);
        }
        else dash(false);
    }
}

void Player::handleShootingInput(const sf::Time dt)
{
    if (!m_equippedWeapon || m_isDashing) return;

    // fire weapon on input
    if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        m_equippedWeapon->fire(m_reticule.getPosition(), m_world->getPlayerProjectilePool());

    // reload on input
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::R) && !m_equippedWeapon->isClipFull())
        m_equippedWeapon->reload();
}

void Player::dash(bool state)
{
    // dash not allowed when recovering
    if (state && m_isRecovering) return;

    // if not already dashing
    if (m_isDashing != state)
    {
        m_isDashing = state;
        m_clock.restart();

        if (state)
        {
            // start roll invulnerability and cancel reload
            setInvulnerable(true);
            m_equippedWeapon->reload(false);
        }
        else setInvulnerable(false); // end dash invulnerability
    }
}

inline unsigned int Player::xpToLevel(unsigned short level) const
{
    return (unsigned int)(1.5 * std::pow(level, 3));
}

bool Player::levelUp()
{
//    if (m_xp < xpToLevel((unsigned short)(m_level + 1)))
//        return false;
//
//    ++m_level;

    // Variables to keep track of stat changes, and their associated
    // multipliers, which depend on the class. The multiplier affects
    // how much that stat increases each level, and is higher if the
    // class specialises in that stat
    // [hp, strength, agility]
//    unsigned int statIncreases[3] = {0, 0, 0};
//    float statMultipliers[3] = {0, 0, 0};
//    statMultipliers[0] = 13.0;
//    statMultipliers[1] = m_class == "Fighter" ? 8.0 : 6.0;
//    statMultipliers[2] = m_class == "Rogue" ? 8.0 : 6.0;

    // Compute the stat increases for each stat
//    for (int i = 0; i < 3; ++i) {
//        float base = std::tanh(m_level / 30.0) * ((m_level % 2) + 1);
//        statIncreases[i] += int(1 + statMultipliers[i] * base);
//    }

    // Adjust all of the stats accordingly
//    this->hp += statIncreases[0];
//    this->maxHp += statIncreases[0];
//    this->strength += statIncreases[1];
//    this->agility += statIncreases[2];

//    std::cout << m_name << " grew to level " << m_level << " !" << std::endl;
//    std::cout << "Health   +" << statIncreases[0] << " -> " << this->maxHp << std::endl;
//    std::cout << "Strength +" << statIncreases[1] << " -> " << this->strength << std::endl;
//    std::cout << "Agility  +" << statIncreases[2] << " -> " << this->agility << std::endl;

    return true;
}

std::string Player::getClassName() const
{
    switch (m_class)
    {
        case PlayerClass::Gunner: return "Gunner";
        case PlayerClass::Outlaw: return "Outlaw";
        case PlayerClass::Deprived: return "Deprived";
        case PlayerClass::Undefined: return "Undefined";
    }
    return "Undefined";
}

bool Player::handleCollision()
{
    if (!m_world) return false;

    // run collision tests and store result
    auto collidedTiles = m_cmgr.creatureToWalls(*this, m_world->getMap());
    auto collidedDoor = m_cmgr.creatureToDoors(*this, m_world->getMap()->getDoors());
    auto collidedEnemies = m_cmgr.creatureToCreatures(*this, m_world->getEnemyPool());

    // process enemy collision damages
    if (!collidedEnemies.empty() && !m_isRecovering)
    {
        // process damages
        unsigned int onTouchDamage = 5;
        auto health = m_stats->getStat<RPGVital>(RPGStat::Health);
        health->setCurrentValue(health->getCurrentValue() - onTouchDamage);

        // recover if player still alive
        if (health->getCurrentValue())
        {
            setInvulnerable(true);
            m_isRecovering = true;
        }
    }

    // open collided doorIte
    if (collidedDoor)
    {
        using MapSystem::DoorType;
        std::string key;
        switch (collidedDoor->getData()->type)
        {
            case DoorType::Exit: key = "item_key"; break;
            case DoorType::Armory: key = "item_key_armory"; break;
            case DoorType::Infirmary: key = "item_key_infirmary"; break;
        }
        if (m_inventory.count(key))
        {
            // open door
            m_world->getMap()->openDoor(*collidedDoor);

            // remove key if consumable
//            if (m_world->getAtlas().getEntity<Item>(key)->isConsumable())
//                m_inventory.remove(key, 1);
        }
    }

    // return final collision result
    return (!collidedTiles.empty() || collidedDoor || !collidedEnemies.empty());
}

void Player::load(json_t *save, EntitySystem::Atlas* mgr)
{
    Creature::load(save, mgr);

    // class
    std::string className = json_string_value(json_object_get(save, "class"));
    if (className == "Gunner")
        m_class = PlayerClass::Gunner;
    else if (className == "Outlaw")
        m_class = PlayerClass::Outlaw;
    else if (className == "Deprived")
        m_class = PlayerClass::Deprived;

    // level
    m_level = static_cast<unsigned short>(json_integer_value(json_object_get(save, "level")));
}

json_t *Player::toJson()
{
    json_t *hero = Creature::toJson();
    json_object_set_new(hero, "class", json_string(getClassName().c_str()));
    json_object_set_new(hero, "level", json_integer(m_level));
    return hero;
}

int Player::save(EntitySystem::Atlas* mgr)
{
    std::cout << "Saving Progress... ";
    std::string filePath = getName() + ".Json";
    filePath[0] = static_cast<char>(tolower(filePath[0]));
    const char* outFile = filePath.c_str();

    json_t* json_hero(this->toJson());
    if (json_dump_file(json_hero, outFile, JSON_INDENT(3)) == -1)
    {
        std::cout << "Failed to save character" << std::endl;
        return -1;
    }

    std::cout << "OK" << std::endl;
    return 0;
}

ProjectilePool *Player::getProjectilePool() const
{
    return m_world->getPlayerProjectilePool();
}

void Player::setInvulnerable(bool state)
{
    m_collider.setActive(!state); // deactivate collider
    m_clock.restart(); // launch timer
}

void Player::feedWorld(FSM::GSGameplay* world)
{
    // set world ptr
    m_world = world;

    // init FOV
    m_fov.init(m_world->getMap(), &m_sprite);
}

void Player::reset()
{
    // reset transform
    // NO NEED TO RESCALE OR RESET ORIGIN
    // POSITION AND ROTATION ARE UPDATE BY GAMEPLAYER MANAGER
//    m_sprite.setScale(1, 1);
//    m_sprite.setOrigin(0, 0);
//    m_sprite.setPosition(0, 0);
//    m_sprite.setRotation(0);

    // nequip weapon
    // will be restored by GSGameplay at restart
    m_equippedWeapon = nullptr;

    // reset stats
    auto health = m_stats->getOrCreate<RPGVital>(RPGStat::Type::Health);
    health->setToMax();
}
