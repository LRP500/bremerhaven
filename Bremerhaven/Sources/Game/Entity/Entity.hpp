//
// Created by Pierre Roy on 20/11/17.
//

#ifndef DANKESTDUNGEON_ENTITY_HPP
#define DANKESTDUNGEON_ENTITY_HPP

#include <jansson.h>
#include <string>

namespace EntitySystem
{
    class Atlas;

    class Entity
    {
    protected:
        std::string m_id;

    public:
        explicit Entity(const std::string& id);
        virtual ~Entity() = default;

        virtual void load(json_t* value, Atlas* mgr) = 0;

        const std::string& getId() const;

        template <typename T>
        bool isOfType() const;
    };
}

#endif //DANKESTDUNGEON_ENTITY_HPP
