//
// Created by Pierre Roy on 20/11/17.
//

#include "Entity.hpp"
#include "../Atlas/Atlas.hpp"

EntitySystem::Entity::Entity(const std::string &id)
        : m_id(id)
{}

const std::string& EntitySystem::Entity::getId() const
{
    return m_id;
}

template <typename T>
bool EntitySystem::Entity::isOfType() const
{
    return m_id.substr(0, entityToString<T>().size()) == entityToString<T>();
}