//
// Created by Pierre Roy on 26/06/2018.
//

#ifndef BREMERHAVEN_ATTACKTARGET_H
#define BREMERHAVEN_ATTACKTARGET_H

#include "Algorithm.hpp"
#include "../Creature/Creature.hpp"

namespace AI
{
    // Attack target if within range specified by weapon attributes
    class AttackTarget : public AI::Algorithm
    {
    public:
        explicit AttackTarget(EntitySystem::Creature&);
        explicit AttackTarget(EntitySystem::Creature&, EntitySystem::Creature*);

        void action(const sf::Time) override;

        void setTarget(EntitySystem::Creature* target) { m_target = target; }

    private:
        EntitySystem::Creature* m_target;
    };
}

#endif //BREMERHAVEN_ATTACKTARGET_H
