//
// Created by Pierre Roy on 10/08/2018.
//

#ifndef BREMERHAVEN_WANDERANDATTACK_HPP
#define BREMERHAVEN_WANDERANDATTACK_HPP

#include "Algorithm.hpp"
#include "WanderRandomly.hpp"
#include "AttackTarget.hpp"

namespace AI
{
    class WanderAndAttack : public AI::Algorithm
    {
    public:
        explicit WanderAndAttack(EntitySystem::Creature&);
        WanderAndAttack(EntitySystem::Creature&, EntitySystem::Creature*);

        void action(const sf::Time) override;

        void setTarget(EntitySystem::Creature*);

    private:
        AI::WanderRandomly m_wander;
        AI::AttackTarget m_attack;
    };
}

#endif //BREMERHAVEN_WANDERANDATTACK_HPP
