//
// Created by Pierre Roy on 26/06/2018.
//

#include "ChaseTarget.hpp"

AI::ChaseTarget::ChaseTarget(EntitySystem::Creature& creature)
        : Algorithm(creature),
          m_moveTo(creature)
{}

AI::ChaseTarget::ChaseTarget(EntitySystem::Creature& creature,
                             EntitySystem::Creature* target)
        : Algorithm(creature),
          m_target(target),
          m_moveTo(creature)
{}

void AI::ChaseTarget::action(const sf::Time dt)
{}

bool AI::ChaseTarget::needUpdate() const
{
    return false;
}
