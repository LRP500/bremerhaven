//
// Created by Pierre Roy on 25/06/2018.
//

#ifndef BREMERHAVEN_WANDERRANDOMLY_H
#define BREMERHAVEN_WANDERRANDOMLY_H

#include "Algorithm.hpp"
#include "MoveTo.hpp"

namespace AI
{
    class WanderRandomly : public AI::Algorithm
    {
    public:
        explicit WanderRandomly(EntitySystem::Creature&);

        // If the creature has no destination, the action() method randomly
        // picks a (x,y) location, and feeds the destination to the GoToXY module
        // through m_gotoxy.SetDestination(x,y).
        void action(const sf::Time) override;

    protected:
        virtual sf::Vector2f getRandomLocation() const;

    private:
        // Instead of aggregation, private inheritance could also be used.
        AI::MoveTo m_moveTo;
        sf::Vector2f m_destination;
    };
}

#endif //BREMERHAVEN_WANDERRANDOMLY_H
