//
// Created by Pierre Roy on 25/06/2018.
//

#include "WanderRandomly.hpp"
#include "../Utils/Random.h"
#include "../Creature/Creature.hpp"

AI::WanderRandomly::WanderRandomly(EntitySystem::Creature& creature)
        : Algorithm(creature),
          m_moveTo(creature),
          m_destination(sf::Vector2f())
{}

void AI::WanderRandomly::action(const sf::Time dt)
{
    // if no destination or reached destination
    if (m_destination == sf::Vector2f() || m_moveTo.hasReachedDestination())
    {
        // decisive percentage
        if (!Utils::Random::randomInt(150))
        {
            // get new destination
            m_destination = getRandomLocation();
            // feed it to the moveTo module
            m_moveTo.setDestination(m_destination.x, m_destination.y);
        }
    }

    // move action
    m_moveTo.action(dt);
}

sf::Vector2f AI::WanderRandomly::getRandomLocation() const
{
    auto currentPos = m_creature.getSprite().getPosition();

    // get absolute offset value within range
    const sf::Vector2i range(20, 50);
    float offsetX = Utils::Random::randomFloat(range.x, range.y);
    float offsetY = Utils::Random::randomFloat(range.x, range.y);

    // 1/2 chance to get negative value
    offsetX = (Utils::Random::randomInt(2) == 1) ? offsetX : -offsetX;
    offsetY = (Utils::Random::randomInt(2) == 1) ? offsetY : -offsetY;

    return sf::Vector2f(currentPos.x + offsetX, currentPos.y + offsetY);
}
