//
// Created by Pierre Roy on 25/06/2018.
//

#ifndef BREMERHAVEN_ALGORITHM_H
#define BREMERHAVEN_ALGORITHM_H

#include <SFML/System/Time.hpp>

#include "../Creature/Creature.hpp"

namespace AI
{
    class Algorithm
    {
    public:
        explicit Algorithm(EntitySystem::Creature& creature);
        virtual ~Algorithm() = default;

        virtual void action(const sf::Time) = 0;

    protected:
        EntitySystem::Creature& m_creature;
    };
}


#endif //BREMERHAVEN_ALGORITHM_H
