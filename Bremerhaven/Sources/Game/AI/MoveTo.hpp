//
// Created by Pierre Roy on 25/06/2018.
//

#ifndef BREMERHAVEN_MOVETO_H
#define BREMERHAVEN_MOVETO_H

#include "Algorithm.hpp"
#include "Path.hpp"

namespace AI
{
    class MoveTo : public AI::Algorithm
    {
    public:
        explicit MoveTo(EntitySystem::Creature&);
        MoveTo(EntitySystem::Creature&, float posX, float posY);

        void setDestination(float posX, float posY);

        // The overriden method action() calculates the best path (if not
        // already calculated) and then moves the creature one step at a time
        // along this path.
        void action(const sf::Time) override;

        bool hasReachedDestination();

    protected:
        AI::Path m_path;
    };
}


#endif //BREMERHAVEN_MOVETO_H
