//
// Created by Pierre Roy on 25/06/2018.
//

#include "Path.hpp"

AI::Path::Path()
        : m_nodes(),
          m_valid(false)
{}

void AI::Path::generate(float posX, float posY)
{
    // basic straight line path
    m_nodes.push(sf::Vector2f(posX, posY));
    m_valid = true;
}

void AI::Path::pop()
{
    m_nodes.pop();
    if (m_nodes.empty()) m_valid = false;
}
