//
// Created by Pierre Roy on 10/08/2018.
//

#include "WanderAndAttack.hpp"

AI::WanderAndAttack::WanderAndAttack(EntitySystem::Creature& creature)
        : Algorithm(creature),
          m_wander(creature),
          m_attack(creature)
{}

AI::WanderAndAttack::WanderAndAttack(EntitySystem::Creature& creature,
                                 EntitySystem::Creature* target)
        : Algorithm(creature),
          m_wander(creature),
          m_attack(creature, target)
{}

void AI::WanderAndAttack::action(const sf::Time dt)
{
    m_wander.action(dt);
    m_attack.action(dt);
}

void AI::WanderAndAttack::setTarget(EntitySystem::Creature* target)
{
    m_attack.setTarget(target);
}
