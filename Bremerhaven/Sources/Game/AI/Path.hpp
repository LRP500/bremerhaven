//
// Created by Pierre Roy on 25/06/2018.
//

#ifndef BREMERHAVEN_PATH_HPP
#define BREMERHAVEN_PATH_HPP

#include <SFML/System/Vector2.hpp>

#include <stack>

namespace AI
{
    class Path
    {
    public:
        Path();

        void generate(float posX, float posY);

        bool isValid() const { return m_valid; }

        const sf::Vector2f& getNextNode() const { return m_nodes.top(); }
        void pop();
        bool reached() { return m_nodes.empty(); }

    private:
        std::stack<sf::Vector2f> m_nodes;
        bool m_valid;
    };
}

#endif //BREMERHAVEN_PATH_HPP
