//
// Created by Pierre Roy on 26/06/2018.
//

#include "AttackTarget.hpp"
#include "Algorithm.hpp"
#include "../Creature/Creature.hpp"
#include "../Item/Weapon/Weapon.hpp"
#include "../FSM/States/GSGameplay.hpp"

AI::AttackTarget::AttackTarget(EntitySystem::Creature& creature)
        : Algorithm(creature),
          m_target(nullptr)
{}

AI::AttackTarget::AttackTarget(EntitySystem::Creature& creature,
                               EntitySystem::Creature* target)
        : Algorithm(creature),
          m_target(target)
{}

void AI::AttackTarget::action(const sf::Time dt)
{
    // no need to continue if no weapon equipped
    if (!m_creature.getEquippedWeapon()) return;

    // updat weapon state (reloading, fire rate...)
    m_creature.getEquippedWeapon()->update(dt);

    // if target set and in sight then shoot
    if (m_target && m_creature.detect(m_target->getSprite().getPosition()))
    {
        m_creature.getEquippedWeapon()->fire(
                m_target->getSprite().getPosition(), m_creature.getProjectilePool());
    }
}