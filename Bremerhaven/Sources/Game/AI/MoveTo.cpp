//
// Created by Pierre Roy on 25/06/2018.
//

#include <SFML/System/Vector2.hpp>

#include "MoveTo.hpp"
#include "../Utils/Utils.h"

AI::MoveTo::MoveTo(EntitySystem::Creature& creature)
        : Algorithm(creature)
{}

AI::MoveTo::MoveTo(EntitySystem::Creature& creature, float posX, float posY)
        : Algorithm(creature)
{}


void AI::MoveTo::action(const sf::Time dt)
{
    if (m_path.isValid())
    {
        // get vector direction unit
        const sf::Vector2f dir = m_creature.getDirectionTo(m_path.getNextNode()) * dt.asSeconds();

        // move creature depending on it's speed
        bool res = m_creature.move(sf::Vector2f(dir.x, 0), static_cast<int>(m_creature.getSpeed()));
        res &= m_creature.move(sf::Vector2f(0, dir.y), static_cast<int>(m_creature.getSpeed()));

        // pop node if reached
        if (hasReachedDestination() || !res)
        {
            m_path.pop();
        }
    }
}

void AI::MoveTo::setDestination(float posX, float posY)
{
    m_path.generate(posX, posY);
}

bool AI::MoveTo::hasReachedDestination()
{
    // always true if no nodes
    if (m_path.reached()) return true;

    const sf::Vector2f pos(m_creature.getSprite().getPosition());
    const sf::Vector2f dest(m_path.getNextNode());
    return Utils::Digit::inRangeFloat(pos.x, dest.x - 1, dest.x + 1) &&
           Utils::Digit::inRangeFloat(pos.y, dest.y - 1, dest.y + 1);
}