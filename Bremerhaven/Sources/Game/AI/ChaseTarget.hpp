//
// Created by Pierre Roy on 26/06/2018.
//

#ifndef BREMERHAVEN_CHASETARGET_H
#define BREMERHAVEN_CHASETARGET_H

#include "Algorithm.hpp"
#include "../Utils/types.h"
#include "MoveTo.hpp"

using EntitySystem::Creature;

namespace AI
{
    class ChaseTarget : public AI::Algorithm
    {
    public:
        explicit ChaseTarget(EntitySystem::Creature&);
        ChaseTarget(EntitySystem::Creature&, Creature*);

        // action() checks whether the path needs to updated (via the
        // needUpdate() method) and follows the created path.
        void action(const sf::Time) override;

        void setTarget(Creature* target) { m_target = target; }

    protected:
        // The NeedUpdate method returns 'true' if the path needs to be
        // recalculated. This method takes into account the distance to the
        // target creature as well as the distance the target has moved since
        // the last update.
        bool needUpdate() const;

    private:
        Creature* m_target;
        AI::MoveTo m_moveTo;
    };
}

#endif //BREMERHAVEN_CHASETARGET_H
