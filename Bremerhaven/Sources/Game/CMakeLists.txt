project(Bremerhaven)

set(GAME_SOURCE_FILES
        main.cpp

        Entity/Entity.cpp
        Entity/Entity.hpp
        Atlas/Atlas.cpp
        Atlas/Atlas.hpp

        Item/Item.cpp Item/Item.hpp
        Item/Weapon/Weapon.cpp Item/Weapon/Weapon.hpp

        Inventory/Inventory.cpp Inventory/Inventory.hpp

        Player/Player.cpp Player/Player.hpp

        FSM/GameState.hpp
        FSM/GSManager.cpp FSM/GSManager.hpp
        FSM/States/GSGameplay.hpp FSM/States/GSGameplay.cpp
        FSM/States/GSTitleMenu.hpp FSM/States/GSTitleMenu.cpp
        FSM/States/GSOptionMenu.cpp FSM/States/GSOptionMenu.hpp
        FSM/States/GSLoadGame.cpp FSM/States/GSLoadGame.hpp
        FSM/States/GSLevelLoading.cpp FSM/States/GSLevelLoading.hpp
        FSM/States/GSEscapeMenu.cpp FSM/States/GSEscapeMenu.hpp
        FSM/States/GSNewGame.cpp FSM/States/GSNewGame.hpp
        FSM/States/GSGameOverScreen.cpp FSM/States/GSGameOverScreen.hpp
        FSM/Shared/SharedStore.cpp FSM/Shared/SharedStore.hpp
        FSM/fsmfwd.h

        Utils/types.h
        Utils/Random.cpp Utils/Random.h
        Utils/Utils.cpp Utils/Utils.h
        Utils/Uncopyable.hpp


        Collision/collision.h Collision/Collision.cpp
        Collision/CollisionManager.hpp Collision/CollisionManager.cpp

        Collision/BitmaskManager.cpp Collision/BitmaskManager.hpp
        Collision/OrientedBoundingBox.cpp Collision/OrientedBoundingBox.hpp

        FOV/FOV.cpp FOV/FOV.hpp

        Collision/RectangleCollider.cpp Collision/RectangleCollider.hpp

        Creature/Creature.cpp Creature/Creature.hpp
        Item/Weapon/Projectile.cpp Item/Weapon/Projectile.hpp
        Item/Weapon/Reticule.cpp Item/Weapon/Reticule.hpp
        Creature/Movement.h

        Item/Weapon/Behaviors/WeaponFiringBehavior.hpp
        Item/Weapon/Behaviors/PistolFiringBehavior.cpp Item/Weapon/Behaviors/PistolFiringBehavior.hpp
        Item/Weapon/Behaviors/ShotgunFiringBehavior.cpp Item/Weapon/Behaviors/ShotgunFiringBehavior.hpp
        AI/Algorithm.cpp AI/Algorithm.hpp
        AI/TODO/Controller.cpp AI/TODO/Controller.hpp
        AI/MoveTo.cpp AI/MoveTo.hpp
        AI/Path.cpp AI/Path.hpp
        AI/WanderRandomly.cpp AI/WanderRandomly.hpp
        AI/ChaseTarget.cpp AI/ChaseTarget.hpp
        AI/AttackTarget.cpp AI/AttackTarget.hpp
        AI/WanderAndAttack.cpp AI/WanderAndAttack.hpp
        
        Item/Items/Medkit.cpp Item/Items/Medkit.hpp
        Item/Usable.hpp
        
        Atlas/RandomPicker.cpp Atlas/RandomPicker.hpp
        Item/Armor/Armor.cpp Item/Armor/Armor.hpp)

add_executable(Bremerhaven ${GAME_SOURCE_FILES})

target_link_libraries(Bremerhaven jansson)
target_link_libraries(Bremerhaven System)
target_link_libraries(Bremerhaven RPGSS)
target_link_libraries(Bremerhaven Graphics)

set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/../../cmake_modules)
find_package(SFML REQUIRED system window graphics audio)
if (SFML_FOUND)
    include_directories(${SFML_INCLUDE_DIR})
    target_link_libraries(Bremerhaven ${SFML_LIBRARIES})
endif()
