//
// Created by Pierre Roy on 27/08/2018.
//

#ifndef BREMERHAVEN_MEDKIT_HPP
#define BREMERHAVEN_MEDKIT_HPP

#include "../Item.hpp"
#include "../../Utils/types.h"

class Medkit : public Item
{
private:
    static const unsigned int BaseValue = 5;

public:
    Medkit(const std::string&, json_t*, EntitySystem::Atlas*);

    bool use(EntitySystem::Creature*);

    unsigned int getAmount() const { return m_amount; }

    // JSON
    void load(json_t* v, EntitySystem::Atlas* mgr) override;

private:
    unsigned int m_amount;
};


#endif //BREMERHAVEN_MEDKIT_HPP
