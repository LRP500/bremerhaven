//
// Created by Pierre Roy on 27/08/2018.
//

#include "Medkit.hpp"
#include "../../Creature/Creature.hpp"

Medkit::Medkit(const std::string& id,
               json_t *file,
               EntitySystem::Atlas* atlas)
        : Item(id, file, atlas),
          m_amount(BaseValue)
{}

bool Medkit::use(EntitySystem::Creature* creature)
{
    return creature->heal(m_amount);
}

void Medkit::load(json_t *v, EntitySystem::Atlas *mgr)
{
    // fetch weapon data
    m_amount = static_cast<unsigned int>(json_integer_value(json_object_get(v, "damage")));
}
