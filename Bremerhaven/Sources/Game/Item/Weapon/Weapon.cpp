//
// Created by Pierre Roy on 21/11/17.
//

#include <set>
#include "Weapon.hpp"
#include "../../../System/Logger/Logger.hpp"
#include "../../Creature/Creature.hpp"
#include "Behaviors/PistolFiringBehavior.hpp"
#include "Behaviors/ShotgunFiringBehavior.hpp"

Weapon::Weapon(const std::string& id, json_t* v, EntitySystem::Atlas* mgr)
        : Item(id, v, mgr),
          m_projectile(this),
          m_clock(sf::Clock()),
          m_damage(0), m_fireRate(0.f), m_reloadSpeed(0.f), m_accuracy(0.f),
          m_isReloading(false), m_reloadCancel(false),
          m_canonLength(0.f), m_canonAnchor(sf::Vector2f()),
          m_spread(0),
          m_projectileCount(1)
{
    load(v, mgr);
}

void Weapon::load(json_t *v, EntitySystem::Atlas* mgr)
{
    // fetch weapon data
    m_damage = static_cast<unsigned int>(json_integer_value(json_object_get(v, "damage")));
    m_fireRate = static_cast<float>(json_real_value(json_object_get(v, "fire_rate")));
    m_reloadSpeed = static_cast<float>(json_real_value(json_object_get(v, "reload_speed")));
    m_accuracy = static_cast<float>(json_real_value(json_object_get(v, "accuracy")));
    m_clipSize = static_cast<unsigned short>(json_integer_value(json_object_get(v, "clip_size")));
    m_clip = m_clipSize;

    // firing behavior
    auto behavior = static_cast<std::string>(json_string_value(json_object_get(v, "firing_behavior")));
    if (behavior == "singleshot") m_firingBehavior = std::make_shared<PistolFiringBehavior>();
    else if (behavior == "spreadshot") m_firingBehavior = std::make_shared<ShotgunFiringBehavior>();

    // canon anchor defining origin of shot projectiles
    m_canonLength = static_cast<float>(json_real_value(json_object_get(v, "canon_anchor")));

    if (json_object_get(v, "projectile_count") && json_object_get(v, "spread"))
    {
        m_projectileCount = json_integer_value(json_object_get(v, "projectile_count"));
        m_spread = static_cast<float>(json_real_value(json_object_get(v, "spread")));
    }

    // fetch projectile data and construct model
    json_t* projectile = json_object_get(v, "projectile");
    auto velocity = json_integer_value(json_object_get(projectile, "velocity"));
    auto lifespan = static_cast<float>(json_real_value(json_object_get(projectile, "lifespan")));
    m_projectile.setVelocity(velocity);
    m_projectile.setLifespan(lifespan);

    json_decref(projectile);
}

void Weapon::update(const sf::Time dt)
{
    if (!m_clip && !m_reloadCancel) reload();

    if (m_isReloading) // update timer and state
    {
        if (m_clock.getElapsedTime().asSeconds() >= m_reloadSpeed)
        {
            reload(false); // reset reloading state
            refillClip();
        }
    }
}

bool Weapon::fire(const sf::Vector2f& target, ProjectilePool* pool) const
{
    // do not fire if clip empty
    if (!m_clip || m_isReloading) return false;

    // fire if timer allows it or first projectile in clip
    if (m_clip == m_clipSize || m_clock.getElapsedTime().asSeconds() > m_fireRate)
    {
        m_firingBehavior->shoot(this, target, pool);
        m_clock.restart();
        return true;
    }
    return false;
}

void Weapon::refillClip()
{
    if (m_clip == m_clipSize) return;
    m_clip = m_clipSize;
    m_reloadCancel = false;
}

Projectile &Weapon::getProjectile()
{
    return const_cast<Projectile&>(static_cast<const Weapon*>(this)->getProjectile());
}

std::unique_ptr<Weapon> Weapon::create() const
{
    return std::make_unique<Weapon>(*this);
}

void Weapon::reload(bool state)
{
    // reload with state false to cancel reloading
    if (m_isReloading != state)
    {
        m_isReloading = state;
        m_clock.restart();
        m_reloadCancel = !state;
    }
}