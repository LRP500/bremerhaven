//
// Created by Pierre Roy on 22/05/18.
//

#include "Reticule.hpp"
#include "../../../Graphics/Map/MapSystem.hpp"

void WeaponSystem::Reticule::init(SharedTexture texture)
{
    setTexture(std::move(texture));
    setScale(MapSystem::TextureScale, MapSystem::TextureScale);
    setOrigin(getLocalBounds().width / 2, getLocalBounds().height / 2);
}
