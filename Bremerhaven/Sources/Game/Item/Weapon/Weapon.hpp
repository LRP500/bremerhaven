//
// Created by Pierre Roy on 21/11/17.
//

#ifndef DANKESTDUNGEON_WEAPON_HPP
#define DANKESTDUNGEON_WEAPON_HPP

#include <jansson.h>

#include <SFML/System/Clock.hpp>

#include "../../../Graphics/Drawable/DrawableItem.hpp"
#include "Projectile.hpp"
#include "Behaviors/WeaponFiringBehavior.hpp"

// Weapon Class definition
// Defines damage, fire rate, clip size
// TODO inherit from DrawableAndTransformable instead of DrawableItem ??
class Weapon : public Item, public sf::Transformable
{
public:
    typedef std::shared_ptr<WeaponFiringBehavior> FiringBehavior;

private:
    // attributes
    unsigned int m_damage;
    float m_fireRate;
    float m_reloadSpeed;
    float m_accuracy;
    unsigned int m_projectileCount;
    float m_spread;

    bool m_isReloading;
    bool m_reloadCancel;

    // projectile instanciation postition
    float m_canonLength;
    sf::Vector2f m_canonAnchor;

    // weapon shooting behavior
    FiringBehavior m_firingBehavior;

    // clip capacity
    unsigned short m_clipSize;
    // projectile instance model
    Projectile m_projectile;
    // current bullet count
    mutable unsigned int m_clip;
    // fire rate and reloading timer
    mutable sf::Clock m_clock;

public:
    // Weapon can only be defined through JSON file or copy ctor
    // (no need for custom copy ctor if shallow copy enough)
    Weapon(const std::string&, json_t*, EntitySystem::Atlas*);

    void update(const sf::Time);

    bool fire(const sf::Vector2f&, ProjectilePool*) const;

    void reload(bool state = true);
    bool isReloading() const { return m_isReloading; }
    void refillClip();
    void decrementClip(unsigned int n) const { m_clip -= n; }
    int getClipCapacity() const { return m_clipSize; }
    int getClip() const { return m_clip; }
    bool isClipFull() const { return m_clip == m_clipSize; }

    void setBehavior(FiringBehavior fb) { m_firingBehavior = std::move(fb); }

    // called by creature for projectile instanciation
    const Projectile& getProjectile() const { return m_projectile; }
    Projectile& getProjectile();

    float getPrecision() const { return m_accuracy; }
    unsigned int getDamage() const { return m_damage; }
    unsigned int getProjectileCount() const { return m_projectileCount; }
    float getSpread() const { return m_spread; }

    void setCanonLength(float length) { m_canonLength = length; }
    float getCanonLength() const { return m_canonLength; }
    void setCanonAnchor(const sf::Vector2f &pos) { m_canonAnchor = pos; }
    const sf::Vector2f& getCanonAnchor() const { return m_canonAnchor; }

    // factory/replicant method
    std::unique_ptr<Weapon> create() const;

private:
    // JSON
    void load(json_t*, EntitySystem::Atlas*) override;

};

#endif //DANKESTDUNGEON_WEAPON_HPP
