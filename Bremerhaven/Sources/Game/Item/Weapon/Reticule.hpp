//
// Created by Pierre Roy on 22/05/18.
//

#ifndef BREMERHAVEN_RETICULE_HPP
#define BREMERHAVEN_RETICULE_HPP

#include "../../Creature/Movement.h"
#include "../../../Graphics/Drawable/DrawableAndTransformable.hpp"
#include "../../Utils/types.h"

using Movement::Direction;

namespace WeaponSystem
{
    class Reticule : public DrawableAndTransformable
    {
    public:
        explicit Reticule() = default;

        void init(SharedTexture texture);
    };

}

#endif //BREMERHAVEN_RETICULE_HPP
