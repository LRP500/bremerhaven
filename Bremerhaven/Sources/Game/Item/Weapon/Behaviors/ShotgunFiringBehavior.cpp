//
// Created by Pierre Roy on 11/06/18.
//

#include <iostream>
#include "ShotgunFiringBehavior.hpp"
#include "../Weapon.hpp"
#include "../../../../Graphics/Map/MapSystem.hpp"
#include "../../../Utils/Random.h"

void ShotgunFiringBehavior::shoot(const Weapon* weapon,
                                  const sf::Vector2f& target,
                                  ProjectilePool* pool)
{
    auto wt = target;
    auto wca = weapon->getCanonAnchor();
    auto angle = std::atan2(wt.y - wca.y, wt.x - wca.x) * (180 / M_PI) + 180;

    auto spread = weapon->getSpread() / (weapon->getProjectileCount() - 1);
    auto offset = angle - (weapon->getSpread() / 2.f) - 180;
    for (int i = 0; i < weapon->getProjectileCount(); ++i)
    {
        auto acc = Utils::Random::randomFloat(-weapon->getPrecision(), weapon->getPrecision());
        shootSingle(weapon, pool, offset + acc + (spread * i));
    }
    weapon->decrementClip(1);
}

void ShotgunFiringBehavior::shootSingle(const Weapon* weapon,
                                        ProjectilePool* pool,
                                        double angle)
{
    // create projectile
    Projectile proj(weapon->getProjectile());
    // set projectile origin
    proj.setPosition(weapon->getCanonAnchor());

    // get direction depending on angle
    const sf::Vector2f dir(std::cos(angle * (M_PI / 180)),
                           std::sin(angle * (M_PI / 180)));
    // get vector length
    auto length = static_cast<float>(std::sqrt(std::pow(dir.x, 2) + std::pow(dir.y, 2)));
    // divide vector by length tp normalize
    proj.setDirection(sf::Vector2f(dir.x / length, dir.y / length));

    // rotate sprite toward target
    proj.rotate(static_cast<float>(angle));
    // rotate collider
    proj.getCollider().rotate(proj.getRotation());

    // avoid bullet superposition
    proj.move(proj.getDirection() * 15.f);

    // emplace in projectile pool
    pool->emplace_back(std::move(proj));
}