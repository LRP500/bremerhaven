//
// Created by Pierre Roy on 11/06/18.
//

#ifndef BREMERHAVEN_SHOTGUNFIRINGBEHAVIOR_HPP
#define BREMERHAVEN_SHOTGUNFIRINGBEHAVIOR_HPP


#include "WeaponFiringBehavior.hpp"

class ShotgunFiringBehavior : public WeaponFiringBehavior
{
public:
    void shoot(const Weapon*, const sf::Vector2f&, ProjectilePool*) override;

private:
    void shootSingle(const Weapon*, ProjectilePool*, double);
};



#endif //BREMERHAVEN_SHOTGUNFIRINGBEHAVIOR_HPP
