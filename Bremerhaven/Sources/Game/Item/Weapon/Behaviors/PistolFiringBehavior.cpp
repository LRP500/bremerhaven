//
// Created by Pierre Roy on 09/06/18.
//

#include "PistolFiringBehavior.hpp"
#include "../Weapon.hpp"
#include "../../../Utils/Random.h"
#include "../../../../Graphics/Map/MapSystem.hpp"

using namespace Utils::Random;

void PistolFiringBehavior::shoot(const Weapon* weapon,
                                 const sf::Vector2f& target,
                                 ProjectilePool* pool)
{
    auto wt = target;
    auto wca = weapon->getCanonAnchor();
    auto angle = std::atan2(wt.y - wca.y, wt.x - wca.x) * (180 / M_PI) + 180;

    // create projectile
    Projectile proj(weapon->getProjectile());
    // set projectile origin
    proj.setPosition(weapon->getCanonAnchor());

    // get vector direction
    const sf::Vector2f dir(std::cos(angle * (M_PI / 180)), std::sin(angle * (M_PI / 180)));

    // get vector length
    auto length = static_cast<float>(std::sqrt(std::pow(dir.x, 2) + std::pow(dir.y, 2)));
    // divide vector by length tp normalize
    proj.setDirection(-sf::Vector2f(dir.x / length, dir.y / length));

    // rotate sprite toward target
    proj.rotate(static_cast<float>(angle));
    // rotate collider
    proj.getCollider().rotate(proj.getRotation());

    // avoid bullet superposition
    proj.move(proj.getDirection() * weapon->getCanonLength());

    // emplace in projectile pool
    pool->emplace_back(std::move(proj));

    // decrement weapon clip
    weapon->decrementClip(1);
}
