//
// Created by Pierre Roy on 09/06/18.
//

#ifndef BREMERHAVEN_PISTOLFIRINGBEHAVIOR_HPP
#define BREMERHAVEN_PISTOLFIRINGBEHAVIOR_HPP


#include "WeaponFiringBehavior.hpp"

class PistolFiringBehavior : public WeaponFiringBehavior
{
public:
    void shoot(const Weapon*, const sf::Vector2f&, ProjectilePool*) override;
};


#endif //BREMERHAVEN_PISTOLFIRINGBEHAVIOR_HPP
