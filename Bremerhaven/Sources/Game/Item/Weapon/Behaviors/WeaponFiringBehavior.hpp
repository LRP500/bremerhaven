//
// Created by Pierre Roy on 09/06/18.
//

#ifndef BREMERHAVEN_WEAPONFIRINGBEHAVIOR_HPP
#define BREMERHAVEN_WEAPONFIRINGBEHAVIOR_HPP

#include <SFML/System/Vector2.hpp>
#include "../../../Utils/types.h"

class WeaponFiringBehavior
{
public:
    virtual void shoot(const Weapon*, const sf::Vector2f&, ProjectilePool*) = 0;
};


#endif //BREMERHAVEN_WEAPONFIRINGBEHAVIOR_HPP
