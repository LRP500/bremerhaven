//
// Created by Pierre Roy on 19/05/18.
//

#include <iostream>
#include <cmath>

#include <SFML/System/Time.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/CircleShape.hpp>

#include "Projectile.hpp"
#include "../../Collision/Collision.h"
#include "../../Collision/CollisionManager.hpp"
#include "../../../Graphics/Map/MapSystem.hpp"
#include "../../../Graphics/GUI/Anchor.hpp"
#include "Weapon.hpp"
#include "../../Utils/Random.h"

using namespace Utils::Random;

Projectile::Projectile(const Weapon* weapon, unsigned int velocity, float lifespan)
        : m_weapon(weapon),
          m_velocity(velocity),
          m_lifespan(lifespan),
          m_timer(0),
          m_expired(false),
          m_direction(sf::Vector2f()),
          m_collider()
{}

void Projectile::init(SharedTexture texture, sf::Vector2f& colliderSize)
{
    setTexture(std::move(texture));
    setScale(MapSystem::TextureScale, MapSystem::TextureScale);
    setOrigin(getLocalBounds().width / 2, getLocalBounds().height / 2);

    m_collider.setSize(colliderSize);
    m_collider.setOrigin(Anchor::Local::getCenter(m_collider));
    m_collider.setPosition(Anchor::Global::getCenter(*this));
}

void Projectile::update(const sf::Time dt, const MapSystem::TileMap& map)
{
    // update lifetime
    if ((m_timer += dt.asSeconds()) >= m_lifespan) m_expired = true;

    // move depending on direction and velocity
    for (int i = 0; i < m_velocity; ++i)
    {
        // store current position for rollback
        sf::Vector2f oldPos = getPosition();

        // move 1 step
        move(m_direction * dt.asSeconds());
        m_collider.setPosition(getPosition());

        if (processCollisions(map))
        {
            // rollback position
            setPosition(oldPos);
            m_collider.setPosition(oldPos);
            m_expired = true;
            break;
        }
    }
}

void Projectile::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    DrawableAndTransformable::draw(target, states);
    target.draw(m_collider);
}

bool Projectile::processCollisions(const MapSystem::TileMap& map)
{
    static CollisionManager cmgr;
    auto collided = cmgr.projectileOnMap(*this, map);
    return (!collided.empty());
}

unsigned int Projectile::getDamage() const
{
    return m_weapon->getDamage();
}

RectangleCollider& Projectile::getCollider()
{
    return const_cast<RectangleCollider&>(static_cast<const Projectile*>(this)->getCollider());
}
