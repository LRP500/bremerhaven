//
// Created by Pierre Roy on 19/05/18.
//

#ifndef BREMERHAVEN_PROJECTILE_HPP
#define BREMERHAVEN_PROJECTILE_HPP

#include <SFML/Graphics/RectangleShape.hpp>

#include "../../Utils/types.h"
#include "../../Collision/RectangleCollider.hpp"
#include "../../../Graphics/Drawable/DrawableAndTransformable.hpp"

class Projectile : public DrawableAndTransformable
{
private:
    const Weapon* m_weapon;
    unsigned int m_velocity;
    float m_lifespan;

    float m_timer;
    bool m_expired;

    sf::Vector2f m_direction;

    mutable RectangleCollider m_collider;

public:
    explicit Projectile(const Weapon*,
                        unsigned int velocity = 0,
                        float lifespan = 0.f);

    void init(SharedTexture, sf::Vector2f&);
    void update(const sf::Time, const MapSystem::TileMap&);
    void draw(sf::RenderTarget&, sf::RenderStates) const override;

    void setDirection(const sf::Vector2f& dir) { m_direction = dir; }
    void setVelocity(unsigned int velocity) { m_velocity = velocity; }
    void setLifespan(float lifespan) { m_lifespan = lifespan; }
    unsigned int getDamage() const;

    const sf::Vector2f& getDirection() const { return m_direction; }

    const RectangleCollider& getCollider() const { return m_collider; }
    RectangleCollider& getCollider();

    bool isExpired() const { return m_expired; }

private:
    bool processCollisions(const MapSystem::TileMap& map);
};

#endif //BREMERHAVEN_PROJECTILE_HPP
