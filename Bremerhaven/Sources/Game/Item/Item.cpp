//
// Created by Pierre Roy on 21/11/17.
//

#include "Item.hpp"
#include "../Player/Player.hpp"

Item::Item(const Item &rhs)
        : Entity(rhs.getId()),
          m_name(),
          m_description()
{}

Item::Item(const std::string& id, json_t* v, EntitySystem::Atlas *mgr)
        : Entity(id)
{
    load(v, mgr);
}

void Item::load(json_t *v, EntitySystem::Atlas *mgr)
{
    m_name = json_string_value(json_object_get(v, "name"));
    m_description = json_string_value(json_object_get(v, "description"));
}