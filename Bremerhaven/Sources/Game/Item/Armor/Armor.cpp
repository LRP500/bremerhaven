//
// Created by Pierre Roy on 08/09/2018.
//

#include "Armor.hpp"

Armor::Armor(const std::string& id, json_t* v, EntitySystem::Atlas* mgr)
        : Item(id, v, mgr)
{
    load(v, mgr);
}

void Armor::load(json_t* v, EntitySystem::Atlas* mgr)
{
    m_hits = json_integer_value(json_object_get(v, "hits"));
}
