//
// Created by Pierre Roy on 08/09/2018.
//

#ifndef BREMERHAVEN_ARMOR_HPP
#define BREMERHAVEN_ARMOR_HPP

#include <jansson.h>
#include <SFML/Graphics/Transformable.hpp>
#include "../Item.hpp"

class Armor : public Item, public sf::Transformable
{
public:
    Armor(const std::string&, json_t*, EntitySystem::Atlas*);

private:
    void load(json_t*, EntitySystem::Atlas*) override;

private:
    unsigned int m_hits; // number of hits armor can take
};


#endif //BREMERHAVEN_ARMOR_HPP
