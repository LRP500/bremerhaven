//
// Created by Pierre Roy on 21/11/17.
//

#ifndef DANKESTDUNGEON_ITEM_HPP
#define DANKESTDUNGEON_ITEM_HPP

#include "Usable.hpp"
#include "../Utils/types.h"
#include "../Entity/Entity.hpp"

class Item : public EntitySystem::Entity
{
protected:
    std::string m_name;
    std::string m_description;

public:
    Item(const Item& rhs);
    Item(const std::string& id, json_t* v, EntitySystem::Atlas* mgr);

    const std::string& getName() const { return m_name; }
    const std::string& getDescription() const { return m_description; }

    void load(json_t* v, EntitySystem::Atlas* mgr) override;
};

typedef std::shared_ptr<const Item> SharedItem;

#endif //DANKESTDUNGEON_ITEM_HPP
