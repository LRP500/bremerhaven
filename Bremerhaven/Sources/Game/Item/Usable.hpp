//
// Created by Pierre Roy on 27/08/2018.
//

#ifndef BREMERHAVEN_CONSUMABLE_HPP
#define BREMERHAVEN_CONSUMABLE_HPP

#include "../Utils/types.h"

class Usable
{
public:
    Usable() = default;

    virtual bool use() { return false; }
    virtual bool use(EntitySystem::Creature*) { return false; }
};

#endif //BREMERHAVEN_CONSUMABLE_HPP
