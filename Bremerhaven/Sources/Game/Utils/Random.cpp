//
// Created by Pierre Roy on 25/06/2018.
//

#include "Random.h"

namespace Utils::Random
{
    std::random_device rd;
    std::mt19937 mt(rd());
}

int Utils::Random::randomInt(int max)
{
    std::uniform_int_distribution<> dist(0, max - 1);
    return dist(mt);
}

int Utils::Random::randomInt(int min, int max)
{
    std::uniform_int_distribution<> dist(0, max - min);
    return dist(mt) + min;
}

float Utils::Random::randomFloat(float min, float max)
{
    std::uniform_real_distribution<> dist(0, max - min);
    return static_cast<float>(dist(mt) + min);
}


bool Utils::Random::randomBool(double probability)
{
    std::bernoulli_distribution dist(probability);
    return dist(mt);
}
