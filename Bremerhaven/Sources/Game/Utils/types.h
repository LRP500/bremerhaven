//
// Created by Pierre Roy on 04/04/18.
//

#ifndef BREMERHAVEN_TYPES_H
#define BREMERHAVEN_TYPES_H

#include <vector>
#include <deque>

// forward declarations for file dependencies optimization

namespace sf
{
    class RenderWindow;
    class RenderTarget;
    class Sprite;
    class Font;
    class Text;
    class SoundBuffer;
    class Texture;
    class Time;
}

namespace FiniteStateMachine
{
    class GSGameplay;
}

namespace FSM = FiniteStateMachine;

namespace GUI
{
    class TextItem;
    class Menu;
}

namespace Movement
{
    enum class Direction;
}

namespace MapSystem
{
    class RawMap;
    class TileMap;
    class Tile;
    class Door;
    struct RoomData;
    struct DoorData;
}

namespace EntitySystem
{
    class Entity;
    class Creature;
    class Player;
    class Atlas;
}

namespace AI
{
    class Controller;
    class Algorithm;
}

class Item;
class Weapon;
class Projectile;
class DrawableItem;
class AnimatedSprite;

typedef std::shared_ptr<EntitySystem::Entity> SharedEntity;
typedef std::shared_ptr<EntitySystem::Player> SharedPlayer;
typedef std::shared_ptr<const sf::Font> SharedFont;
typedef std::shared_ptr<const sf::Texture> SharedTexture;
typedef std::shared_ptr<const sf::SoundBuffer> SharedSound;
typedef std::shared_ptr<MapSystem::TileMap> SharedMap;
typedef std::shared_ptr<const Item> SharedItem;
typedef std::shared_ptr<Weapon> SharedWeapon;
typedef std::shared_ptr<MapSystem::DoorData> SharedDoor;
typedef std::shared_ptr<MapSystem::RoomData> SharedRoom;
typedef std::shared_ptr<AI::Algorithm> SharedAI;

typedef std::vector<Projectile> ProjectilePool;
// use deque to avoid reference/pointer invalidation from vector emplace_back/push_back
typedef std::deque<EntitySystem::Creature> CreaturePool;

template <typename T> class ResourceHolder;
typedef ResourceHolder<sf::Texture> TextureHolder;
typedef ResourceHolder<sf::Font> FontHolder;
typedef ResourceHolder<sf::SoundBuffer> SoundHolder;

#endif //BREMERHAVEN_TYPES_H
