//
// Created by Pierre Roy on 25/06/2018.
//

#ifndef BREMERHAVEN_RANDOM_H
#define BREMERHAVEN_RANDOM_H

#include <random>

namespace Utils::Random
{
    extern std::random_device rd;
    extern std::mt19937 mt;

    int randomInt(int max); // exclusive max
    int randomInt(int min, int max); // inclusive min and max
    float randomFloat(float min, float max); // inclusive min and max
    bool randomBool(double probability = 0.5);
}


#endif //BREMERHAVEN_RANDOM_H
