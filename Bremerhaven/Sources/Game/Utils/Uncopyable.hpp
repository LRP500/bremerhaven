//
// Created by Pierre Roy on 31/08/2018.
//

#ifndef BREMERHAVEN_UNCOPYABLE_HPP
#define BREMERHAVEN_UNCOPYABLE_HPP

// Prevent construction-copy and assignation-copy
class Uncopyable
{
public:
    Uncopyable() = default;

private:
    Uncopyable(const Uncopyable&) = delete;
    Uncopyable &operator=(const Uncopyable&) = delete;
};

#endif //BREMERHAVEN_UNCOPYABLE_HPP
