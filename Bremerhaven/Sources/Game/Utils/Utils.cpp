//
// Created by Pierre Roy on 09/08/2018.
//

#include <SFML/System/Vector2.hpp>
#include <cmath>
#include "Utils.h"

bool Utils::Digit::inRangeFloat(float x, float low, float high)
{
    return (x >= low) && (x <= high);
}

double Utils::getAngle(const sf::Vector2f& a, const sf::Vector2f& b)
{
    return std::atan2(b.y - a.y, b.x - a.x) * 180 / M_PI;
}

double Utils::toRadians(double angle)
{
    return angle * M_PI / 180;
}

double Utils::toDegrees(double angle)
{
    return angle * 180 / M_PI;
}