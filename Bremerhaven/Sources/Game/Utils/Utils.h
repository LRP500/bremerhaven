//
// Created by Pierre Roy on 09/08/2018.
//

#ifndef BREMERHAVEN_UTILS_HPP
#define BREMERHAVEN_UTILS_HPP

namespace Utils
{
    namespace Digit
    {
        bool inRangeFloat(float x, float low, float high);
    }

    double getAngle(const sf::Vector2f&, const sf::Vector2f&);
    double toRadians(double angle);
    double toDegrees(double angle);
}

#endif //BREMERHAVEN_UTILS_HPP
