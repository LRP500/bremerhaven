//
// Created by Pierre Roy on 22/11/17.
//

#ifndef DANKESTDUNGEON_CREATURE_HPP
#define DANKESTDUNGEON_CREATURE_HPP

#include "Movement.h"
#include "../Utils/types.h"
#include "../Item/Weapon/Reticule.hpp"
#include "../Entity/Entity.hpp"
#include "../Inventory/Inventory.hpp"
#include "../Collision/RectangleCollider.hpp"
#include "../Collision/CollisionManager.hpp"
#include "../../Graphics/GUI/ArmorGUIPanel.hpp"
#include "../../Graphics/GUI/HealthBar.hpp"
#include "../../Graphics/Drawable/DrawableItem.hpp"
#include "../../Graphics/Animation/AnimatedSprite.hpp"
#include "../../RPGSS/Collection/RPGStatCollection.hpp"


namespace EntitySystem
{
    // Creature base class
    class Creature : public EntitySystem::Entity
    {
    public:
        static const unsigned int BaseSpeed = 100;

    protected:
        // world access
        FSM::GSGameplay* m_world;
        // dat animated sprite boi
        AnimatedSprite m_sprite;
        // all player animations
        std::unordered_map<Movement::Direction, Animation> m_animations;
        // stat collection
        mutable RPGStatSystem::RPGStatCollection* m_stats;
        // collider + manager
        RectangleCollider m_collider;
        CollisionManager m_cmgr;
        // inventory
        Inventory m_inventory;
        // orientation
        Movement::Direction m_orientation;
        // movement states
        bool m_isMoving;
        bool m_isDashing;
        // movement speed in pixel/second
        float m_speed;

        // weapon object
        SharedWeapon m_equippedWeapon;
        // weapon wrapper
        std::shared_ptr<DrawableItem> m_weaponWrapper;

        // does it even need commenting
        bool m_alive;

        // health bar gui
        GUI::HealthBar m_healthBar;

        // Attributes
    private:
        std::string m_name;
        unsigned int m_xp;

    // AI
    private:
        std::shared_ptr<AI::Algorithm> m_ai;

    public:
        Creature(const std::string& id, json_t* json,
                 EntitySystem::Atlas* mgr);
        explicit Creature(const std::string& id,
                          const std::string& name,
                          unsigned int xp);
        Creature(const Creature& other);
        ~Creature() override = default;

        virtual void init(const TextureHolder&);
        virtual void update(const sf::Time);
        virtual void draw(sf::RenderWindow&);

        void updateOrientation(const sf::Vector2f&);
        virtual void updateWeapon(const sf::Time dt);

        virtual void feedWorld(FSM::GSGameplay* world) { m_world = world; }

        bool detect(const sf::Vector2f&) const;

        void initWeaponWrapper();

        // Movement
        bool move(const sf::Vector2f& offset, int velocity);
        sf::Vector2f getDirectionTo(const sf::Vector2f&) const;

        virtual ProjectilePool* getProjectilePool() const;

        FSM::GSGameplay* getWorld() { return m_world; }
        const std::string& getName() const { return m_name; }
        const AnimatedSprite& getSprite() const { return m_sprite; }
        const RectangleCollider& getCollider() const { return m_collider; }

        void setPosition(const sf::Vector2f& pos);

        void setXp(unsigned int xp) { m_xp = xp; }
        float getSpeed() const { return m_speed; }
        unsigned int getHealth() const;

        void setAIModule(SharedAI newAI) { m_ai = std::move(newAI); }
        AI::Algorithm* getAIModule() const { return m_ai.get(); }

        void kill();

        // a creature is alive if animated by an AI
        bool isAlive() { return (m_id == "player" || m_ai); }

        // return false if creature can't heal
        bool heal(unsigned int m_amount);

        // weapon
        const SharedWeapon& getEquippedWeapon() const { return m_equippedWeapon; }
        void equipWeapon(SharedWeapon);

        // inventory
        const Inventory& getInventory() const { return m_inventory; }
        Inventory& getInventory();

        // JSON
        virtual json_t* toJson();
        void load(json_t* json, EntitySystem::Atlas* mgr) override;

        // replicate/factory method
        std::unique_ptr<Creature> create() const;

    private:
        const Creature& copy(const Creature& rhs);
        virtual bool handleCollision();
    };
}

#endif //DANKESTDUNGEON_CREATURE_HPP
