//
// Created by Pierre Roy on 22/11/17.
//

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/CircleShape.hpp>

#include "Creature.hpp"
#include "../Utils/Utils.h"
#include "../AI/Algorithm.hpp"
#include "../Item/Weapon/Weapon.hpp"
#include "../FSM/GSManager.hpp"
#include "../FSM/States/GSGameplay.hpp"
#include "../../System/Logger/Logger.hpp"
#include "../../RPGSS/Stat/Vital/RPGVital.hpp"
#include "../../Graphics/Map/Door.hpp"
#include "../../Graphics/GUI/Anchor.hpp"
#include "../../Graphics/Map/TileMap.hpp"
#include "../../Graphics/Map/MapSystem.hpp"
#include "../../Graphics/Raycast/Daisy.hpp"
#include "../../Graphics/Resources/ResourceHolder.hpp"

using namespace RPGStatSystem;

EntitySystem::Creature::Creature(const std::string &id,
                                 const std::string &name,
                                 unsigned int xp)
        : Entity(id),
          m_world(nullptr),
          m_stats(nullptr),
          m_collider(),
          m_cmgr(),
          m_inventory(),
          m_orientation(Movement::Direction::Right),
          m_isMoving(false), m_isDashing(false),
          m_speed(BaseSpeed),
          m_alive(true),
          m_name(name),
          m_xp(xp),
          m_healthBar(),
          m_ai(nullptr)
{}

EntitySystem::Creature::Creature(const std::string &id, json_t *json, EntitySystem::Atlas *mgr)
    : Creature(id, "", 0)
{
    load(json, mgr);
}

EntitySystem::Creature::Creature(const EntitySystem::Creature& other)
        : Entity(other.m_id)
{
    copy(other);
}

void EntitySystem::Creature::init(const TextureHolder& th)
{
    // init animations
    auto spriteSheet = th.getRef("player_animation_sheet");
    Animation right(spriteSheet);
    for (int i = 0; i < 1; ++i) right.addFrame(sf::IntRect(320 * i, 0, 320, 320));
    m_animations.insert(std::make_pair(Direction::Right, std::move(right)));
    Animation left(spriteSheet);
    for (int i = 0; i < 1; ++i) left.addFrame(sf::IntRect(320 * i, 320, 320, 320));
    m_animations.insert(std::make_pair(Direction::Left, left));
    Animation up(spriteSheet);
    for (int i = 0; i < 1; ++i) up.addFrame(sf::IntRect(320 * i, 640, 320, 320));
    m_animations.insert(std::make_pair(Direction::Up, up));
    Animation down(spriteSheet);
    for (int i = 0; i < 1; ++i) down.addFrame(sf::IntRect(320 * i, 960, 320, 320));
    m_animations.insert(std::make_pair(Direction::Down, down));
    m_sprite.setAnimation(&m_animations.at(Direction::Right)); // Set Default animation

    // init transform
    auto gb = m_sprite.getGlobalBounds();
    m_sprite.setOrigin(gb.width / 2, gb.height / 2);
    m_sprite.setScale(MapSystem::TextureScale, MapSystem::TextureScale);

    // init collider
    m_collider.setSize(m_sprite.getSize() / 1.35f);
    m_collider.setOrigin(Anchor::Local::getCenter(m_collider));

    // init health bar
    m_healthBar.setPosition(Anchor::Global::getTopCenter(m_sprite));
}

void EntitySystem::Creature::update(const sf::Time dt)
{
    // update weapon position and angle;
    updateWeapon(dt);

    // process projectile collisions
    int damageTaken = m_cmgr.creatureToProjectile(*this, m_world->getPlayerProjectilePool());
    auto health = m_stats->getOrCreate<RPGVital>(RPGStat::Health);
    health->setCurrentValue(health->getCurrentValue() - damageTaken);
    m_healthBar.setHealth(static_cast<unsigned int>(health->getValue()), health->getCurrentValue());
    m_healthBar.setPosition(Anchor::Global::getTopCenter(m_sprite));

    // "kill" creature if life down to zero
    if (!health->getCurrentValue()) kill();

    // AI action
    if (m_ai) m_ai->action(dt);
}

void EntitySystem::Creature::updateWeapon(const sf::Time dt)
{
    if (!m_equippedWeapon) return;

    using namespace Utils;
    float radius = m_equippedWeapon->getCanonLength();
    auto angle(toRadians(getAngle(m_sprite.getPosition(), m_world->getPlayer()->getSprite().getPosition())));
    sf::Vector2f anchor(std::cos(angle) * radius, std::sin(angle) * radius);
    m_equippedWeapon->setCanonAnchor(m_sprite.getPosition() + anchor);
}

void EntitySystem::Creature::draw(sf::RenderWindow& window)
{
    // udpate orientation
    updateOrientation(m_world->getPlayer()->getSprite().getPosition());

    // draw sprites
    window.draw(m_sprite);
    window.draw(m_collider);

    // draw health bar if hurt
    if (!m_stats->getStat<RPGVital>(RPGStat::Health)->full())
        m_healthBar.draw(window);
}

void EntitySystem::Creature::equipWeapon(SharedWeapon weapon)
{
    m_equippedWeapon = std::move(weapon);
    initWeaponWrapper();
}

void EntitySystem::Creature::initWeaponWrapper()
{
    if (!m_world) return;

    m_weaponWrapper = std::make_shared<DrawableItem>(
            m_equippedWeapon.get(),
            m_world->getManager()->getTextures().get(m_equippedWeapon->getId()),
            MapSystem::TileSize,
            m_sprite.getPosition());
    m_weaponWrapper->getSprite().setOrigin(Anchor::Local::getCenterLeft(m_weaponWrapper->getSprite()));
}

Inventory& EntitySystem::Creature::getInventory()
{
    return const_cast<Inventory&>(static_cast<const Creature*>(this)->getInventory());
}

void EntitySystem::Creature::setPosition(const sf::Vector2f &pos)
{
    // move player sprite
    m_sprite.setPosition(pos);

    // update collider position
    m_collider.setPosition(pos);
}

bool EntitySystem::Creature::move(const sf::Vector2f& offset, int velocity)
{
    for (int i = 0; i < velocity; ++i)
    {
        // store current position for rollback
        sf::Vector2f oldPos = m_sprite.getPosition();

        // move 1 step
        auto pos = getSprite().getPosition() + offset;
        setPosition(pos);

        // check for collisions
        if (handleCollision())
        {
            // rollback
            setPosition(oldPos);
            return false;
        }

        // move weapon depending on offset
        if (m_weaponWrapper) m_weaponWrapper->getSprite().move(offset);
    }
    return true;
}

bool EntitySystem::Creature::handleCollision()
{
    if (!m_world) return false;

    CollisionManager::ConstTileRefVector collidedTiles = m_cmgr.creatureToWalls(*this, m_world->getMap());
    auto collidedDoor = m_cmgr.creatureToDoors(*this, m_world->getMap()->getDoors());
    auto collidedEnemy = m_cmgr.creatureToCreatures(*this, m_world->getEnemyPool());

    return (!collidedTiles.empty() || collidedDoor || !collidedEnemy.empty());
}

unsigned int EntitySystem::Creature::getHealth() const
{
    return m_stats->getStat<RPGVital>(RPGStat::Health)->getCurrentValue();
}

void EntitySystem::Creature::updateOrientation(const sf::Vector2f& target)
{
    // get angle between sprite and target
    auto angle = static_cast<int>(Utils::getAngle(m_sprite.getPosition(), target) + 180);
    // set corresponding animation
    if (angle <= 45 || angle > 315)
        m_sprite.setAnimation(&m_animations.at(Direction::Left));
    else if (angle > 45 && angle <= 135)
        m_sprite.setAnimation(&m_animations.at(Direction::Up));
    else if (angle > 135 && angle <= 225)
        m_sprite.setAnimation(&m_animations.at(Direction::Right));
    else m_sprite.setAnimation(&m_animations.at(Direction::Down));
}

bool EntitySystem::Creature::detect(const sf::Vector2f& pos) const
{
    sf::VertexArray ray(sf::Lines, 2);
    ray[0].position = m_sprite.getPosition();
    ray[1].position = pos;
    static Raycast::Daisy rc;
    bool first = true;
    Raycast::Intersection closestIntersect, newIntersection;
    for (const auto &segment : m_world->getMap()->getSegments())
    {
        newIntersection = rc.cast(ray, segment);
        if (!newIntersection.status) continue;
        if (first || newIntersection.distance < closestIntersect.distance)
        {
            closestIntersect = newIntersection;
        }
        first = false;
    }
    if (closestIntersect.position == sf::Vector2f()) return false;

    // draw line of sight
//    ray[1].position = closestIntersect.position;
//    ray[0].color = sf::Color::Red;
//    ray[1].color = sf::Color::Red;
//    window.draw(ray);

    using Raycast::getDistance;
    return getDistance(m_sprite.getPosition(), closestIntersect.position) >=
           getDistance(m_sprite.getPosition(), pos);
}

bool EntitySystem::Creature::heal(unsigned int m_amount)
{
    // get health stat
    auto health = m_stats->getStat<RPGVital>(RPGStat::Health);

    // return error if can't heal
    if (health->full()) return false;

    // add healing amount to creature
    health->setCurrentValue(health->getCurrentValue() + m_amount);
    return true;
}

ProjectilePool *EntitySystem::Creature::getProjectilePool() const
{
    return m_world->getEnemyProjectilePool();
}

sf::Vector2f EntitySystem::Creature::getDirectionTo(const sf::Vector2f& dest) const
{
    // store direction unit vector
    const sf::Vector2f dir = sf::Vector2f(dest.x, dest.y) - m_sprite.getPosition();
    float magnitude = std::sqrt(std::pow(dir.x, 2) + std::pow(dir.y, 2));
    sf::Vector2f unitVector(dir.x / magnitude, dir.y / magnitude);
    return unitVector;
}

void EntitySystem::Creature::kill()
{
    m_ai = nullptr;
    m_alive = false;
    m_collider.setActive(false);
}

void EntitySystem::Creature::load(json_t *json, EntitySystem::Atlas *mgr)
{
    // Get JSON main object
    json_t* obj = json_object();

    // Initialize empty stat collection
    // GetOrCreate<...>() will then ensure that all stats are properly created if needed
    if (!m_stats) m_stats = new RPGStatCollection();

    // Recover name
    m_name = json_string_value(json_object_get(json, "name"));

    // Recover vitals
    auto health = m_stats->getOrCreate<RPGVital>(RPGStat::Type::Health);
    auto current = static_cast<int>(json_integer_value(json_object_get(json, "hp")));
    health->setCurrentValue(current);
    if (json_object_get(json, "hp_max"))
    {
        health->setValue(static_cast<unsigned int>(json_integer_value(json_object_get(json, "hp_max"))));
    }
    else health->setValue(current);

    // Recover attributes
    auto strength = m_stats->getOrCreate<RPGAttribute>(RPGStat::Type::Strength);
    auto agility = m_stats->getOrCreate<RPGAttribute>(RPGStat::Type::Agility);

    strength->setValue(static_cast<unsigned int>(json_integer_value(json_object_get(json, "strength"))));
    agility->setValue(static_cast<unsigned int>(json_integer_value(json_object_get(json, "agility"))));

    // Recover experience
    m_xp = static_cast<unsigned int>(json_integer_value(json_object_get(json, "xp")));

    // Recover inventory
    json_t* temp;
    if ((temp = json_object_get(json, "inventory")))
    {
        m_inventory = Inventory(temp, mgr);
    }

    // Set equipped items
    if ((temp = json_object_get(json, "equipped_weapon")))
    {
        std::string weapon = json_string_value(temp);
        equipWeapon((weapon == "nullptr") ? nullptr : mgr->getSharedEntity<Weapon>(weapon));
    }
}

json_t *EntitySystem::Creature::toJson()
{
    // Get current stats
    auto health = m_stats->getStat<RPGVital>(RPGStat::Type::Health);
    auto strength = m_stats->getStat<RPGAttribute>(RPGStat::Type::Strength);
    auto agility = m_stats->getStat<RPGAttribute>(RPGStat::Type::Agility);

    // Get JSON Object
    json_t* obj = json_object();

    // Save current stats to JSON object
    json_object_set_new(obj, "name", json_string(m_name.c_str()));
    json_object_set_new(obj, "inventory", m_inventory.toJson());
    json_object_set_new(obj, "hp", json_integer(health->getCurrentValue()));
    json_object_set_new(obj, "hp_max", json_integer(health->getValue()));
    json_object_set_new(obj, "strength", json_integer(strength->getValue()));
    json_object_set_new(obj, "agility", json_integer(agility->getValue()));
    json_object_set_new(obj, "xp", json_integer(m_xp));

    // Save equipped items to JSON object
    std::string temp = (getEquippedWeapon() == nullptr) ? "nullptr" : getEquippedWeapon()->getId();
    json_object_set_new(obj,"equipped_weapon", json_string(temp.c_str()));

    return obj;
}

std::unique_ptr<EntitySystem::Creature> EntitySystem::Creature::create() const
{
    return std::make_unique<Creature>(*this);
}

const EntitySystem::Creature& EntitySystem::Creature::copy(const Creature &rhs)
{
    if (this == &rhs) return *this;

    // shallow copy through default operator= for non pointer types
    *this = rhs;

    // replicate weapon
    if (m_equippedWeapon) m_equippedWeapon = std::move(rhs.getEquippedWeapon()->create());

    // deep copy stat collection
    // TODO specify collection name in JSON to avoid deep copy. Nasty
    // e.g. m_stats = new RPGAgilityCollection();
    m_stats = new RPGStatCollection(*rhs.m_stats);

    return *this;
}