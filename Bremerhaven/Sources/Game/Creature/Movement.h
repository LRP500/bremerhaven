//
// Created by Pierre Roy on 22/05/18.
//

#ifndef BREMERHAVEN_MOVEMENT_H
#define BREMERHAVEN_MOVEMENT_H

#include <SFML/System/Vector2.hpp>

namespace Movement
{
    // Direction enumeration definition
    enum class Direction
    { Up, Down, Right, Left };

    // Movement data struct definition
    struct MovementData
    {
        MovementData(Direction dir, const sf::Vector2f& f)
                : direction(dir), force(f)
        {}
        Direction direction;
        sf::Vector2f force;
    };
}

#endif //BREMERHAVEN_MOVEMENT_H
