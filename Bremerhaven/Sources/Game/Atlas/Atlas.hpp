//
// Created by Pierre Roy on 20/11/17.
//

#ifndef DANKESTDUNGEON_ENTITYMANAGER_HPP
#define DANKESTDUNGEON_ENTITYMANAGER_HPP

#include <map>

#include "RandomPicker.hpp"
#include "../Entity/Entity.hpp"
#include "../Utils/types.h"
#include "../Utils/Uncopyable.hpp"
#include "../../System/Logger/Logger.hpp"

namespace EntitySystem
{
    class Atlas : public Uncopyable
    {
    private:
        // all entities loaded from JSON files
        mutable std::map<std::string, std::shared_ptr<Entity>> m_entities;

        // Pools
        friend class RandomPicker;
        RandomPicker m_picker;

    public:
        Atlas() = default;

        void generatePools();

        const RandomPicker& getRandomPicker() const { return m_picker; }

        template<typename T>
        int loadJson(const std::string &filename);

        template<typename T>
        T* getEntity(const std::string& id) const;

        template<typename T>
        std::shared_ptr<T> getSharedEntity(const std::string& id) const;
    };

    // return class name
    template <typename T>
    std::string entityToString(bool standardized = false);

    template<typename T>
    int Atlas::loadJson(const std::string& filename)
    {
        json_t *root, *value;
        const char *key;

        // Get JSON root from file name
        const std::string standardized = entityToString<T>(true);
        if (!(root = json_load_file(filename.c_str(), 0, nullptr))) return 0;

        // Save all entities recovered from JSON data
        json_object_foreach(root, key, value)
        { m_entities[key] = std::make_shared<T>(key, value, this); }

        return 1;
    }

    template<typename T>
    T* Atlas::getEntity(const std::string& id) const
    {
        if (id.substr(0, entityToString<T>().size()) != entityToString<T>()) return nullptr;
        return dynamic_cast<T*>(m_entities[id].get());
    }

    template<typename T>
    std::shared_ptr<T> Atlas::getSharedEntity(const std::string& id) const
    {
        if (id.substr(0, entityToString<T>().size()) != entityToString<T>()) return nullptr;
        return std::dynamic_pointer_cast<T>(m_entities[id]);
    }
}

#endif //DANKESTDUNGEON_ENTITYMANAGER_HPP
