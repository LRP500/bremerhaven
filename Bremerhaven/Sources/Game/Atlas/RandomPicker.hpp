//
// Created by Pierre Roy on 31/08/2018.
//

#ifndef BREMERHAVEN_RANDOMPICKER_HPP
#define BREMERHAVEN_RANDOMPICKER_HPP

#include <vector>

#include "../Utils/Uncopyable.hpp"
#include "../Entity/Entity.hpp"

namespace EntitySystem
{
// RandomPicker defines object pools and provides random access methods for instancation
    class RandomPicker : public Uncopyable
    {
    public:
        RandomPicker() = default;

        void init(const Atlas *const);

        const std::string& rollArmory() const;
        const std::string& rollInfirmary() const;

    private:
        // Mod pools
        std::vector<std::string> m_militaryPool;
        std::vector<std::string> m_medicalPool;
    };
}

#endif //BREMERHAVEN_RANDOMPICKER_HPP
