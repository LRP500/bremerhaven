//
// Created by Pierre Roy on 31/08/2018.
//

#include <iostream>

#include "RandomPicker.hpp"
#include "Atlas.hpp"
#include "../Utils/Random.h"
#include "../../System/Logger/Logger.hpp"

using namespace Utils::Random;

void EntitySystem::RandomPicker::init(const Atlas* atlas)
{
    for (auto&& entity : atlas->m_entities)
    {
        // log error if resource not found
        if (!entity.second)
        {
            Logger::Write(Logger::WARNING, "resource for " + entity.first + " not found");
            continue;
        }

        // get entity id
        auto id = entity.second->getId();

        // add weapons to military pool
        if (id.substr(0, std::string("weapon").size()) == "weapon")
        {
            // do not add enemy weapons into pool
            if (id.substr(0, std::string("weapon_enemy").size()) == "weapon_enemy") continue;
            m_militaryPool.emplace_back(id);
        }
        // add mods to appropriate pool depending on substring
        else if (id.substr(0, std::string("mod").size()) != "mod")
        {
            unsigned long pos = id.find('_');
            if (id.substr(pos + 1, std::string("military").size()) == "military")
                m_militaryPool.emplace_back(id);
            else if (id.substr(pos + 1, std::string("medical").size()) == "medical")
                m_medicalPool.emplace_back(id);
            else continue;
        }
    }
}

const std::string& EntitySystem::RandomPicker::rollArmory() const
{
    return m_militaryPool[randomInt(m_militaryPool.size())];
}

const std::string &EntitySystem::RandomPicker::rollInfirmary() const
{
    return m_medicalPool[randomInt(m_medicalPool.size())];
}
