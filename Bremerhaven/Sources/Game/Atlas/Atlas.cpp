//
// Created by Pierre Roy on 20/11/17.
//

#include <cctype>

#include "Atlas.hpp"
#include "../Item/Item.hpp"
#include "../Item/Armor/Armor.hpp"
#include "../Item/Items/Medkit.hpp"
#include "../Item/Weapon/Weapon.hpp"
#include "../Creature/Creature.hpp"

using namespace EntitySystem;

void EntitySystem::Atlas::generatePools()
{
    m_picker.init(this);
}

// template specialization
namespace EntitySystem
{
    template <> std::string entityToString<Item>(bool standardized){ return standardized ? "items" : "item"; }
    template <> std::string entityToString<Armor>(bool standardized){ return standardized ? "armors" : "armor"; }
    template <> std::string entityToString<Medkit>(bool standardized){ return standardized ? "medkits" : "medkit"; }
    template <> std::string entityToString<Weapon>(bool standardized) { return standardized ? "weapons" : "weapon"; }
    template <> std::string entityToString<Creature>(bool standardized) { return standardized ? "creatures" : "creature"; }
}

