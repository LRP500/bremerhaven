//
// Created by Pierre Roy on 25/11/17.
//

#include <fstream>

#include "GameManager.hpp"

#include "../Item/Item.hpp"
#include "../Item/Items/Armor.hpp"
#include "../Weapon/Weapon.hpp"
#include "../Item/Items/Medecine.hpp"
#include "../Area/Area.hpp"
#include "../Area/Door/Door.hpp"
#include "../Battle/Battle.hpp"

#include "../Utils/global.h"

static void searchArea(Player *hero, Area *area)
{
    std::cout << "You find:" << std::endl;
    area->getItems().print();
    hero->getInventory().merge(&(area->getItems()));
    area->setItems(Inventory());
}

static void nextArea(Player *hero, Area *current, int next)
{
    Door *door = current->getDoors()[next - current->getDialogue().size() - 1];
    int flag = hero->traverse(door);
    if (flag == 0)
        std::cout << "The " << door->getDescription() << " is locked." << std::endl;
    else if (flag == 1)
        std::cout << "You unlock the " << door->getDescription() << " and go through it." << std::endl;
    else if (flag == 2)
        std::cout << "You go through the " << door->getDescription() << "." << std::endl;
}

int GameManager::Initialize()
{
    std::cout << "Initializing..." << std::endl;
    if (!(m_entityManager.loadJson<Item>(SystemSettings::G_JSON_DIRECTORY_PATH + "items.json"))
        || !(m_entityManager.loadJson<Armor>(SystemSettings::G_JSON_DIRECTORY_PATH + "armor.json"))
        || !(m_entityManager.loadJson<Weapon>(SystemSettings::G_JSON_DIRECTORY_PATH + "weapons.json"))
        || !(m_entityManager.loadJson<Creature>(SystemSettings::G_JSON_DIRECTORY_PATH + "creatures.json"))
        || !(m_entityManager.loadJson<Door>(SystemSettings::G_JSON_DIRECTORY_PATH + "doors.json"))
        || !(m_entityManager.loadJson<Area>(SystemSettings::G_JSON_DIRECTORY_PATH + "areas.json"))) {
        fprintf(stderr, "Error loading media\n");
        return -1;
    }

    std::srand((unsigned int)(time(nullptr)));

    std::cout << "Resources initialized" << std::endl;
    std::cout << "Starting game...\n" << std::endl;

    m_hero = createCharacter();
    m_hero.setCurrentArea("area_01");

    return 0;
}

void GameManager::Run()
{
    while (true)
    {
        m_hero.addVisitedArea(m_hero.getCurrentArea());
        Area *areaPtr = m_hero.getAreaPtr(&m_entityManager);

        if (SystemSettings::G_AUTOMATIC_SAVE)
            m_hero.save(&m_entityManager);

        if (!areaPtr->getCreatures().empty()) {
            std::vector<Creature *> combatants;
            combatants.reserve(8);
            std::cout << "You are attacked by ";
            for (int i = 0; i < areaPtr->getCreatures().size(); ++i) {
                auto *c = const_cast<Creature *>(&(areaPtr->getCreatures()[i]));
                combatants.push_back(c);
                std::cout << c->getName() << (i == areaPtr->getCreatures().size() - 1 ? "\n" : ", ");
            }
            combatants.push_back(&m_hero);

            BattleSystem::Battle battle(combatants);
            battle.run();

            if (m_hero.getHp() > 0) {
                // Or use std::accumulate, but that requires an additional header
                unsigned int xp = 0;
                for (auto &creature : areaPtr->getCreatures())
                    xp += creature.getXp();
                m_hero.setXp(m_hero.getXp() + xp);
                std::cout << "You gained " << xp << " experience\n";
                areaPtr->setCreatures(std::vector<Creature>()); // Remove the creatures from the area
                continue ;
            } else {
                return destroyCharacter();
            }
        }

        Dialogue roomOptions = areaPtr->getDialogue();
        for (auto door : areaPtr->getDoors())
            roomOptions.addChoice("Go through the " + door->getDescription());
        roomOptions.addChoice("Search");
        roomOptions.addChoice("Menu");

        int result = roomOptions.activate();
        if (result == roomOptions.size())
            menuDialogue();
        else if (result == areaPtr->getDoors().size() + 1)
            searchArea(&m_hero, areaPtr);
        else if (result && result <= areaPtr->getDoors().size()) {
            nextArea(&m_hero, areaPtr, result);
        }
    }
}

void GameManager::menuDialogue()
{
    while (true) {
        int result = Dialogue("Menu\n====", {"Inventory", "Equipement", "Character", "Back", "Save & Exit"}).activate();
        switch (result) {
            case 1:
                inventoryDialogue();
                break;
            case 2:
                equippementDialogue();
                break ;
            case 3:
                characterDialogue();
                break ;
            case 4:
                return ;
            case 5:
                m_hero.save(&m_entityManager);
                exit(0);
            default:
                break;
        }
    }
}

void GameManager::inventoryDialogue()
{
    while (true)
    {
        std::cout << "Inventory\n=====" << std::endl;
        m_hero.getInventory().print();
        int result = Dialogue("", {"Use", "Close"}).activate();
        if (result == 1)
        {
            int userInput = 0;
            std::vector<Item*> usables = m_hero.getInventory().getUsables();
            if (usables.empty())
                continue ;
            m_hero.getInventory().printUsables(true);
            while (!userInput)
            {
                std::cout << "Choose Item :" << std::endl;
                std::cin >> userInput;
                if (userInput >= 1 && userInput <= usables.size())
                {
                    if (usables[userInput - 1]->isOfType<Medecine>())
                    {
                        int healed = m_hero.heal(dynamic_cast<Medecine *>(usables[userInput - 1]));
                        if (healed)
                            std::cout << m_hero.getName() << " gained " << healed << " HP" << std::endl;
                    }
                }
            }
            usables.clear();
        }
        else if (result == 2)
        {
            break ;
        }
    }
}

void GameManager::equippementDialogue()
{
    while (true)
    {
        std::cout << "Equipment\n=========\n";
        std::cout << "Armor: "
                  << (m_hero.getEquippedArmor() != nullptr ? m_hero.getEquippedArmor()->getName() : "Nothing")
                  << std::endl;
        std::cout << "Weapon: "
                  << (m_hero.getEquippedWeapon() != nullptr ? m_hero.getEquippedWeapon()->getName() : "Nothing")
                  << std::endl;

        int result = Dialogue("", {"Equip Armor", "Equip Weapon", "Close"}).activate();
        if (result == 1) {
            int userInput = 0;
            int numItems = m_hero.getInventory().print<Armor>(true);
            if (numItems == 0)
                continue ;
            while (!userInput) {
                std::cout << "Choose Armor :" << std::endl;
                std::cin >> userInput;
                if(userInput >= 1 && userInput <= numItems)
                    m_hero.equipArmor(m_hero.getInventory().get<Armor>(userInput - 1));
            }
        } else if (result == 2) {
            int userInput = 0;
            int numItems = m_hero.getInventory().print<Weapon>(true);
            if (numItems == 0)
                continue ;
            while (!userInput) {
                std::cout << "Choose Weapon :" << std::endl;
                std::cin >> userInput;
                if(userInput >= 1 && userInput <= numItems)
                    m_hero.equipWeapon(m_hero.getInventory().get<Weapon>(userInput - 1));
            }
            std::cout << "----------------\n";
        } else if (result == 3) {
            break ;
        }
    }
}

void GameManager::characterDialogue()
{
    std::cout << "Character\n=========\n";
    std::cout << m_hero.getName() << std::endl;
    std::cout << "Health:   " << m_hero.getHp() << " / " << m_hero.getMaxHp() << std::endl;
    std::cout << "Strength: " << m_hero.getStrength() << std::endl;
    std::cout << "Agility:  " << m_hero.getAgility() << std::endl;
    std::cout << "Level:    " << m_hero.getLevel() << " (" << m_hero.getXp();
    std::cout << " / " << m_hero.xpToLevel(static_cast<unsigned short>(m_hero.getLevel() + 1)) << ")" << std::endl;
    Dialogue("", {"Close"}).activate();
}

json_t* GameManager::fetchCharacter(const std::string& name) const
{
    json_error_t error {};
    json_t *save = json_load_file(std::string(name + ".Json").c_str(), 0, &error);
    if (save)
        std::cout << "Existing Character Data Found" << std::endl;
    return save;
}

json_t* GameManager::fetchProgress(const std::string& name) const
{
    std::cout << "Fetching Map Data... ";
    json_error_t error {};
    std::string progressFilePath(name + "_areas.json");
    json_t *areas = json_load_file(progressFilePath.c_str(), 0, &error);
    if (!areas)
        std::cout << "Map Data Not Found" << std::endl;
    else
        std::cout << "OK" << std::endl;
    return areas;
}

static Player chooseClass(const std::string& name)
{
    int result = Dialogue("Choose Your Class", {"Gunner", "Outlaw", "Deprived"}).activate();
    switch (result)
    {
        case 1:
            return Player(name, Player::Class::Gunner, 1);
        case 2:
            return Player(name, Player::Class::Outlaw, 1);
        case 3:
            return Player(name, Player::Class::Deprived, 1);;
        default:
            break ;
    }
    return Player(name, Player::Class::Deprived, 1);;
}

Player GameManager::createCharacter()
{
    std::cout << "Welcome To Bremerhaven\n==========================\n" << std::endl;
    std::cout << "Enter Your Name" << std::endl;
    std::string name;
    std::cin >> name;

    json_t *save = fetchCharacter(name);
    if (save)
    {
        json_t *progress = fetchProgress(name);
        return Player(save, progress, &m_entityManager);
    }
    else
    {
        return chooseClass(name);
    }
}

void GameManager::Clean()
{}

void GameManager::destroyCharacter()
{
    std::cout << "\n\t----YOU DIED----\n\tGameManager Over\n" << std::endl;
    resetProgress(m_hero.getName());
}

void GameManager::resetProgress(const std::string &name)
{
//    m_hero.reset();
//    m_hero.save(&m_entityManager);
}