//
// Created by Pierre Roy on 25/11/17.
//

#ifndef DANKESTDUNGEON_GAMEMANAGER_HPP
#define DANKESTDUNGEON_GAMEMANAGER_HPP

#include "../Atlas/Atlas.hpp"
#include "../Player/Player.hpp"

class GameManager
{
private:
    EntitySystem::EntityManager m_entityManager;
    Player m_hero;

public:
    GameManager() = default;
    ~GameManager() = default;

    int Initialize();
    void Run();
    void Clean();

    void menuDialogue();
    void inventoryDialogue();
    void equippementDialogue();
    void characterDialogue();

    json_t* fetchCharacter(const std::string& name) const;
    json_t* fetchProgress(const std::string& name) const;
    void resetProgress(const std::string& name);

    Player createCharacter();
    void destroyCharacter();
};

#endif //DANKESTDUNGEON_GAMEMANAGER_HPP
