//
// Created by Pierre Roy on 26/04/18.
//

#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/ConvexShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

#include "FOV.hpp"
#include "../../Graphics/Animation/AnimatedSprite.hpp"
#include "../../Graphics/Raycast/Daisy.hpp"
#include "../../Graphics/Map/TileMap.hpp"

FOV::FOV() : m_target(nullptr), m_color(255, 0, 0, 110)
{}

void FOV::init(MapSystem::TileMap* map, AnimatedSprite* target)
{
    m_map = map;
    m_target = target;
}

void FOV::update()
{
    getAngles();
    sortAngles();
    intersect();
    updateVertices();
}

void FOV::drawDebug(sf::RenderWindow &window)
{
    // draw map segments
    for (auto&& segment : m_map->getSegments())
    {
        sf::VertexArray ray(sf::LineStrip, 2);
        ray[0].position = segment[0].position;
        ray[1].position = segment[1].position;
        ray[0].color = sf::Color::Red;
        ray[1].color = sf::Color::Red;
        window.draw(ray);
    }

    // draw map corners
    for (auto&& corner : m_map->getCorners())
    {
        sf::CircleShape dot;
        dot.setRadius(1);
        dot.setPosition(corner);
        dot.setOrigin(dot.getRadius(), dot.getRadius());
        dot.setFillColor(sf::Color::Red);
        window.draw(dot);
    }

    // draw raycasts
    for (auto&& intersection : m_intersections)
    {
        //std::cout << "HERE" << std::endl;
        sf::VertexArray ray(sf::Lines, 2);
        ray[0].position = m_target->getPosition();
        ray[1].position = intersection.position;
        ray[0].color = sf::Color::Red;
        ray[1].color = sf::Color::Red;
        window.draw(ray);
    }

//    // draw polygons
//    sf::ConvexShape polygon;
//    polygon.setPointCount(3);
//    for (int i = 0; i < m_intersections.size(); ++i)
//    {
//        // get neighboor Rey
//        Intersection neighboor = (i == m_intersections.size() - 1) ? m_intersections[0] : m_intersections[i + 1];
//        // create polygon
//        polygon.setPoint(0, getOrigin());
//        polygon.setPoint(1, m_intersections[i].position);
//        polygon.setPoint(2, neighboor.position);
//        polygon.setFillColor(sf::Color(255, 0, 0, 120));
//        window.draw(polygon);
//    }
}

void FOV::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    // apply the transform
    states.transform *= getTransform();
    // draw the vertex array
    target.draw(m_vertices, states);
}

void FOV::getAngles()
{
    // store all angles to raycast at
    m_angles.clear();
//    for (const auto& point : m_map->getCorners())
//    {
//        // limit raycast distance of player view + max room size
//        if (Raycast::getDistance(m_target->getPosition(), point)
//            > (GraphicSettings::G_WINDOW_WIDTH / (GraphicSettings::G_ZOOM_RATIO * 2)
//            + (m_map->getConfig().maxRoomSize * MapSystem::TileSize)))
//            continue ;
//
//        double angle = std::atan2(point.y - m_target->getPosition().y,
//                                  point.x - m_target->getPosition().x);
//
//        m_angles.emplace_back(angle);
//        // adding +/- 0.00001 rad angles to check beyond edges (increase value if flicker)
//        m_angles.emplace_back(angle - ANGLE_MARGIN);
//        m_angles.emplace_back(angle + ANGLE_MARGIN);
//    }
}

void FOV::intersect()
{
    Raycast::Daisy rc;

    m_intersections.clear();
    for (const auto& angle : m_angles)
    {
        // Calculate dx & dy from angle
        double dx = std::cos(angle);
        double dy = std::sin(angle);
        // Ray from camera pos to new pos
        sf::VertexArray ray(sf::Lines, 2);
        ray[0].position = m_target->getPosition();
        ray[1].position = m_target->getPosition() + sf::Vector2f(dx, dy);

        // Find closest intersection
        bool first = true;
        Raycast::Intersection closestIntersect, newIntersection;
        for (const auto &segment : m_map->getSegments())
        {
            newIntersection = rc.cast(ray, segment);
            if (!newIntersection.status) continue;
            if (first || newIntersection.distance < closestIntersect.distance)
            {
                closestIntersect = newIntersection;
            }
            first = false;
        }
        // do not store if empty intersection
        if (closestIntersect.position == sf::Vector2f()) continue;
        // Add to list of intersects
        m_intersections.emplace_back(closestIntersect);
    }
}

void FOV::sortAngles()
{
    std::sort(m_angles.begin(), m_angles.end(), [](double a, double b) { return a < b; });
}

void FOV::updateVertices()
{
    // init vertex array
    m_vertices.setPrimitiveType(sf::Triangles);
    m_vertices.resize(m_intersections.size() * 3);

    for (int i = 0; i < m_intersections.size(); ++i)
    {
        // get neighboor Rey
        Raycast::Intersection neighboor = (i == m_intersections.size() - 1) ?
                                          m_intersections[0] : m_intersections[i + 1];
        // get a pointer to the current triangle's vertices
        sf::Vertex* triangle = &m_vertices[i * 3];
        // define its 3 corners
        triangle[0].position = m_target->getPosition();
        triangle[1].position = m_intersections[i].position;
        triangle[2].position = neighboor.position;
        // set triangle color
        triangle[0].color = m_color;
        triangle[1].color = m_color;
        triangle[2].color = m_color;
    }
}