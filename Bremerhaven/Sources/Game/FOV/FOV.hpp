//
// Created by Pierre Roy on 26/04/18.
//

#ifndef BREMERHAVEN_FOV_HPP
#define BREMERHAVEN_FOV_HPP

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/Graphics/Color.hpp>
#include "../Utils/types.h"
#include "../../Graphics/Raycast/Daisy.hpp"

class FOV : public sf::Drawable, public sf::Transformable
{
private:
    static constexpr float ANGLE_MARGIN = 0.0001f;

private:
    MapSystem::TileMap* m_map;
    AnimatedSprite* m_target;
    std::vector<double> m_angles; // all angles to raycast at
    std::vector<Raycast::Intersection> m_intersections; // raycast resulting intersections
    sf::VertexArray m_vertices;
    sf::Color m_color;

public:
    FOV();

    void init(MapSystem::TileMap*, AnimatedSprite*);

    void update();
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    void intersect();
    void updateVertices();
    void drawDebug(sf::RenderWindow &);

    void getAngles();
    void sortAngles();
};

#endif //BREMERHAVEN_FOV_HPP
