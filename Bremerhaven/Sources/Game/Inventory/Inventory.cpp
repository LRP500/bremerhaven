//
// Created by Pierre Roy on 21/11/17.
//

#include "Inventory.hpp"
#include "../Item/Weapon/Weapon.hpp"

using EntitySystem::Atlas;
using EntitySystem::entityToString;

Inventory::Inventory(json_t *file, EntitySystem::Atlas *mgr)
{
    load<Item>(json_object_get(file, "items"), mgr);
    load<Weapon>(json_object_get(file, "weapons"), mgr);
}

json_t* Inventory::toJson()
{
    json_t* obj = json_object();
    json_object_set_new(obj, "items", jsonArray<Item>());
    json_object_set_new(obj, "weapons", jsonArray<Weapon>());
    return obj;
}

void Inventory::add(std::shared_ptr<const Item> item, int count)
{
    for (auto& it : m_items)
    {
        if (it.first->getId() == item->getId())
        {
            it.second += count;
            return;
        }
    }
    m_items.emplace_back(std::move(item), count);
}

void Inventory::remove(const SharedItem& item, int count)
{
    for (auto it = m_items.begin(); it != m_items.end(); ++it)
    {
        if ((*it).first->getId() == item->getId())
        {
            (*it).second -= count;
            if ((*it).second < 1) m_items.erase(it);
            return ;
        }
    }
}

void Inventory::remove(const std::string& id, int count)
{
    for (auto it = m_items.begin(); it != m_items.end(); ++it)
    {
        if ((*it).first->getId() == id)
        {
            (*it).second -= count;
            if ((*it).second < 1) m_items.erase(it);
            return ;
        }
    }
}

int Inventory::count(const SharedItem& item) const
{
    for (auto it : m_items)
    {
        if (it.first->getId() == item->getId())
            return it.second;
    }
    return 0;
}

int Inventory::count(const std::string &id) const
{
    for (auto it : m_items)
    {
        if (it.first->getId() == id) return it.second;
    }
    return 0;
}

int Inventory::size() const
{
    return static_cast<int>(m_items.size());
}

int Inventory::print(bool label) const
{
    unsigned int i = 0;
    if (m_items.empty()) { std::cout << "Nothing" << std::endl; }
    else
    {
        i += print<Item>(label);
        i += print<Weapon>(label);
    }
    return i;
}

void Inventory::clear()
{
    m_items.clear();
}

void Inventory::merge(Inventory* inventory)
{
    if (inventory == this) return;
    for (auto it : inventory->m_items)
    {
        add(it.first, it.second);
    }
}