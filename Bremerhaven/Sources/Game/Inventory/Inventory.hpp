

#ifndef DANKESTDUNGEON_INVENTORY_HPP
#define DANKESTDUNGEON_INVENTORY_HPP

#include <list>
#include <iostream>
#include <jansson.h>

#include "../Utils/types.h"

class Inventory
{
private:
    std::list<std::pair<std::shared_ptr<const Item>, int>> m_items;

public:
    Inventory(json_t *json, EntitySystem::Atlas* mgr);
    Inventory() = default;

    void add(std::shared_ptr<const Item>, int count);
    void remove(const SharedItem&, int count);
    void remove(const std::string& id, int count);
    void clear();
    void merge(Inventory* inventory);
    int count(const SharedItem& item) const;
    int count(const std::string& id) const;
    int size() const;

    template <typename T>
    int count(unsigned int n) const;

    template <typename T>
    const T* get(unsigned int n) const;

    template <typename T>
    int print(bool label = false) const;

    int print(bool label = false) const;

    json_t* toJson();

private:
    template <typename T>
    void load(json_t *json, EntitySystem::Atlas* mgr);

    template <typename T>
    json_t* jsonArray() const;
};

#include "../Item/Item.hpp"
#include "../Atlas/Atlas.hpp"

template <typename T>
inline int Inventory::count(unsigned int n) const
{
    return count(get<T>(n));
}

template <typename T>
const T* Inventory::get(unsigned int n) const
{
    unsigned int i = 0;
    auto it = m_items.begin();
    for (; it != m_items.end(); ++it)
    {
        if ((*it).first->getId().substr(
                0, EntitySystem::entityToString<T>().size()) != EntitySystem::entityToString<T>())
            continue;
        if (i++ == n) break;
    }
    return (it != m_items.end()) ? dynamic_cast<const T*>((*it).first) : nullptr;
}

template<typename T>
void Inventory::load(json_t *json, EntitySystem::Atlas *mgr)
{
    size_t index;
    json_t *obj;
    json_array_foreach(json, index, obj)
    {
        std::string item = json_string_value(json_array_get(obj, 0));
        json_int_t quantity = json_integer_value(json_array_get(obj, 1));
        m_items.emplace_back(std::move(mgr->getSharedEntity<T>(item)), static_cast<int>(quantity));
    }
}

template <typename T>
json_t* Inventory::jsonArray() const
{
    json_t *json_arr = json_array();
    for (auto item : m_items)
    {
        if (item.first->getId().substr(
                0, EntitySystem::entityToString<T>().size()) != EntitySystem::entityToString<T>())
            continue;
        json_t *pair = json_array();
        json_array_append(pair, json_string(item.first->getId().c_str()));
        json_array_append(pair, json_integer(item.second));
        json_array_append(json_arr, pair);
    }
    return json_arr;
}

template <typename T>
int Inventory::print(bool label) const
{
    unsigned int i = 1;
    for(auto& it : m_items)
    {
        if (it.first->getId().substr(
                0, EntitySystem::entityToString<T>().size()) != EntitySystem::entityToString<T>())
            continue;
        if (label) std::cout << i++ << ": ";
        std::cout << it.first->getName() << " (" << it.second << ") - ";
        std::cout << it.first->getDescription() << std::endl;
    }
    return (i - 1);
}

#endif //DANKESTDUNGEON_INVENTORY_HPP
