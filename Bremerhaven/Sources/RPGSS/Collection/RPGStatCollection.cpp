//
// Created by Pierre Roy on 28/11/17.
//

#include <iostream>

#include "RPGStatCollection.hpp"
#include "../Stat/Modifiable/RPGStatModifiable.hpp"
#include "../Stat/Attribute/RPGAttribute.hpp"
#include "../Stat/Vital/RPGVital.hpp"

using namespace RPGStatSystem;

RPGStatCollection::RPGStatCollection()
{
    m_stats.reserve(STAT_TYPE_COUNT);
}

RPGStatCollection::RPGStatCollection(const RPGStatCollection& rhs)
{
    copy(rhs);
}

template <typename T>
T* RPGStatCollection::getStat(RPGStat::Type type)
{
    return (contains(type)) ? dynamic_cast<T*>(m_stats[type]) : nullptr;
}

template <typename T>
T* RPGStatCollection::createStat(RPGStat::Type type)
{
    m_stats.insert(std::make_pair(type, new T()));
    return getStat<T>(type);
}

template <typename T>
T* RPGStatCollection::getOrCreate(RPGStat::Type type)
{
    return (contains(type)) ? getStat<T>(type) : createStat<T>(type);
}

void RPGStatCollection::configureStats() {}

bool RPGStatCollection::contains(RPGStat::Type type) const
{
    return static_cast<bool>(m_stats.count(type));
}

void RPGStatCollection::addStatModifier(RPGStat::Type target, RPGStatModifier* mod, bool update)
{
    if (contains(target))
    {
        auto modStat = dynamic_cast<IStatModifiable*>(getStat<RPGStat>(target));
        if (modStat)
        {
            modStat->addModifier(mod);
            if (update)
            {
                modStat->updateModifiers();
            }
        }
    }
}

void RPGStatSystem::RPGStatCollection::clearStatModifiers(RPGStat::Type type, bool update)
{
    if (contains(type))
    {
        auto modStat = dynamic_cast<IStatModifiable*>(getStat<RPGStat>(type));
        if (modStat)
        {
            modStat->clearModifiers();
            if (update)
            {
                modStat->updateModifiers();
            }
        }
    }
}

void RPGStatCollection::clearAllStatModifiers(bool update)
{
    for (const auto& stat : m_stats)
    {
        clearStatModifiers(stat.first, update);
    }
}

void RPGStatCollection::updateStatModifier(RPGStat::Type type)
{
    if (contains(type))
    {
        auto modStat = dynamic_cast<IStatModifiable*>(getStat<RPGStat>(type));
        if (modStat)
        {
            modStat->updateModifiers();
        }
    }
}

void RPGStatCollection::updateAllStatModifiers()
{
    for (const auto& stat : m_stats)
    {
        updateStatModifier(stat.first);
    }
}

// copy all stats in collection
const RPGStatCollection& RPGStatCollection::copy(const RPGStatCollection& other)
{
    // check for equality
    if (this == &other) return *this;

    for (const auto& stat : other.m_stats)
    {
        if (stat.first == RPGStat::Health || stat.first == RPGStat::Mana || stat.first == RPGStat::Stamina)
        {
            auto cpy = getOrCreate<RPGVital>(stat.first); // allocate derived stat class pointer
            *cpy = *(dynamic_cast<RPGVital*>(stat.second)); // down cast to derived stat class
        }
        else if (stat.first == RPGStat::Strength || stat.first == RPGStat::Agility || stat.first == RPGStat::Intelligence)
        {
            auto cpy = getOrCreate<RPGAttribute>(stat.first);
            *cpy = *(dynamic_cast<RPGAttribute*>(stat.second));
        }
    }
    return *this;
}

template RPGStat* RPGStatCollection::getStat<RPGStat>(RPGStat::Type type);
template RPGStatModifiable* RPGStatCollection::getStat<RPGStatModifiable>(RPGStat::Type type);
template RPGAttribute* RPGStatCollection::getStat<RPGAttribute>(RPGStat::Type type);
template RPGVital* RPGStatCollection::getStat<RPGVital>(RPGStat::Type type);

template RPGStat* RPGStatCollection::createStat<RPGStat>(RPGStat::Type type);
template RPGStatModifiable* RPGStatCollection::createStat<RPGStatModifiable>(RPGStat::Type type);
template RPGAttribute* RPGStatCollection::createStat<RPGAttribute>(RPGStat::Type type);
template RPGVital* RPGStatCollection::createStat<RPGVital>(RPGStat::Type type);

template RPGStat* RPGStatCollection::getOrCreate<RPGStat>(RPGStat::Type type);
template RPGStatModifiable* RPGStatCollection::getOrCreate<RPGStatModifiable>(RPGStat::Type type);
template RPGAttribute* RPGStatCollection::getOrCreate<RPGAttribute>(RPGStat::Type type);
template RPGVital* RPGStatCollection::getOrCreate<RPGVital>(RPGStat::Type type);