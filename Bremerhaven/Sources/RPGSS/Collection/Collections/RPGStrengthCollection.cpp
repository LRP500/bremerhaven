//
// Created by Pierre Roy on 17/03/18.
//

#include "RPGStrengthCollection.hpp"
#include "../../Stat/Attribute/RPGAttribute.hpp"
#include "../../Modifier/Mods/RPGStatModBaseAdd.hpp"
#include "../../Stat/Vital/RPGVital.hpp"


RPGStatSystem::RPGStrengthCollection::RPGStrengthCollection()
{
    configureStats();
}

void RPGStatSystem::RPGStrengthCollection::configureStats()
{
    RPGStatCollection::configureStats();

    auto health = getOrCreate<RPGVital>(RPGStat::Type::Health);
    health->setName("Health");
    health->setValue(15);
    health->setToMax();

    auto strength = getOrCreate<RPGAttribute>(RPGStat::Type::Strength);
    strength->setName("Strength");
    strength->setValue(7);

    auto agility = getOrCreate<RPGAttribute>(RPGStat::Type::Agility);
    agility->setName("Agility");
    agility->setValue(5);
}
