//
// Created by Pierre Roy on 17/03/18.
//

#ifndef BREMERHAVEN_RPGSTRENGTHCOLLECTION_HPP
#define BREMERHAVEN_RPGSTRENGTHCOLLECTION_HPP

#include "../RPGStatCollection.hpp"

namespace RPGStatSystem
{
    class RPGStrengthCollection : public RPGStatCollection
    {
    public:
        RPGStrengthCollection();

    private:
        void configureStats() override;
    };
}


#endif //BREMERHAVEN_RPGSTRENGTHCOLLECTION_HPP
