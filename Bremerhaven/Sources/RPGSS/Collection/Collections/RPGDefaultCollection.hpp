//
// Created by Pierre Roy on 17/03/18.
//

#ifndef BREMERHAVEN_RPGDEPRIVEDCOLLECTION_HPP
#define BREMERHAVEN_RPGDEPRIVEDCOLLECTION_HPP

#include "../RPGStatCollection.hpp"

namespace RPGStatSystem
{
    class RPGDefaultCollection : public RPGStatCollection
    {
    public:
        RPGDefaultCollection();

    private:
        void configureStats() override;
    };
}

#endif //BREMERHAVEN_RPGDEPRIVEDCOLLECTION_HPP
