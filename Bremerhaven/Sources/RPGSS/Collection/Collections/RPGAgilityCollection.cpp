//
// Created by Pierre Roy on 17/03/18.
//

#include "RPGAgilityCollection.hpp"
#include "../../Stat/Attribute/RPGAttribute.hpp"
#include "../../Stat/Vital/RPGVital.hpp"
#include "../../Modifier/Mods/RPGStatModBaseAdd.hpp"

RPGStatSystem::RPGAgilityCollection::RPGAgilityCollection()
{
    configureStats();
}

void RPGStatSystem::RPGAgilityCollection::configureStats()
{
    RPGStatCollection::configureStats();

    auto health = getOrCreate<RPGVital>(RPGStat::Type::Health);
    health->setName("Health");
    health->setValue(15);
    health->setToMax();

    auto strength = getOrCreate<RPGAttribute>(RPGStat::Type::Strength);
    strength->setName("Strength");
    strength->setValue(5);

    auto agility = getOrCreate<RPGAttribute>(RPGStat::Type::Agility);
    agility->setName("Agility");
    agility->setValue(7);
}
