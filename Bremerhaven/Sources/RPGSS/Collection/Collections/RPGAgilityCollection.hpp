//
// Created by Pierre Roy on 17/03/18.
//

#ifndef BREMERHAVEN_RPGAGILITYCOLLECTION_HPP
#define BREMERHAVEN_RPGAGILITYCOLLECTION_HPP

#include "../RPGStatCollection.hpp"

namespace RPGStatSystem
{
    class RPGAgilityCollection : public RPGStatCollection
    {
    public:
        RPGAgilityCollection();

    private:
        void configureStats() override;
    };
}

#endif //BREMERHAVEN_RPGAGILITYCOLLECTION_HPP
