//
// Created by Pierre Roy on 28/11/17.
//

#include "RPGStat.hpp"

RPGStatSystem::RPGStat::RPGStat() : m_name(""), m_baseValue(0)
{}

const RPGStatSystem::RPGStat& RPGStatSystem::RPGStat::copy(const RPGStatSystem::RPGStat& other)
{
    // check for equality
    if (this == &other) return *this;

    m_name = other.m_name;
    m_baseValue = other.m_baseValue;

    return *this;
}
