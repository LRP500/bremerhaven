//
// Created by Pierre Roy on 02/12/17.
//

#ifndef RPGSTATSYSTEM_RPGVITAL_HPP
#define RPGSTATSYSTEM_RPGVITAL_HPP

#include "../../Stat/Attribute/RPGAttribute.hpp"
#include "../../Interface/IStatCurrentValueChange.hpp"

namespace RPGStatSystem
{

    class RPGVital : public RPGAttribute, public IStatCurrentValueChange
    {
    private:
        mutable int m_currentValue;

    public:
        RPGVital();

        void setCurrentValue(int value);
        void setToMax();

        unsigned int getCurrentValue() const;

        bool full() const;

        void triggerCurrentValueChange();
    };

}

#endif //RPGSTATSYSTEM_RPGVITAL_HPP
