//
// Created by Pierre Roy on 02/12/17.
//

#include <iostream>

#include "RPGVital.hpp"

RPGStatSystem::RPGVital::RPGVital() : m_currentValue(0)
{}

void RPGStatSystem::RPGVital::setCurrentValue(int value)
{
    if (value != m_currentValue)
    {
        m_currentValue = value;
        triggerCurrentValueChange();
    }
}

unsigned int RPGStatSystem::RPGVital::getCurrentValue() const
{
    if (m_currentValue > getValue())
    {
        m_currentValue = getValue();
    }
    else if (m_currentValue < 0)
    {
        m_currentValue = 0;
    }
    return static_cast<unsigned int>(m_currentValue);
}

void RPGStatSystem::RPGVital::setToMax()
{
    m_currentValue = getValue();
}

void RPGStatSystem::RPGVital::triggerCurrentValueChange()
{
    if (!listenTokens.empty()) OnCurrentValueChange(*this);
}

bool RPGStatSystem::RPGVital::full() const
{
    return m_currentValue == getValue();
}