//
// Created by Pierre Roy on 26/03/18.
//

#ifndef BASEENGINE_TEXTUREMANAGER_HPP
#define BASEENGINE_TEXTUREMANAGER_HPP

#include <unordered_map>

#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "../../System/Logger/Logger.hpp"
#include "../../Game/Collision/Collision.h"

// Resource storage for Font, Texture and SoundBuffer
// sf::Music not compatible as it uses openFromFile()
template <typename Resource>
class ResourceHolder
{
private:
    typedef std::shared_ptr<Resource> SharedResource;
    std::unordered_map<std::string, SharedResource> m_resources;

public:
    ResourceHolder() = default;

    void load(const std::string &id, const std::string &filename);

    // Overload ResourceHolder::load for shader compatibility
    template <typename Parameter>
    void load(const std::string &id, const std::string &filename, const Parameter&);

    const Resource& get(const std::string&) const;
    Resource& get(const std::string&);
    std::shared_ptr<Resource> getRef(const std::string&) const;
};

template <typename Resource>
void ResourceHolder<Resource>::load(const std::string& id, const std::string &filename)
{
    // Create Resource from specified file
    std::unique_ptr<Resource> resource(new Resource());
    if (!resource->loadFromFile(filename))
    {
        // TODO What do we want ? Exceptions. When do we want it ? SFML3
        throw std::runtime_error("ResourceHolder::load - Failed to Load from " + filename);
    }
    // Add newly created Resource to map
    m_resources.insert(std::make_pair(id, std::move(resource)));
}

template<typename Resource>
template<typename Parameter>
void ResourceHolder<Resource>::load(const std::string &id,
                                    const std::string& filename,
                                    const Parameter& param)
{
    std::unique_ptr<Resource> resource(new Resource());
    if (!resource->loadFromFile(filename, param))
    {
        throw std::runtime_error("ResourceHolder::load - Failed to Load from " + filename);
    }
    m_resources.insert(std::make_pair(std::move(id), std::move(resource)));
}

template <typename Resource>
const Resource& ResourceHolder<Resource>::get(const std::string& id) const
{
    auto result = m_resources.find(id);
    if (result == m_resources.end())
    {
        Logger::Write(Logger::ERROR, "ResourceHolder::get - Failed to retrieve " + id);
    }
    return (*result->second);
}

template <typename Resource>
Resource& ResourceHolder<Resource>::get(const std::string& id)
{
    return const_cast<Resource&>(static_cast<const ResourceHolder*>(this)->get(id));
}

template<typename Resource>
std::shared_ptr<Resource>
ResourceHolder<Resource>::getRef(const std::string &id) const
{
    auto result = m_resources.find(id);
    if (result == m_resources.end())
    {
        Logger::Write(Logger::ERROR, "ResourceHolder::get - Failed to retrieve " + id);
    }
    return result->second;
}

// Template explicit specialization for sf::Texture
template <>
inline void ResourceHolder<sf::Texture>::load(const std::string& id, const std::string &filename)
{
    // Create Resource from specified file
    std::unique_ptr<sf::Texture> resource(new sf::Texture());
    if (!(Collision::CreateTextureAndBitmask(*resource, filename)))
    {
        throw std::runtime_error("ResourceHolder::load - Failed to Load from " + filename);
    }
    // Add newly created Resource to map
    m_resources.insert(std::make_pair(id, std::move(resource)));
}


#endif //BASEENGINE_TEXTUREMANAGER_HPP
