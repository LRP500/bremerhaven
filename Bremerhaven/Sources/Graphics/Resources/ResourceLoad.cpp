//
// Created by Pierre Roy on 25/03/18.
//

#include "../../Game/FSM/GSManager.hpp"
#include "../../Game/FSM/States/GSGameplay.hpp"
#include "../../Game/Collision/Collision.h"
#include "../../Game/Item/Armor/Armor.hpp"
#include "../../Game/Item/Items/Medkit.hpp"
#include "../../Game/Item/Weapon/Weapon.hpp"
#include "../../Game/Item/Weapon/Behaviors/PistolFiringBehavior.hpp"
#include "../../Game/Item/Weapon/Behaviors/ShotgunFiringBehavior.hpp"

void FSM::GSGameplay::loadEntities()
{
    if (!(m_atlas.loadJson<Item>(GSManager::JsonDirectoryPath + "items.json")) ||
        !(m_atlas.loadJson<Medkit>(GSManager::JsonDirectoryPath + "medkits.json")) ||
        !(m_atlas.loadJson<Armor>(GSManager::JsonDirectoryPath + "armors.json")) ||
        !(m_atlas.loadJson<Weapon>(GSManager::JsonDirectoryPath + "weapons.json")) ||
        !(m_atlas.loadJson<EntitySystem::Creature>(GSManager::JsonDirectoryPath + "creatures.json")))
        Logger::Write(Logger::ERROR, "Error loading JSON files");

    // Generate item pools
    m_atlas.generatePools();
}

void FSM::GSManager::loadTextures()
{
    // player textures
    std::string path = GSManager::AssetDirectoryPath + "/Textures/Player/";
    m_textures.load("player_animation_sheet", path + "player_animation_sheet.png");
    m_textures.load("reticule", path + "reticule.png");
    m_textures.get("player_animation_sheet").setSmooth(true);
    m_textures.get("reticule").setSmooth(true);

    // map textures
    path = GSManager::AssetDirectoryPath + "/Textures/Map/";
    m_textures.load("tile_map_sheet", path + "tile_map_sheet.png");

    // items textures
    path = GSManager::AssetDirectoryPath + "/Textures/Items/";
    m_textures.load("item_key", path + "item_key.png");
    m_textures.load("item_key_armory", path + "item_key_armory.png");
    m_textures.load("item_key_infirmary", path + "item_key_infirmary.png");
    m_textures.load("item_medecine_medkit", path + "item_medecine_medkit.png");
    m_textures.get("item_key").setSmooth(true);
    m_textures.get("item_key_armory").setSmooth(true);
    m_textures.get("item_key_infirmary").setSmooth(true);
    m_textures.get("item_medecine_medkit").setSmooth(true);

    // armors textures
    path = GSManager::AssetDirectoryPath + "/Textures/Armors/";
    m_textures.load("armor_default", path + "armor_default.png");
    m_textures.get("armor_default").setSmooth(true);

    // weapons textures
    path = GSManager::AssetDirectoryPath + "/Textures/Weapons/";
    m_textures.load("weapon_smg", path + "weapon_smg.png");
    m_textures.load("weapon_shotgun", path + "weapon_shotgun.png");
    m_textures.load("weapon_revolver", path + "weapon_revolver.png");
    m_textures.load("weapon_sawed_off", path + "weapon_sawed_off.png");
    m_textures.load("weapon_pistol", path + "weapon_pistol.png");
    m_textures.load("base_projectile", path + "base_projectile.png");
    m_textures.get("weapon_smg").setSmooth(true);
    m_textures.get("weapon_shotgun").setSmooth(true);
    m_textures.get("weapon_revolver").setSmooth(true);
    m_textures.get("weapon_sawed_off").setSmooth(true);
    m_textures.get("weapon_pistol").setSmooth(true);
    m_textures.get("base_projectile").setSmooth(true);
}

void FSM::GSManager::loadFonts()
{
    const std::string path = GSManager::AssetDirectoryPath + "/Fonts/";
    m_fonts.load("main_font", path + "TheLightFont.otf");
}

void FSM::GSManager::loadSounds()
{
    const std::string path = GSManager::AssetDirectoryPath + "/Sounds/";
}