//
// Created by Pierre Roy on 26/04/18.
//

#include <cmath>

#include "Daisy.hpp"

using namespace Raycast;

Intersection Daisy::cast(const sf::VertexArray& ray, const sf::VertexArray& segment)
{
    reset(); // reset resulting intersection

    // RAY in parametric: Point + Delta * T1
    float r_px = ray[0].position.x;
    float r_py = ray[0].position.y;
    float r_dx = ray[1].position.x - ray[0].position.x;
    float r_dy = ray[1].position.y - ray[0].position.y;

    // SEGMENT in parametric: Point + Delta * T2
    float s_px = segment[0].position.x;
    float s_py = segment[0].position.y;
    float s_dx = segment[1].position.x - segment[0].position.x;
    float s_dy = segment[1].position.y - segment[0].position.y;

    // check if parallel
    float r_mag = std::sqrt(r_dx * r_dx + r_dy * r_dy);
    float s_mag = std::sqrt(s_dx * s_dx + s_dy * s_dy);

    // unit vectors are the same
    if ((r_dx / r_mag == s_dx / s_mag) && (r_dy / r_mag == s_dy / s_mag))
        return m_result;

    // solve distances
    auto T2 = (r_dx * (s_py - r_py) + r_dy * (r_px - s_px)) / (s_dx * r_dy - s_dy * r_dx);
    auto T1 = (s_px + s_dx * T2 - r_px) / r_dx;

    if (T1 < 0) return m_result;
    if (T2 < 0 || T2 > 1) return m_result;

    m_result.position = sf::Vector2f(r_px + r_dx * T1, r_py + r_dy * T1);
    m_result.distance = T1;
    m_result.status = true;

    return m_result;
}

void Daisy::reset()
{
    m_result.position = sf::Vector2f();
    m_result.distance = 0.0f;
    m_result.status = false;
}

float Raycast::getDistance(const sf::Vector2f& a, const sf::Vector2f& b)
{
    return (std::hypot((a.x - b.x), (a.y - b.y)));
}