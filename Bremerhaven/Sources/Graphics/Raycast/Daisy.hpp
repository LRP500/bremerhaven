//
// Created by Pierre Roy on 26/04/18.
//

#ifndef BREMERHAVEN_RAYCASTER_HPP
#define BREMERHAVEN_RAYCASTER_HPP

#include <SFML/Graphics/VertexArray.hpp>

namespace Raycast
{
    // tool functions
    float getDistance(const sf::Vector2f&, const sf::Vector2f&);


    // intersection Data
    struct Intersection
    {
        sf::Vector2f position = sf::Vector2f();
        float distance = 0.0f;
        float status = false;
    };

    // Rey caster
    class Daisy
    {
    private:
        Intersection m_result;

    public:
        Daisy() = default;

        Intersection cast(const sf::VertexArray&, const sf::VertexArray&);
        void reset();
    };
}

#endif //BREMERHAVEN_RAYCASTER_HPP
