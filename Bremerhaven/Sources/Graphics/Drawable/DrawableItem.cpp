//
// Created by Pierre Roy on 29/04/18.
//

#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

#include "DrawableItem.hpp"
#include "../GUI/Anchor.hpp"

DrawableItem::DrawableItem(const Item* item,
                           const sf::Texture& tex,
                           int tileSize,
                           const sf::Vector2f& pos)
        : Item(*item)
{
    init(item, tex, tileSize, pos);
}

void DrawableItem::init(const Item* item,
                        const sf::Texture& tex,
                        int tileSize,
                        const sf::Vector2f& pos)
{
    // transform
    m_sprite.setTexture(tex);
    m_sprite.setScale(static_cast<float>(tileSize) / tex.getSize().x,
                      static_cast<float>(tileSize) / tex.getSize().y);

    // set position
    auto gb = m_sprite.getGlobalBounds();
    m_sprite.setOrigin(gb.width / 2, gb.height / 2);
    m_sprite.setPosition(pos);

    // init collider
    m_collider.setSize(sf::Vector2f(gb.width, gb.height) / 3.f);
    m_collider.setOrigin(Anchor::Local::getCenter(m_collider));
    m_collider.setPosition(Anchor::Global::getCenter(m_sprite));
}

void DrawableItem::draw(sf::RenderWindow &window)
{
    window.draw(m_sprite);
    window.draw(m_collider);
}

sf::Sprite &DrawableItem::getSprite()
{
    return const_cast<sf::Sprite&>(static_cast<const DrawableItem*>(this)->getSprite());
}

