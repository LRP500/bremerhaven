//
// Created by Pierre Roy on 04/04/18.
//

#include <SFML/Graphics/RenderTarget.hpp>

#include "DrawableGroup.hpp"

DrawableGroup::DrawableGroup() : m_drawables {}
{}

void DrawableGroup::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    for(const auto& drawable : m_drawables)
    {
        target.draw(drawable, states);
    }
}

std::size_t DrawableGroup::push_back(const sf::Drawable &drawable)
{
    m_drawables.emplace_back(drawable);
    return m_drawables.size() - 1;
}

const sf::Drawable &DrawableGroup::pop_back()
{
    const auto& drawable = m_drawables.back();
    m_drawables.pop_back();
    return drawable;
}

const sf::Drawable &DrawableGroup::operator[](std::size_t index)
{
    return m_drawables[index];
}
