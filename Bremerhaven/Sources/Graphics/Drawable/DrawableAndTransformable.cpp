//
// Created by Pierre Roy on 22/05/18.
//

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "DrawableAndTransformable.hpp"

DrawableAndTransformable::DrawableAndTransformable()
        : m_texture(nullptr),
          m_textureRect()
{}

DrawableAndTransformable::DrawableAndTransformable(SharedTexture texture)
        : m_texture(nullptr),
          m_textureRect()
{
    setTexture(texture);
}

DrawableAndTransformable::DrawableAndTransformable(SharedTexture texture, const sf::IntRect& rect)
        : m_texture(nullptr),
          m_textureRect()
{
    setTexture(texture);
    setTextureRect(rect);
}

sf::FloatRect DrawableAndTransformable::getLocalBounds() const
{
    float width = static_cast<float>(std::abs(m_textureRect.width));
    float height = static_cast<float>(std::abs(m_textureRect.height));

    return sf::FloatRect(0.f, 0.f, width, height);
}

sf::FloatRect DrawableAndTransformable::getGlobalBounds() const
{
    return getTransform().transformRect(getLocalBounds());
}

void DrawableAndTransformable::setTextureRect(const sf::IntRect& rectangle)
{
    if (rectangle != m_textureRect)
    {
        m_textureRect = rectangle;
        updatePositions();
        updateTexCoords();
    }
}

void DrawableAndTransformable::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    if (m_texture)
    {
        states.transform *= getTransform();
        states.texture = &(*m_texture);
        target.draw(m_vertices, 4, sf::TriangleStrip, states);
    }
}

void DrawableAndTransformable::updatePositions()
{
    sf::FloatRect bounds = getLocalBounds();

    m_vertices[0].position = sf::Vector2f(0, 0);
    m_vertices[1].position = sf::Vector2f(0, bounds.height);
    m_vertices[2].position = sf::Vector2f(bounds.width, 0);
    m_vertices[3].position = sf::Vector2f(bounds.width, bounds.height);
}

void DrawableAndTransformable::updateTexCoords()
{
    float left   = static_cast<float>(m_textureRect.left);
    float right  = left + m_textureRect.width;
    float top    = static_cast<float>(m_textureRect.top);
    float bottom = top + m_textureRect.height;

    m_vertices[0].texCoords = sf::Vector2f(left, top);
    m_vertices[1].texCoords = sf::Vector2f(left, bottom);
    m_vertices[2].texCoords = sf::Vector2f(right, top);
    m_vertices[3].texCoords = sf::Vector2f(right, bottom);
}

void DrawableAndTransformable::setTexture(SharedTexture texture, bool resetRect)
{
    if (resetRect || (!m_texture && (m_textureRect == sf::IntRect())))
        setTextureRect(sf::IntRect(0, 0, (*texture).getSize().x, (*texture).getSize().y));

    m_texture = std::move(texture);
}