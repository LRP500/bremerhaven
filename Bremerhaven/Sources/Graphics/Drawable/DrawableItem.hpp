//
// Created by Pierre Roy on 29/04/18.
//

#ifndef BREMERHAVEN_DRAWABLEITEM_HPP
#define BREMERHAVEN_DRAWABLEITEM_HPP

#include <SFML/Graphics/Sprite.hpp>

#include "../../Game/Item/Item.hpp"
#include "../../Game/Collision/RectangleCollider.hpp"

class DrawableItem : public Item
{
private:
    sf::Sprite m_sprite;
    RectangleCollider m_collider;

public:
    DrawableItem(const Item*, const sf::Texture&, int tileSize, const sf::Vector2f& = {});

    void init(const Item*, const sf::Texture&, int tileSize, const sf::Vector2f&);
    void draw(sf::RenderWindow&);

    const RectangleCollider& getCollider() const { return m_collider; }

    const sf::Sprite& getSprite() const { return m_sprite; };
    sf::Sprite& getSprite();

public:
    // allow erase-remove idiom
    bool operator==(const DrawableItem& rhs)
    { return m_sprite.getPosition() == rhs.m_sprite.getPosition(); }
};


#endif //BREMERHAVEN_DRAWABLEITEM_HPP
