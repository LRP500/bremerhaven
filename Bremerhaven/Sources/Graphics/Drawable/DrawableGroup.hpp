//
// Created by Pierre Roy on 04/04/18.
//

#ifndef BREMERHAVEN_GROUP_HPP
#define BREMERHAVEN_GROUP_HPP

#include <SFML/Graphics/Drawable.hpp>

#include "../../Game/Utils/types.h"

class DrawableGroup : public sf::Drawable
{
private:
    std::vector<std::reference_wrapper<const sf::Drawable>> m_drawables;

public:
    DrawableGroup();
    ~DrawableGroup() override = default;

    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    const sf::Drawable& operator[](std::size_t index);
    std::size_t push_back(const sf::Drawable& drawable);
    const sf::Drawable& pop_back();
};

#endif //BREMERHAVEN_GROUP_HPP
