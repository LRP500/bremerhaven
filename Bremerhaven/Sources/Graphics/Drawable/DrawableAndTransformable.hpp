//
// Created by Pierre Roy on 22/05/18.
//

#ifndef BREMERHAVEN_DRAWABLEANDTRANSFORMABLE_HPP
#define BREMERHAVEN_DRAWABLEANDTRANSFORMABLE_HPP

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include "../../Game/Utils/types.h"

class DrawableAndTransformable : public sf::Drawable, public sf::Transformable
{
private:
    SharedTexture m_texture;
    sf::Vertex m_vertices[4];
    sf::IntRect m_textureRect;

public:
    DrawableAndTransformable();
    explicit DrawableAndTransformable(SharedTexture texture);
    DrawableAndTransformable(SharedTexture texture, const sf::IntRect&);

    void setTexture(SharedTexture texture, bool resetRect = false);
    void setTextureRect(const sf::IntRect& rectangle);
    const sf::Texture* getTexture() const { return m_texture.get(); }

    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    void updatePositions();
    void updateTexCoords();

    sf::FloatRect getLocalBounds() const;
    sf::FloatRect getGlobalBounds() const;
};


#endif //BREMERHAVEN_DRAWABLEANDTRANSFORMABLE_HPP
