//
// Created by Pierre Roy on 08/09/2018.
//

#ifndef BREMERHAVEN_ARMORBAR_HPP
#define BREMERHAVEN_ARMORBAR_HPP

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Sprite.hpp>

namespace GUI
{
    class ArmorGUIPanel : public sf::Transformable
    {
    private:
        std::vector<sf::Sprite> m_sprites;
        unsigned int m_amount;

    public:
        ArmorGUIPanel();

        void init(const sf::Texture&);
        void draw(sf::RenderWindow&);

        void setArmor(unsigned int amount);

    private:
        void update();
    };
}

#endif //BREMERHAVEN_ARMORBAR_HPP
