//
// Created by Pierre Roy on 08/09/2018.
//

#include <SFML/Graphics/RenderWindow.hpp>
#include "ArmorGUIPanel.hpp"

GUI::ArmorGUIPanel::ArmorGUIPanel()
{
    m_sprites.resize(3);
}

void GUI::ArmorGUIPanel::init(const sf::Texture& texture)
{
    for (auto& sprite : m_sprites)
    {
        sprite.setTexture(texture);
        sprite.setScale(this->getScale());
    }

    auto gb = m_sprites[0].getGlobalBounds();
    setPosition(getPosition().x, getPosition().y - gb.height);
    sf::Vector2f pos(this->getPosition());
    for (auto& sprite : m_sprites)
    {
        sprite.setPosition(pos);
        pos.x += gb.width * 0.8;
    }
}

void GUI::ArmorGUIPanel::update()
{
    unsigned int index = 1;
    for (auto& sprite : m_sprites)
    {
        if (index <= m_amount) sprite.setColor(sf::Color::Black);
        else sprite.setColor(sf::Color::Transparent);
        index += 1;
    }
}

void GUI::ArmorGUIPanel::draw(sf::RenderWindow& window)
{
    // draw each sprite separately
    for (auto&& sprite : m_sprites) window.draw(sprite);
}

void GUI::ArmorGUIPanel::setArmor(unsigned int amount)
{
    m_amount = amount;
    update();
}