//
// Created by Pierre Roy on 08/06/18.
//

#ifndef BREMERHAVEN_HEALTHBAR_HPP
#define BREMERHAVEN_HEALTHBAR_HPP

#include <SFML/Graphics/RectangleShape.hpp>

namespace GUI
{
    class HealthBar
    {
    private:
        sf::RectangleShape m_container;
        sf::RectangleShape m_content;

    public:
        HealthBar();

        void draw(sf::RenderWindow&);

        void setHealth(unsigned int max, unsigned int current);
        void setPosition(const sf::Vector2f& pos);
        const sf::Vector2f& getPosition() { return m_container.getPosition(); }

    private:
        void init();
    };
}

#endif //BREMERHAVEN_HEALTHBAR_HPP
