//
// Created by Pierre Roy on 29/03/18.
//

#ifndef BASEENGINE_MENU_HPP
#define BASEENGINE_MENU_HPP

#include <SFML/System/Vector2.hpp>

#include "../../Game/Utils/types.h"

namespace GUI
{
    static const int DefaultFontSize = 32;
    static const std::string DefaultFont = "main_font";

    enum Direction { Up, Down, Left, Right, Count };

    class Menu
    {
    private:
        static const int ItemReserve = 100; // max item number allowed

    private:
        std::string m_name;

        SharedFont m_font;
        unsigned int m_fontSize;

        std::vector<TextItem> m_items;
        unsigned int m_current;

        sf::Vector2f m_dimensions; // menu size
        sf::Vector2f m_position;
        float m_padding;

        bool m_isFocused;
        bool m_isMultiChoice;
        bool m_isHorizontal;

    private:
        std::vector<std::string> m_transitions;

    public:
        Menu();
        explicit Menu(std::shared_ptr<const sf::Font>,
                      std::string&& name = std::string(),
                      sf::Vector2f&& dimensions = {0, 0},
                      sf::Vector2f&& position = {0, 0},
                      float padding = 0.0f,
                      bool horizontal = false);

        void draw(sf::RenderWindow& window);
        void update(const sf::Time dt);

        bool moveUp();
        bool moveDown();
        bool moveLeft();
        bool moveRight();

        void setFont(std::shared_ptr<const sf::Font>);
        void setFontSize(unsigned int size) { m_fontSize = size; }
        unsigned int getFontSize() const { return m_fontSize; }
        void setPadding(float padding);
        void setDimensions(sf::Vector2f&& size) { m_dimensions = size; }
        void setDimensions(float width, float height) { m_dimensions = {width, height}; }
        const sf::Vector2f& getDimensions() const { return m_dimensions; }
        void setPosition(sf::Vector2f&& position) { m_position = position; }
        void setPosition(float x, float y) { m_position = {x, y}; }
        const sf::Vector2f getPosition() const { return m_position; }

        bool addItem(const std::string& name);
        TextItem* getItem(const std::string& name);
        void setToStyle(sf::Text* text);
        void setHorizontal() { m_isHorizontal = true; }

        void setCurrent(unsigned int index) { m_current = index; }

        void focus(bool state) { m_isFocused = state; }

        void resetHighlight();
        void resetSelection();

        bool isMultiChoice() const { return m_isMultiChoice; }

        TextItem* getCurrent();
        unsigned int getCurrentIndex() const { return m_current; }

        void addTransition(Direction, const std::string&);
        const std::string& getTransition(Direction direction) const;
        void setName(std::string&& name) { m_name = std::move(name); }
        const std::string& getName() { return m_name; }

    public:
        bool operator==(const Menu& other) { return this->m_name == other.m_name; }
    };
}

#endif //BASEENGINE_MENU_HPP
