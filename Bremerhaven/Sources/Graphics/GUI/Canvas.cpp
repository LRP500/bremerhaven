//
// Created by Pierre Roy on 05/04/18.
//

#include <SFML/System/Time.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

#include "Canvas.hpp"
#include "Menu.hpp"
#include "TextItem.hpp"

using namespace GUI;

Canvas::Canvas(sf::Vector2f &&position, sf::Vector2f&& dimensions)
        : m_position(position), m_dimensions(dimensions)
{}

void Canvas::draw(sf::RenderWindow &window)
{
    for (auto &m_item : m_items)
    {
        m_item.draw(window);
    }

    // draw rect (DEBUG)
    sf::RectangleShape rect;
    rect.setSize(m_dimensions);
    rect.setPosition(m_position);
    rect.setFillColor(sf::Color::Transparent);
    rect.setOutlineThickness(1);
    rect.setOutlineColor(sf::Color::Red);
    window.draw(rect);
}

void Canvas::update(const sf::Time dt)
{
    for (auto&& menu : m_items)
    {
        menu.update(dt);
    }
}

void Canvas::moveUp()
{
    if (!m_items[m_selected].moveUp())
    {
        if (m_items[m_selected].getTransition(Up).empty())
            return ;
        navigateTo(m_items[m_selected].getTransition(Up));
    }
}

void Canvas::moveDown()
{
    if (!m_items[m_selected].moveDown())
    {
        if (m_items[m_selected].getTransition(Down).empty())
            return ;
        navigateTo(m_items[m_selected].getTransition(Down));
    }
}

void Canvas::moveLeft()
{
    if (!m_items[m_selected].moveLeft())
    {
        if (m_items[m_selected].getTransition(Left).empty())
            return ;
        navigateTo(m_items[m_selected].getTransition(Left));
    }
}

void Canvas::moveRight()
{
    if (!m_items[m_selected].moveRight())
    {
        if (m_items[m_selected].getTransition(Right).empty())
            return ;
        navigateTo(m_items[m_selected].getTransition(Right));
    }
}

void Canvas::addItem(Menu item)
{
    m_items.emplace_back(std::move(item));
}

void Canvas::navigateTo(const std::string& item)
{
    for (unsigned int i = 0; i < m_items.size(); ++i)
    {
        if (m_items[i].getName() == item)
        {
            m_items[m_selected].focus(false);
            m_selected = i;
            m_items[m_selected].focus(true);
        }
    }
}

Menu* Canvas::getItem(const std::string &name)
{
    for (auto&& item : m_items)
    {
        if (item.getName() == name)
            return &item;
    }
    return nullptr;
}

const std::string &Canvas::getCurrentId()
{
    return  m_items[m_selected].getName();
}
