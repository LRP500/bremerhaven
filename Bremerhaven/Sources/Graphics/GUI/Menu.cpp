//
// Created by Pierre Roy on 29/03/18.
//

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

#include "Menu.hpp"
#include "TextItem.hpp"
#include "../Utils/style.h"

using GUI::Menu;

Menu::Menu() : GUI::Menu(nullptr)
{}

Menu::Menu(std::shared_ptr<const sf::Font> font,
           std::string&& name,
           sf::Vector2f&& dimensions,
           sf::Vector2f&& position,
           float padding,
           bool horizontal)
        : m_name(std::move(name)),
          m_font(std::move(font)),
          m_fontSize(DefaultFontSize),
          m_current(0),
          m_dimensions(dimensions),
          m_position(position),
          m_padding(padding),
          m_isFocused(true),
          m_isMultiChoice(false),
          m_isHorizontal(horizontal)
{
    // limit item vector capacity
    m_items.reserve(ItemReserve);

    // initialize transitions
    m_transitions.reserve(4);
    for (int i = 0; i < Direction::Count; ++i)
    {
        m_transitions.emplace_back();
    }
}

void Menu::setFont(std::shared_ptr<const sf::Font> font)
{
    if (font != m_font)
    {
        m_font = std::move(font);
    }
}

void Menu::draw(sf::RenderWindow& window)
{
    sf::Vector2f padding(m_dimensions.x * m_padding / 100,
                         m_dimensions.y * m_padding / 100);

    if (!m_isHorizontal)
    {
        for (int i = 0; i < m_items.size(); ++i)
        {
            m_items[i].getText()->setPosition(sf::Vector2f(
                    m_position.x + padding.x
                    + (m_dimensions.x - (padding.x * 2)) / 2,
                    m_position.y + padding.y
                    + (m_dimensions.y - (padding.y * 2)) / (m_items.size() + 1) * (i + 1)
            ));
            m_items[i].draw(window);
        }
    }
    else
    {
        for (int i = 0; i < m_items.size(); ++i)
        {
            m_items[i].getText()->setPosition(sf::Vector2f(
                    m_position.x + padding.x
                    + (m_dimensions.x - (padding.x * 2)) / (m_items.size() + 1) * (i + 1),

                    m_position.y + padding.y
                    + (m_dimensions.y - (padding.y * 2)) / 2
            ));
            m_items[i].draw(window);
        }
    }
}

void GUI::Menu::update(const sf::Time dt)
{
    resetHighlight();
    if (m_isFocused) m_items[m_current].highlight(true);
}

bool Menu::addItem(const std::string &name)
{
    if (m_items.size() + 1 > ItemReserve)
        return false;

    TextItem item;
    item.getText()->setString(name);
    setToStyle(item.getText());

    m_items.emplace_back(std::move(item));

    return true;
}

bool Menu::moveUp()
{
    if (m_isHorizontal)
        return false;

    unsigned int i = m_current;
    while (i)
    {
        --i;
        if (m_items[i].isLocked()) continue;
        else if (i != m_current)
        {
            setCurrent(i);
            return true;
        }
    }
    return false;
}

bool Menu::moveDown()
{
    if (m_isHorizontal)
        return false;

    unsigned int i = m_current;
    while (i < m_items.size() - 1)
    {
        ++i;
        if (m_items[i].isLocked()) continue;
        else if (i != m_current)
        {
            setCurrent(i);
            return true;
        }
    }
    return false;
}

bool Menu::moveLeft()
{
    if (!m_isHorizontal)
        return false;

    unsigned int i = m_current;
    while (i)
    {
        --i;
        if (m_items[i].isLocked()) continue;
        else if (i != m_current)
        {
            setCurrent(i);
            return true;
        }
    }
    return false;
}

bool Menu::moveRight()
{
    if (!m_isHorizontal)
        return false;

    unsigned int i = m_current;
    while (i < m_items.size() - 1)
    {
        ++i;
        if (m_items[i].isLocked()) continue;
        else if (i != m_current)
        {
            setCurrent(i);
            return true;
        }
    }
    return false;
}

void Menu::addTransition(Direction direction, const std::string& item)
{
    m_transitions[direction] = item;
}

const std::string& Menu::getTransition(Direction direction) const
{
    return m_transitions[direction];
}

void Menu::setToStyle(sf::Text* text)
{
    // TODO style struct or class
    text->setFont(*m_font);
    text->setFillColor(sf::Color::White);
    text->setCharacterSize(m_fontSize);
}

void Menu::setPadding(float padding)
{
    m_padding = padding;
}

void GUI::Menu::resetHighlight()
{
    for (auto&& item : m_items)
    {
        item.highlight(false);
    }
}

void GUI::Menu::resetSelection()
{
    for (auto&& item : m_items)
    {
        item.select(false);
    }
}

GUI::TextItem *GUI::Menu::getItem(const std::string &name)
{
    for (auto&& item : m_items)
    {
        if (std::string(item.getText()->getString()) == name)
            return &item;
    }
    return nullptr;
}

GUI::TextItem *GUI::Menu::getCurrent()
{
    return &m_items[m_current];
}

