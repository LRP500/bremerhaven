//
// Created by Pierre Roy on 08/06/18.
//

#include <SFML/Graphics/RenderWindow.hpp>

#include "HealthBar.hpp"
#include "Anchor.hpp"
#include "../Utils/style.h"

GUI::HealthBar::HealthBar() : m_container(), m_content()
{
    init();
}

void GUI::HealthBar::init()
{
    m_container.setSize(sf::Vector2f(32, 2));
    m_content.setSize(sf::Vector2f(22, 2));

    m_container.setOrigin(Anchor::Local::getBottomCenter(m_container));

    m_container.setFillColor(Style::CustomColor::GREY);
    m_content.setFillColor(sf::Color::Black);
}

void GUI::HealthBar::setPosition(const sf::Vector2f& pos)
{
    m_container.setPosition(pos);
    m_content.setPosition(Anchor::Global::getTopLeft(m_container));
}

void GUI::HealthBar::draw(sf::RenderWindow& window)
{
    window.draw(m_container);
    window.draw(m_content);
}

void GUI::HealthBar::setHealth(unsigned int max, unsigned int current)
{
    float percentage = (static_cast<float>(current) / max) * 100.f;
    float size = (m_container.getSize().x * percentage) / 100.f;

    m_content.setSize(sf::Vector2f(size, 2));
}