//
// Created by Pierre Roy on 05/04/18.
//

#ifndef BREMERHAVEN_CANVAS_HPP
#define BREMERHAVEN_CANVAS_HPP

#include <SFML/System/Vector2.hpp>

#include "../../Game/Utils/types.h"

namespace GUI
{
    class Canvas
    {
    private:
        sf::Vector2f m_position;
        sf::Vector2f m_dimensions;
        std::vector<Menu> m_items;

        unsigned int m_selected {0};

    public:
        explicit Canvas(sf::Vector2f&& position = {0, 0},
                        sf::Vector2f&& dimensions = {0, 0});

        void draw(sf::RenderWindow& window);
        void update(const sf::Time dt);

        void addItem(Menu);
        Menu* getItem(const std::string& name);

        void moveUp();
        void moveDown();
        void moveRight();
        void moveLeft();

        void navigateTo(const std::string& name);

        const std::string& getCurrentId();
        const Menu& getCurrent() const { return m_items[m_selected]; }
        Menu& getCurrent() { return const_cast<Menu&>(static_cast<const Canvas*>(this)->getCurrent()); }
    };

}

#endif //BREMERHAVEN_CANVAS_HPP
