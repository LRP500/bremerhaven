//
// Created by Pierre Roy on 06/04/18.
//

#include <SFML/Graphics/RenderTarget.hpp>

#include "TextItem.hpp"
#include "../Utils/style.h"

GUI::TextItem::TextItem()
        : m_isLocked {false},
          m_isHighlighted {false},
          m_isSelected {false}
{}

void GUI::TextItem::draw(sf::RenderTarget& window)
{
    if (m_isLocked)
    {
        m_text.setFillColor(Style::CustomColor::LIGHT_GREY);
    }
    else if (m_isHighlighted)
    {
        m_text.setFillColor(Style::CustomColor::ORANGE);
    }
    else if (m_isSelected)
    {
        m_text.setFillColor(Style::CustomColor::YELLOW);
    }
    else
    {
        m_text.setFillColor(sf::Color::White);
    }
    window.draw(m_text);
}