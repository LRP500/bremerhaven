//
// Created by Pierre Roy on 29/05/18.
//

#ifndef BREMERHAVEN_ANCHOR_HPP
#define BREMERHAVEN_ANCHOR_HPP

#include <SFML/System/Vector2.hpp>

namespace Anchor
{
    // namespace Local
    namespace Local
    {
        template <class T>
        sf::Vector2f getTopLeft(const T& object)
        {
            const sf::FloatRect bounds{ object.getLocalBounds() };
            return{ bounds.left, bounds.top };
        }

        template <class T>
        sf::Vector2f getTopCenter(const T& object)
        {
            const sf::FloatRect bounds{ object.getLocalBounds() };
            return{ bounds.left + bounds.width / 2.f, bounds.top };
        }

        template <class T>
        sf::Vector2f getTopRight(const T& object)
        {
            const sf::FloatRect bounds{ object.getLocalBounds() };
            return{ bounds.left + bounds.width, bounds.top };
        }

        template <class T>
        sf::Vector2f getCenterLeft(const T& object)
        {
            const sf::FloatRect bounds{ object.getLocalBounds() };
            return{ bounds.left, bounds.top + bounds.height / 2.f };
        }

        template <class T>
        sf::Vector2f getCenter(const T& object)
        {
            const sf::FloatRect bounds{ object.getLocalBounds() };
            return{ bounds.left + bounds.width / 2.f, bounds.top + bounds.height / 2.f };
        }

        template <class T>
        sf::Vector2f getCenterRight(const T& object)
        {
            const sf::FloatRect bounds{ object.getLocalBounds() };
            return{ bounds.left + bounds.width, bounds.top + bounds.height / 2.f };
        }

        template <class T>
        sf::Vector2f getBottomLeft(const T& object)
        {
            const sf::FloatRect bounds{ object.getLocalBounds() };
            return{ bounds.left, bounds.top + bounds.height };
        }

        template <class T>
        sf::Vector2f getBottomCenter(const T& object)
        {
            const sf::FloatRect bounds{ object.getLocalBounds() };
            return{ bounds.left + bounds.width / 2.f, bounds.top + bounds.height };
        }

        template <class T>
        sf::Vector2f getBottomRight(const T& object)
        {
            const sf::FloatRect bounds{ object.getLocalBounds() };
            return{ bounds.left + bounds.width, bounds.top + bounds.height };
        }

    }

    // namespace Global
    namespace Global
    {
        template <class T>
        sf::Vector2f getTopLeft(const T& object)
        {
            const sf::FloatRect bounds{ object.getGlobalBounds() };
            return{ bounds.left, bounds.top };
        }

        template <class T>
        sf::Vector2f getTopCenter(const T& object)
        {
            const sf::FloatRect bounds{ object.getGlobalBounds() };
            return{ bounds.left + bounds.width / 2.f, bounds.top };
        }

        template <class T>
        sf::Vector2f getTopRight(const T& object)
        {
            const sf::FloatRect bounds{ object.getGlobalBounds() };
            return{ bounds.left + bounds.width, bounds.top };
        }

        template <class T>
        sf::Vector2f getCenterLeft(const T& object)
        {
            const sf::FloatRect bounds{ object.getGlobalBounds() };
            return{ bounds.left, bounds.top + bounds.height / 2.f };
        }

        template <class T>
        sf::Vector2f getCenter(const T& object)
        {
            const sf::FloatRect bounds{ object.getGlobalBounds() };
            return{ bounds.left + bounds.width / 2.f, bounds.top + bounds.height / 2.f };
        }

        template <class T>
        sf::Vector2f getCenterRight(const T& object)
        {
            const sf::FloatRect bounds{ object.getGlobalBounds() };
            return{ bounds.left + bounds.width, bounds.top + bounds.height / 2.f };
        }

        template <class T>
        sf::Vector2f getBottomLeft(const T& object)
        {
            const sf::FloatRect bounds{ object.getGlobalBounds() };
            return{ bounds.left, bounds.top + bounds.height };
        }

        template <class T>
        sf::Vector2f getBottomCenter(const T& object)
        {
            const sf::FloatRect bounds{ object.getGlobalBounds() };
            return{ bounds.left + bounds.width / 2.f, bounds.top + bounds.height };
        }

        template <class T>
        sf::Vector2f getBottomRight(const T& object)
        {
            const sf::FloatRect bounds{ object.getGlobalBounds() };
            return{ bounds.left + bounds.width, bounds.top + bounds.height };
        }

    }
}

#endif //BREMERHAVEN_ANCHOR_HPP
