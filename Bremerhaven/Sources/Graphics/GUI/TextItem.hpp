//
// Created by Pierre Roy on 06/04/18.
//

#ifndef BREMERHAVEN_MENUITEM_HPP
#define BREMERHAVEN_MENUITEM_HPP

#include <SFML/Graphics/Text.hpp>

#include "../../Game/Utils/types.h"

namespace GUI
{
    class TextItem
    {
    private:
        sf::Text m_text;
        bool m_isLocked;
        bool m_isHighlighted;
        bool m_isSelected;

    public:
        TextItem();

        void draw(sf::RenderTarget& window);

        void highlight(bool state) { m_isHighlighted = state; }
        void select(bool state) { m_isSelected = state; }

        void lock() { m_isLocked = true; }
        void unlock() { m_isLocked = false; }

        bool isLocked() { return m_isLocked; }

        sf::Text* getText() { return &m_text; }
    };
}

#endif //BREMERHAVEN_MENUITEM_HPP
