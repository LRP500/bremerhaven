//
// Created by Pierre Roy on 30/03/18.
//

#ifndef BASEENGINE_TILEMAPLOADER_HPP
#define BASEENGINE_TILEMAPLOADER_HPP

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/VertexArray.hpp>

#include "RawMap.hpp"
#include "RoomData.hpp"
#include "../../Game/Utils/types.h"

// Tile Map Wrapper
namespace MapSystem
{
    class TileMap : public RawMap, public sf::Drawable, public sf::Transformable
    {
    private:
        std::vector<Tile> m_tiles; // drawable tiles
        std::vector<Door> m_doors; // drawable doors
        std::vector<DrawableItem> m_items; // consumable items

        // Drawable
        sf::VertexArray m_vertices;

        // FOV
        std::vector<sf::VertexArray> m_segments;
        std::vector<sf::Vector2f> m_corners;

        // store texture and set every tile with it
        // tile will then specify its own rect
        std::shared_ptr<const sf::Texture> m_texture;

        // map dimensions
        sf::Vector2u m_windowSize;

    public:
        explicit TileMap(const sf::Vector2u& size);

        void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
        void update(sf::Time);

        void generate(unsigned int level) override;
        void generateKeys(const TextureHolder&, EntitySystem::Atlas*);
        void initSpecialRooms(EntitySystem::Atlas*, const TextureHolder&);
        void loadTiles();

        void setTexture(std::shared_ptr<const sf::Texture>);
        const sf::Texture& getTexture() { return *m_texture; }

        void drawItems(sf::RenderWindow&);
        void drawDoors(sf::RenderWindow&window);
        void removeDrop(DrawableItem &);
        void dropItem(DrawableItem);
        void openDoor(const Door&);

        // Accessors
        const std::vector<DrawableItem>& getItems() const { return m_items; }
        const std::vector<Tile>& getTiles() const { return m_tiles; }
        const Tile& getTile(int x, int y) const;
        const Tile& getTile(const sf::Vector2f&) const;
        const Tile& getEntrance() const;
        const Tile& getExit() const;
        std::vector<Door>::const_iterator getDoor(const sf::Vector2f&) const;
        const std::vector<MapSystem::Door>& getDoors() const { return m_doors; }
        const RoomData* findRoom(const sf::Vector2f&); // return room containing given position

        // FOV
        void findCorners();
        void findSegments();
        const std::vector<sf::VertexArray>& getSegments() const { return m_segments; }
        const std::vector<sf::Vector2f>& getCorners() const { return m_corners; }
        std::bitset<8> getAdjacentWalls(unsigned int x, unsigned int y) const;

        // Screen/Tile transformations
        sf::Vector2f getScreenPadding() const;
        sf::Vector2f gridToWorldPos(int, int) const;
        sf::Vector2f gridToWorldPos(const sf::Vector2f&) const;
        const Tile& getTileFromWorldPos(const sf::Vector2f&) const;

//    public:
//        // Proxy for overload[][]
//        // allow direct tile access with bracket syntax i.e map[4][6]
//        class Proxy {
//        private:
//            Tile* _array;
//        public:
//            explicit Proxy(Tile* array) : _array(array) {}
//            Tile* operator[](int index) { return _array + index; }
//        };
//        Proxy operator[](int index) { return Proxy(&m_tiles[index * getConfig().width]); }
    };

    bool contains(RoomData*, const sf::Vector2f&);
}

#include "Tile.hpp"

#endif //BASEENGINE_TILEMAPLOADER_HPP
