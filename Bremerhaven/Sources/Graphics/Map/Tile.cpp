//
// Created by Pierre Roy on 31/03/18.
//

#include <iostream>
#include "Tile.hpp"

MapSystem::Tile::Tile(TileType type) : m_type(type)
{
    setType(type);
    setScale(TextureScale, TextureScale);
}

void MapSystem::Tile::setType(MapSystem::TileType type)
{
    m_type = type;
    setTextureRect(TextureRects.at(m_type));
}

void MapSystem::Tile::setTextureRect(const sf::IntRect& rectangle)
{
    if (rectangle != m_textureRect)
    {
        m_textureRect = rectangle;
    }
}

sf::FloatRect MapSystem::Tile::getLocalBounds() const
{
    float width = static_cast<float>(std::abs(m_textureRect.width));
    float height = static_cast<float>(std::abs(m_textureRect.height));

    return sf::FloatRect(0.f, 0.f, width, height);
}

sf::FloatRect MapSystem::Tile::getGlobalBounds() const
{
    return getTransform().transformRect(getLocalBounds());
}