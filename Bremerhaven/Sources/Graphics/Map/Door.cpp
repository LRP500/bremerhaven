//
// Created by Pierre Roy on 28/04/18.
//

#include <SFML/Graphics/RenderWindow.hpp>

#include "Door.hpp"
#include "Tile.hpp"
#include "TileMap.hpp"
#include "../GUI/Anchor.hpp"
#include "../Utils/style.h"
#include "../../System/Logger/Logger.hpp"

using MapSystem::Door;

Door::Door(SharedDoor data, const sf::Texture& mapTexture, const sf::Vector2f& pos)
        : m_data(std::move(data))
{
    // init transform
    m_sprite.setTexture(mapTexture);

    // set texture depending on room types
    if ((m_data->rooms.first->type == RoomType::Armory) ||
        (m_data->rooms.second && m_data->rooms.second->type == RoomType::Armory))
        m_sprite.setTextureRect(MapSystem::TextureRects.at(MapSystem::TileType::ClosedDoorMilitary));
    else if ((m_data->rooms.first->type == RoomType::Infirmary) ||
             (m_data->rooms.second && m_data->rooms.second->type == RoomType::Infirmary))
        m_sprite.setTextureRect(MapSystem::TextureRects.at(MapSystem::TileType::ClosedDoorMedical));
    else m_sprite.setTextureRect(MapSystem::TextureRects.at(MapSystem::TileType::ClosedDoor));

    m_sprite.setScale(MapSystem::TextureScale, MapSystem::TextureScale);
    m_sprite.setPosition(pos);
    m_sprite.setOrigin(m_sprite.getLocalBounds().width / 2,
                       m_sprite.getLocalBounds().height / 2);

    // init collider
    m_collider.setSize(sf::Vector2f(m_sprite.getGlobalBounds().width - 2.f,
                                    m_sprite.getGlobalBounds().height - 2.f));
    m_collider.setOrigin(Anchor::Local::getCenter(m_collider));
    m_collider.setPosition(Anchor::Global::getCenter(m_sprite));
}

void MapSystem::Door::draw(sf::RenderWindow& window)
{
    if (m_data->closed) window.draw(m_sprite);
    window.draw(m_collider);
}

void MapSystem::Door::open() const
{
    m_data->closed = false;
}

void MapSystem::Door::setPosition(const sf::Vector2f &pos)
{
    m_sprite.setPosition(pos);
}

sf::Sprite &MapSystem::Door::getSprite()
{
    return const_cast<sf::Sprite&>(static_cast<const Door*>(this)->getSprite());
}
