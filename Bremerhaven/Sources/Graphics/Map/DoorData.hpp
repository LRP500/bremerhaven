//
// Created by Pierre Roy on 27/04/18.
//

#ifndef BREMERHAVEN_EXIT_HPP
#define BREMERHAVEN_EXIT_HPP

#include "MapSystem.hpp"

#include "../../Game/Utils/types.h"

namespace MapSystem
{
    enum class DoorType
    {
        Default,
        Exit,
        Armory,
        Infirmary
    };

    // Door data struct definition
    struct DoorData
    {
        DoorData(int x, int y, Orientation o)
                : x(x),
                  y(y),
                  closed(false),
                  type(DoorType::Default),
                  orientation(o) {}
        int x;
        int y;
        bool closed;
        DoorType type;
        Orientation orientation;
        std::pair<SharedRoom, SharedRoom> rooms;
    };
}

#endif //BREMERHAVEN_EXIT_HPP
