//
// Created by Pierre Roy on 22/03/18.
//

#include "RawMap.hpp"
#include "RoomData.hpp"
#include "Pathfinder.hpp"
#include "../../Game/Utils/Random.h"
#include "../../System/Logger/Logger.hpp"

using namespace Utils::Random;

MapSystem::RawMap::RawMap()
{
    m_tiles = std::vector<TileAscii>(Config.width * Config.height, TileAscii::Unused);
    m_rooms.reserve(RoomReserve);
}

MapSystem::TileAscii MapSystem::RawMap::getTileAscii(int x, int y) const
{
    if (x < 0 || y < 0 || x >= Config.width || y >= Config.height)
        return TileAscii::Unused;
    return m_tiles[x + y * Config.width];
}

void MapSystem::RawMap::setTileAscii(int x, int y, TileAscii tile)
{
    m_tiles[x + y * Config.width] = tile;
}

bool MapSystem::RawMap::createFeature()
{
    for (int i = 0; i < 1000; ++i)
    {
        if (m_slots.empty()) return false;

        // choose a random side of a random room or corridor
        int r = randomInt(static_cast<int>(m_slots.size()));
        int x = randomInt(m_slots[r].x, m_slots[r].x + m_slots[r].width - 1);
        int y = randomInt(m_slots[r].y, m_slots[r].y + m_slots[r].height - 1);

        // north, south, west, east
        for (int j = 0; j < Direction::Count; ++j)
        {
            if (!createFeature(x, y, static_cast<Direction>(j))) continue;
            m_slots.erase(m_slots.begin() + r);
            return true;
        }
    }
    return false;
}

bool MapSystem::RawMap::createFeature(int x, int y, Direction dir)
{
    static const int roomChance = 50; // corridorChance = 100 - roomChance

    int dx = 0;
    int dy = 0;

    switch (dir)
    {
        case North: dy = 1; break;
        case South: dy = -1; break;
        case West: dx = 1; break;
        case East: dx = -1; break;
        case Count: break;
    }

    if (getTileAscii(x + dx, y + dy) != TileAscii::Floor
        && getTileAscii(x + dx, y + dy) != TileAscii::Corridor)
        return false;

    if (randomInt(100) < roomChance)
    {
        if (makeRoom(x, y, dir))
        {
            setTileAscii(x, y, TileAscii::ClosedDoor);
            m_doors.emplace_back(std::make_shared<DoorData>(x, y, dy ? Horizontal : Vertical));
            return true;
        }
    }
    else
    {
        if (Config.hasCorridors)
        {
            if (makeCorridor(x, y, dir))
            {
                auto ascii = (getTileAscii(x + dx, y + dy) == TileAscii::Floor) ?
                             TileAscii::ClosedDoor : TileAscii::Corridor;
                setTileAscii(x, y, ascii);
                return true;
            }
        }
    }
    return false;
}

bool MapSystem::RawMap::makeRoom(int x, int y, Direction dir, bool firstRoom)
{
    SharedRoom room = std::make_shared<RoomData>();

    room->width = randomInt(Config.minRoomSize, Config.maxRoomSize);
    room->height = randomInt(Config.minRoomSize, Config.maxRoomSize);
    room->type = RoomType::Empty;

    if (dir == North)
    {
        room->x = x - room->width / 2;
        room->y = y - room->height;
    }
    else if (dir == South)
    {
        room->x = x - room->width / 2;
        room->y = y + 1;
    }
    else if (dir == West)
    {
        room->x = x - room->width;
        room->y = y - room->height / 2;
    }
    else if (dir == East)
    {
        room->x = x + 1;
        room->y = y - room->height / 2;
    }

    if (placeRoom(*room, TileAscii::Floor))
    {
        m_rooms.emplace_back(room);
        if (dir != South || firstRoom) // north side
            m_slots.emplace_back(RoomData(room->x, room->y - 1, room->width, 1));
        if (dir != North || firstRoom) // south side
            m_slots.emplace_back(RoomData(room->x, room->y + room->height, room->width, 1));
        if (dir != East || firstRoom) // west side
            m_slots.emplace_back(RoomData(room->x - 1, room->y, 1, room->height));
        if (dir != West || firstRoom) // east side
            m_slots.emplace_back(RoomData(room->x + room->width, room->y, 1, room->height));
        return true;
    }

    return false;
}

bool MapSystem::RawMap::makeCorridor(int x, int y, Direction dir)
{
    RoomData corridor(x, y, 0, 0);

    if (randomBool()) // horizontal corridor
    {
        corridor.width = randomInt(Config.minCorridorLength, Config.maxCorridorLength);
        corridor.height = 1;
        if (dir == North)
        {
            corridor.y = y - 1;
            if (randomBool())corridor.x = x - corridor.width + 1;
        }
        else if (dir == South)
        {
            corridor.y = y + 1;
            if (randomBool()) corridor.x = x - corridor.width + 1;
        }
        else if (dir == West) corridor.x = x - corridor.width;
        else if (dir == East) corridor.x = x + 1;
    }
    else // vertical corridor
    {
        corridor.width = 1;
        corridor.height = randomInt(Config.minCorridorLength, Config.maxCorridorLength);

        if (dir == North) corridor.y = y - corridor.height;
        else if (dir == South) corridor.y = y + 1;
        else if (dir == West)
        {
            corridor.x = x - 1;
            if (randomBool()) corridor.y = y - corridor.height + 1;
        }
        else if (dir == East)
        {
            corridor.x = x + 1;
            if (randomBool()) corridor.y = y - corridor.height + 1;
        }
    }

    if (placeRoom(corridor, TileAscii::Corridor))
    {
        if (dir != South && corridor.width != 1) // north side
            m_slots.emplace_back(RoomData(corridor.x, corridor.y - 1, corridor.width, 1));
        if (dir != North && corridor.width != 1) // south side
            m_slots.emplace_back(RoomData(corridor.x, corridor.y + corridor.height, corridor.width, 1));
        if (dir != East && corridor.height != 1) // west side
            m_slots.emplace_back(RoomData(corridor.x - 1, corridor.y, 1, corridor.height));
        if (dir != West && corridor.height != 1) // east side
            m_slots.emplace_back(RoomData(corridor.x + corridor.width, corridor.y, 1, corridor.height));
        return true;
    }
    return false;
}

bool MapSystem::RawMap::placeRoom(const RoomData& room, TileAscii tile)
{
    if (room.x < 1 || room.y < 1
        || room.x + room.width > Config.width - 1
        || room.y + room.height > Config.height - 1)
        return false;

    for (int y = room.y; y < room.y + room.height; ++y)
    {
        for (int x = room.x; x < room.x + room.width; ++x)
        {
            if (getTileAscii(x, y) != TileAscii::Unused) return false;
        }
    }

    for (int y = room.y - 1; y < room.y + room.height + 1; ++y)
    {
        for (int x = room.x - 1; x < room.x + room.width + 1; ++x)
        {
            if (x == room.x - 1 ||
                y == room.y - 1 ||
                x == room.x + room.width ||
                y == room.y + room.height)
                setTileAscii(x, y, TileAscii::Wall);
            else
                setTileAscii(x, y, tile);
        }
    }

    return true;
}

sf::Vector2f MapSystem::RawMap::placeObject(TileAscii ascii, bool reachable)
{
    if (m_rooms.empty()) return {}; // no room to place object

    // find a random tile (not touching any wall) on a random room
    int r = randomInt(static_cast<int>(m_rooms.size()));
    int x = randomInt(m_rooms[r]->x + 1, m_rooms[r]->x + m_rooms[r]->width - 2);
    int y = randomInt(m_rooms[r]->y + 1, m_rooms[r]->y + m_rooms[r]->height - 2);

    if (reachable && !getSharedRoom(sf::Vector2f(x, y))->reachable)
        return sf::Vector2f();

    // invalid tile if not floor or already in use
    if ((getTileAscii(x, y) != TileAscii::Floor &&
        getTileAscii(x, y) != TileAscii::Unused) ||
        m_rooms[r]->type != RoomType::Empty)
        return {};

    // place object
    setTileAscii(x, y, ascii);

    // update room type
    if (ascii == TileAscii::UpStairs) m_rooms[r]->type = RoomType::Entrance;
    else if (ascii == TileAscii::DownStairs) m_rooms[r]->type = RoomType::Exit;

    return sf::Vector2f(x, y);
}

sf::Vector2f MapSystem::RawMap::placeEnemy()
{
    if (m_rooms.empty()) return {}; // no room to place enemy

    // find a random tile (not touching any wall) on a random room
    int r = randomInt(static_cast<int>(m_rooms.size()));
    int x = randomInt(m_rooms[r]->x + 1, m_rooms[r]->x + m_rooms[r]->width - 2);
    int y = randomInt(m_rooms[r]->y + 1, m_rooms[r]->y + m_rooms[r]->height - 2);

    // invalid tile if not floor or already in use
    if ((getTileAscii(x, y) != TileAscii::Floor &&
         getTileAscii(x, y) != TileAscii::Unused) ||
        m_rooms[r]->type != RoomType::Empty)
        return {};

    setTileAscii(x, y, TileAscii::Creature);

    return sf::Vector2f(x, y);
}

void MapSystem::RawMap::generate(unsigned int level)
{
    MapSystem::Pathfinder kylo(*this); // a boi gotta find his way

    m_isvalid = false;
    while (!m_isvalid)
    {
        // place first room on center
        while (!makeRoom(Config.width / 2, Config.height / 2, static_cast<Direction>(randomInt(4), true)));

        // calculate density and create all rooms
        unsigned int density = calculateDensity(level);
        for (int i = 1; i < density; ++i) if (!createFeature()) break;

        // place entrance and exit tiles
        while (placeObject(TileAscii::UpStairs) == sf::Vector2f());
        while (placeObject(TileAscii::DownStairs) == sf::Vector2f());

        assignDoors(); // create doors and lock exit
        generateSpecialRooms(); // place armory and infirmary

        // run reachability test and regenerate if no path possible
        kylo.reachabilityBFS();
        if (std::count_if(m_rooms.cbegin(), m_rooms.cend(),
                          [&](const SharedRoom &room) { return room->reachable; }) < 4)
        {
            Logger::Write(Logger::INFO, "impossible map! Regenerating...");
            m_tiles = std::vector<TileAscii>(Config.width * Config.height, TileAscii::Unused);
            m_rooms.clear();
            m_slots.clear();
            m_doors.clear();
            continue;
        }

        // place keys
        while (placeObject(TileAscii::Key, true) == sf::Vector2f(0, 0));
        if (getRoom(RoomType::Armory))
        { while (placeObject(TileAscii::KeyArmory, true) == sf::Vector2f()); }
        if (getRoom(RoomType::Infirmary))
        { while (placeObject(TileAscii::KeyInfirmary, true) == sf::Vector2f()); }

        m_isvalid = true;
    }

    polish(); // last patches
}

void MapSystem::RawMap::assignDoors()
{
    SharedRoom first = nullptr, second = nullptr;

    for (auto&& door : m_doors)
    {
        if (door->orientation == MapSystem::Horizontal)
        {
            first = getSharedRoom(sf::Vector2f(door->x, door->y - 1));
            second = getSharedRoom(sf::Vector2f(door->x, door->y + 1));
        }
        else
        {
            first = getSharedRoom(sf::Vector2f(door->x - 1, door->y));
            second = getSharedRoom(sf::Vector2f(door->x + 1, door->y));
        }

        // assign doors to room
        first->doors.emplace_back(door);
        second->doors.emplace_back(door);

        // close door if leads to exit
        if (first->type == RoomType::Exit || second->type == RoomType::Exit)
            door->closed = true;

        // assign door type depending on room types
        if (first->type == RoomType::Exit || second->type == RoomType::Exit)
            door->type = DoorType::Exit;

        // assign rooms to door
        door->rooms.first = std::move(first);
        door->rooms.second = std::move(second);
    }
}

void MapSystem::RawMap::print() const
{
    for (int y = 0; y < Config.height; ++y)
    {
        for (int x = 0; x < Config.width; ++x)
            std::cout << as_character(getTileAscii(x, y));
        std::cout << std::endl;
    }
    std::cout << "[armory]: " << (getRoom(RoomType::Armory) ? "true" : "false") << std::endl;
    std::cout << "[infirmary]: " << (getRoom(RoomType::Infirmary) ? "true" : "false") << std::endl;
}

void MapSystem::RawMap::polish()
{
    for (auto&& tile : m_tiles)
    {
        if (tile == TileAscii::Unused)
        {
            tile = TileAscii::Floor;
        }
        else if (tile == TileAscii::Floor || tile == TileAscii::Corridor)
        {
            tile = TileAscii::Unused;
        }
    }
}

unsigned int MapSystem::RawMap::calculateDensity(unsigned int level)
{
    // scale density to level
    unsigned int density = Config.density;
    for (int i = 0; i < level; ++i)
    {
        density += (density * DensityLevelIncrement / 100);
    }

    // randomize
    if (m_isDensityFluctuating)
    {
        unsigned int margin = (density * DensityFluctuationRatio / 100);
        int randomized = randomInt(density - margin, density + margin);
        density = (randomized >= Config.minDensity) ? randomized : density;
    }

    return density;
}

SharedRoom MapSystem::RawMap::getSharedRoom(const sf::Vector2f& pos) const
{
    auto room = std::find_if(
            m_rooms.cbegin(), m_rooms.cend(),
            [&](const SharedRoom& room)
            {
                return (pos.x >= room->x && pos.y >= room->y) &&
                       (pos.x < room->x + room->width &&
                        pos.y < room->y + room->height);
            });
    return (room == m_rooms.cend()) ? nullptr : *room;
}

SharedDoor MapSystem::RawMap::getSharedDoor(const sf::Vector2f& pos) const
{
    auto door = std::find_if(m_doors.cbegin(), m_doors.cend(),
                             [&](const SharedDoor& door)
                             { return (door->x == pos.x && door->y == pos.y); });
    return (door == m_doors.cend()) ? nullptr : *door;
}

const std::vector<MapSystem::RoomData*> MapSystem::RawMap::getDeadEnds() const
{
    std::vector<MapSystem::RoomData*> deadEnds;

    for (auto& room : m_rooms)
    {
        if (room->doors.size() == 1 && !room->doors.front()->closed &&
            room->type != RoomType::Exit && room->type != RoomType::Entrance)
        {
            deadEnds.emplace_back(room.get());
        }
    }
    return deadEnds;
}

void MapSystem::RawMap::generateSpecialRooms()
{
    // allow special rooms to be genereated in random order
    typedef std::pair<RoomType, DoorType> Room;
    static std::vector<std::pair<RoomType, DoorType>> attributes;
    attributes.reserve(2);
    attributes.emplace_back(Room(RoomType::Armory, DoorType::Armory));
    attributes.emplace_back(Room(RoomType::Infirmary, DoorType::Infirmary));

    // order of generation
    int order = randomInt(2);

    auto deadEnds = getDeadEnds();
    int rand = randomInt(100);
    if (rand <= SpecialRoomAppearanceChance && !deadEnds.empty())
    {
        auto room = deadEnds[randomInt(static_cast<int>(deadEnds.size()))];
        room->doors.front()->type = attributes[order].second; // specialize door
        room->doors.front()->closed = true; // close door
        room->type = attributes[order].first; // specialize room
    }

    // next room
    order = (!order) ? 1 : 0;

    deadEnds = getDeadEnds();
    rand = randomInt(100);
    if (rand <= SpecialRoomAppearanceChance && !deadEnds.empty())
    {
        auto room = deadEnds[randomInt(static_cast<int>(deadEnds.size()))];
        room->doors.front()->type = attributes[order].second;
        room->doors.front()->closed = true;
        room->type = attributes[order].first;
    }
}

MapSystem::RoomData* MapSystem::RawMap::getRoom(RoomType type) const
{
    auto room = std::find_if(
            m_rooms.cbegin(),
            m_rooms.cend(),
            [&](const SharedRoom& room)
            { return (room->type == type); });
    return (room == m_rooms.cend()) ? nullptr : (*room).get();
}

sf::Vector2f MapSystem::RawMap::getKeyPos(MapSystem::RoomType type) const
{
    for (int i = 0; i < getConfig().height; ++i)
    {
        for (int j = 0; j < getConfig().width; ++j)
        {
            auto tile = getTileAscii(j, i);
            if (type == RoomType::Exit && tile == TileAscii::Key)
                return sf::Vector2f(j, i);
            else if (type == RoomType::Armory && tile == TileAscii::KeyArmory)
                return sf::Vector2f(j, i);
            else if (type == RoomType::Infirmary && tile == TileAscii::KeyInfirmary)
                return sf::Vector2f(j, i);
        }
    }
    return sf::Vector2f();
}

sf::Vector2f MapSystem::RawMap::getTilePos(TileAscii type) const
{
    for (int i = 0; i < getConfig().height; ++i)
    {
        for (int j = 0; j < getConfig().width; ++j)
        {
            auto tile = getTileAscii(j, i);
            if (tile == type) return sf::Vector2f(j, i);
        }
    }
    return sf::Vector2f();
}
