//
// Created by Pierre Roy on 30/03/18.
//

#include <set>

#include <SFML/Graphics/RenderWindow.hpp>

#include "TileMap.hpp"
#include "RoomData.hpp"
#include "../Map/Door.hpp"
#include "../Drawable/DrawableItem.hpp"
#include "../Resources/ResourceHolder.hpp"
#include "../../Game/Utils/Random.h"
#include "../../Game/Atlas/Atlas.hpp"
#include "../../Game/Item/Weapon/Weapon.hpp"
#include "../../Graphics/GUI/Anchor.hpp"
#include "../../Game/Item/Items/Medkit.hpp"
#include "../../Game/Item/Armor/Armor.hpp"

using namespace MapSystem;

// overload sf::Vector2f operator < to allow STL sort
namespace sf
{
    bool operator<(sf::Vector2f lhs, sf::Vector2f rhs)
    {
        return (lhs.x != rhs.x) ? (lhs.x < rhs.x) : (lhs.y < rhs.y);
    }
}

TileMap::TileMap(const sf::Vector2u& size)
        : m_tiles(), m_doors(), m_items(),
          m_vertices(),
          m_segments(), m_corners(),
          m_texture(nullptr),
          m_windowSize(size)
{
    m_tiles.reserve(getConfig().width * getConfig().height);
}

void TileMap::loadTiles()
{
    m_vertices.setPrimitiveType(sf::Quads);
    m_vertices.resize(getConfig().width * getConfig().height * 4);

    for (unsigned int h = 0; h < getConfig().height; ++h)
    {
        for (unsigned int w = 0; w < getConfig().width; ++w)
        {
            // get the current tile
            auto tile = getTile(w, h);

            if (tile.getType() == TileType::Wall) continue;

            // get a pointer to the current tile's quad
            sf::Vertex* quad = &m_vertices[(w + h * getConfig().width) * 4];

            // define its 4 corners
            const sf::FloatRect& gb = tile.getGlobalBounds();
            quad[0].position = sf::Vector2f(gb.left, gb.top);
            quad[1].position = sf::Vector2f(gb.left + gb.width, gb.top);
            quad[2].position = sf::Vector2f(gb.left + gb.width, gb.top + gb.height);
            quad[3].position = sf::Vector2f(gb.left, gb.top + gb.height);

            // define its 4 texture coordinates
            auto rect = TextureRects.at(tile.getType());
            quad[0].texCoords = sf::Vector2f(rect.left, rect.top);
            quad[1].texCoords = sf::Vector2f(rect.left, rect.top + rect.height);
            quad[2].texCoords = sf::Vector2f(rect.left + rect.width, rect.top);
            quad[3].texCoords = sf::Vector2f(rect.left + rect.width, rect.top + rect.height);
        }
    }
}

void TileMap::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    // apply the transform
    states.transform *= getTransform();

    // apply the tileset texture
    states.texture = m_texture.get();

    // draw the vertex array
    target.draw(m_vertices, states);
}

void TileMap::drawItems(sf::RenderWindow &window)
{
    for (auto& item : m_items) { item.draw(window); }
}

void TileMap::drawDoors(sf::RenderWindow &window)
{
    for (auto& door : m_doors) { door.draw(window); }
}

void TileMap::update(const sf::Time dt)
{}

void TileMap::generate(unsigned int level)
{
    RawMap::generate(level);

    sf::Vector2f pos(m_windowSize.x / 2 - ((getConfig().width * TileSize) / 2),
                     m_windowSize.y / 2 - ((getConfig().height * TileSize) / 2));
    Tile tile;
    for (int i = 0; i < getConfig().height; ++i)
    {
        for (int j = 0; j < getConfig().width; ++j)
        {
            TileAscii ascii = getTileAscii(j, i);
            switch (ascii)
            {
                case TileAscii::ClosedDoor:
                {
                    auto data = getSharedDoor(sf::Vector2f(j, i));
                    auto doorPos = gridToWorldPos(data->x, data->y) + (sf::Vector2f(TileSize, TileSize) / 2.f);
                    m_doors.emplace_back(MapSystem::Door(data, getTexture(), doorPos));
                    tile.setType(TileType::Floor);
                    break;
                }
                case TileAscii::Unused: tile.setType(TileType::Floor); break;
                case TileAscii::UpStairs: tile.setType(TileType::Stairs); break;
                case TileAscii::DownStairs: tile.setType(TileType::Stairs); break;
                case TileAscii::Wall: tile.setType(TileType::Wall); break;
                case TileAscii::Floor: tile.setType(TileType::Wall); break;
                default:break;
            }
            tile.setPosition(pos); // set tile window pos
            m_tiles.emplace_back(tile); // add to tiles list
            pos.x += TileSize; // adjust row position
        }
        pos.x = m_windowSize.x / 2 - ((getConfig().width * TileSize) / 2); // reset to initial pos
        pos.y += TileSize; // adjust column position
    }

    // store informations for FOV
    findCorners();
    findSegments();

    // load vertices to display
    loadTiles();
}

void TileMap::generateKeys(const TextureHolder& textures, EntitySystem::Atlas *atlas)
{
    if (!isValid()) return;

    // generate exit key
    sf::Vector2f pos = getKeyPos(RoomType::Exit);
    m_items.emplace_back(DrawableItem(
            atlas->getEntity<Item>("item_key"),
            textures.get("item_key"),
            TileSize, gridToWorldPos(pos.x, pos.y)));

    // generate infirmary key if necessary
    if ((pos = getKeyPos(RoomType::Armory)) != sf::Vector2f())
    {
        m_items.emplace_back(DrawableItem(
                atlas->getEntity<Item>("item_key_armory"),
                textures.get("item_key_armory"),
                TileSize, gridToWorldPos(pos.x, pos.y)));
    }

    // generate armory key if necessary
    if ((pos = getKeyPos(RoomType::Infirmary)) != sf::Vector2f())
    {
        m_items.emplace_back(DrawableItem(
                atlas->getEntity<Item>("item_key_infirmary"),
                textures.get("item_key_infirmary"),
                TileSize, gridToWorldPos(pos.x, pos.y)));
    }
}

void TileMap::removeDrop(DrawableItem &item)
{
    // erase-remove idiom
    m_items.erase(std::remove(m_items.begin(), m_items.end(), item), m_items.end());
}

void TileMap::setTexture(std::shared_ptr<const sf::Texture> texture)
{
    m_texture = std::move(texture);
}

const Tile& TileMap::getTile(int x, int y) const
{
    // return specificaly created static empty tile if out of bound
    if (x < 0 || y < 0 || x >= getConfig().width || y >= getConfig().height)
    {
        static Tile undefined;
        return undefined;
    }
    return m_tiles[x + y * getConfig().width];
}

const Tile& TileMap::getTile(const sf::Vector2f &pos) const
{
    // return specificaly created static empty tile if out of bound
    if (pos.x < 0 || pos.y < 0 || pos.x >= getConfig().width || pos.y >= getConfig().height)
    {
        static Tile undefined;
        return undefined;
    }
    return m_tiles[pos.x + pos.y * getConfig().width];
}

const Tile& TileMap::getTileFromWorldPos(const sf::Vector2f& pos) const
{
    sf::Vector2f realPos(pos - getScreenPadding());
    auto posx = (realPos.x - (static_cast<int>(realPos.x) % TileSize)) / TileSize;
    auto posy = (realPos.y - (static_cast<int>(realPos.y) % TileSize)) / TileSize;
    return getTile(posx, posy);
}

sf::Vector2f TileMap::getScreenPadding() const
{
    sf::Vector2f screenSize(m_windowSize.x, m_windowSize.y);
    sf::Vector2f mapSize(getConfig().width * MapSystem::TileSize, getConfig().height * MapSystem::TileSize);
    return sf::Vector2f((screenSize - mapSize) / 2.f);
}

const Tile& TileMap::getEntrance() const
{
    auto entrance = getRoom(RoomType::Entrance);
    return getTile(entrance->x, entrance->y);
}

const Tile& TileMap::getExit() const
{
    auto exit = getRoom(RoomType::Exit);
    return getTile(exit->x, exit->y);
}

std::bitset<8> TileMap::getAdjacentWalls(unsigned int x, unsigned int y) const
{
    std::bitset<8> walls;

    const sf::Vector2f corners[8] = {
            sf::Vector2f(x - 1, y - 1),
            sf::Vector2f(x, y - 1),
            sf::Vector2f(x + 1, y - 1),
            sf::Vector2f(x + 1, y),
            sf::Vector2f(x + 1, y + 1),
            sf::Vector2f(x, y + 1),
            sf::Vector2f(x - 1, y + 1),
            sf::Vector2f(x - 1, y),
    };

    for (unsigned int i = 0; i < 8; ++i)
    {
        TileType type = getTile(corners[i]).getType();
        walls.set(i, type == TileType::Wall);
    }

    return walls;
}

void TileMap::findCorners()
{
    // bitset values for valid corner
    const std::string valid[5] = {"010", "000", "101", "011", "110"}; // 011 and 110 for corner doors

    for (unsigned int y = 0; y < getConfig().height; ++y)
    {
        for (unsigned int x = 0; x < getConfig().width; ++x)
        {
            Tile tile = getTile(x, y);

            if (tile.getType() == TileType::Floor || tile.getType() == TileType::Stairs)
                continue;

            sf::Vector2f origin = tile.getPosition(); // get tile origin (top left)
            std::bitset<8> walls = getAdjacentWalls(x, y); // get bitset of all adjacent tile state

            std::string walls_str = walls.to_string();
            std::reverse(walls_str.begin(), walls_str.end()); // reverse bitset string

            // what's happening here:
            // get adjacent walls sub-bitset of size 3 (width, height, diagonal) for each single corner
            // check if result value contained in valid values
            // if valid then add to vector of valid corners to be raycasted to
            // PS: a drawing really helps understand

            // top left
            std::string corner = walls_str[7] + std::string(walls_str, 0, 2);
            if (std::find(std::begin(valid), std::end(valid), corner) != std::end(valid))
            {
                if (x == 0 && y == 0) break;
                m_corners.emplace_back(sf::Vector2f(origin.x, origin.y));
            }

            // top right
            corner = std::string(walls_str, 1, 3);
            if (std::find(std::begin(valid), std::end(valid), corner) != std::end(valid))
            {
                if (x == getConfig().width - 1 && y == 0) break;
                m_corners.emplace_back(sf::Vector2f(origin.x + TileSize, origin.y));
            }

            // bottom right
            corner = std::string(walls_str, 3, 3);
            if (std::find(std::begin(valid), std::end(valid), corner) != std::end(valid))
            {
                if (x == getConfig().width - 1 && y == getConfig().height - 1) break;
                m_corners.emplace_back(sf::Vector2f(origin.x + TileSize, origin.y + TileSize));
            }

            // bottom left
            corner = std::string(walls_str, 5, 3);
            if (std::find(std::begin(valid), std::end(valid), corner) != std::end(valid))
            {
                if (x == 0 && y == getConfig().height - 1) break;
                m_corners.emplace_back(sf::Vector2f(origin.x, origin.y + TileSize));
            }
        }
    }

    for (const auto& door : m_doors)
    {
        if (!door.getData()->closed) continue;
        auto gbounds = door.getSprite().getGlobalBounds();
        m_corners.emplace_back(sf::Vector2f(gbounds.left, gbounds.top));
        m_corners.emplace_back(sf::Vector2f(gbounds.left + gbounds.width, gbounds.top));
        m_corners.emplace_back(sf::Vector2f(gbounds.left, gbounds.top + gbounds.height));
        m_corners.emplace_back(sf::Vector2f(gbounds.left + gbounds.width, gbounds.top + gbounds.height));
    }

    // convert to set and dump back to vector to erase duplicates
    std::set<sf::Vector2f> s(m_corners.begin(), m_corners.end());
    m_corners.assign(s.begin(), s.end());
}

void TileMap::findSegments()
{
    sf::VertexArray seg(sf::LinesStrip, 2);

    m_segments.clear();
    for (auto&& room : getRooms())
    {
        // top wall
        sf::Vector2f end(room->x + room->width, room->y);
        auto door = std::find_if(room->doors.begin(), room->doors.end(),
                                 [&](const SharedDoor& d) { return d->y == room->y - 1; });
        if (door != room->doors.end() && !door->get()->closed) end = sf::Vector2f(door->get()->x, room->y);
        seg[0].position = gridToWorldPos(room->x, room->y);
        seg[1].position = gridToWorldPos(end.x, end.y);
        m_segments.emplace_back(seg);
        if (end != sf::Vector2f(room->x + room->width, room->y))
        {
            seg[0].position = gridToWorldPos(end.x + 1, room->y);
            seg[1].position = gridToWorldPos(room->x + room->width, end.y);
            m_segments.emplace_back(seg);
        }

        // right wall
        end = sf::Vector2f(room->x + room->width, room->y + room->height);
        door = std::find_if(room->doors.begin(), room->doors.end(),
                            [&](const SharedDoor& d) { return d->x == room->x + room->width; });
        if (door != room->doors.end() &&
            !door->get()->closed) end = sf::Vector2f(room->x + room->width, door->get()->y);
        seg[0].position = gridToWorldPos(room->x + room->width, room->y);
        seg[1].position = gridToWorldPos(end.x, end.y);
        m_segments.emplace_back(seg);
        if (end != sf::Vector2f(room->x + room->width, room->y + room->height))
        {
            seg[0].position = gridToWorldPos(room->x + room->width, end.y + 1);
            seg[1].position = gridToWorldPos(room->x + room->width, room->y + room->height);
            m_segments.emplace_back(seg);
        }

        // bottom wall
        end = sf::Vector2f(room->x, room->y + room->height);
        door = std::find_if(room->doors.begin(), room->doors.end(),
                            [&](const SharedDoor& d) { return d->y == room->y + room->height; });
        if (door != room->doors.end() &&
            !door->get()->closed) end = sf::Vector2f(door->get()->x + 1, room->y + room->height);
        seg[0].position = gridToWorldPos(room->x + room->width, room->y + room->height);
        seg[1].position = gridToWorldPos(end.x, end.y);
        m_segments.emplace_back(seg);
        if (end != sf::Vector2f(room->x, room->y + room->height))
        {
            seg[0].position = gridToWorldPos(end.x - 1, room->y + room->height);
            seg[1].position = gridToWorldPos(room->x, room->y + room->height);
            m_segments.emplace_back(seg);
        }

        // left wall
        end = sf::Vector2f(room->x, room->y);
        door = std::find_if(room->doors.begin(), room->doors.end(),
                            [&](const SharedDoor& d) { return d->x == room->x - 1; });
        if (door != room->doors.end() && !door->get()->closed) end = sf::Vector2f(room->x, door->get()->y + 1);
        seg[0].position = gridToWorldPos(room->x, room->y + room->height);
        seg[1].position = gridToWorldPos(end.x, end.y);
        m_segments.emplace_back(seg);
        if (end != sf::Vector2f(room->x, room->y))
        {
            seg[0].position = gridToWorldPos(end.x, end.y - 1);
            seg[1].position = gridToWorldPos(room->x, room->y);
            m_segments.emplace_back(seg);
        }

        // door edges
        for (const auto& door : room->doors)
        {
            sf::Vector2f topLeft(gridToWorldPos(door->x, door->y));
            sf::Vector2f topRight(topLeft.x + TileSize, topLeft.y);
            sf::Vector2f bottomLeft(topLeft.x, topLeft.y + TileSize);

            seg[0].position = topLeft;
            seg[1].position = (door->orientation == Orientation::Horizontal) ? bottomLeft : topRight;
            m_segments.emplace_back(seg);
            seg[0].position = sf::Vector2f(topLeft.x + TileSize, topLeft.y + TileSize);
            seg[1].position = (door->orientation == Orientation::Horizontal) ? topRight : bottomLeft;
            m_segments.emplace_back(seg);
        }
    }
}

sf::Vector2f TileMap::gridToWorldPos(int x, int y) const
{
    return {(x * TileSize) + getScreenPadding().x, (y * TileSize) + getScreenPadding().y};
}

sf::Vector2f TileMap::gridToWorldPos(const sf::Vector2f& pos) const
{
    return gridToWorldPos(pos.x, pos.y);
}

void TileMap::openDoor(const Door& door)
{
    // process door collision
    if (door.getData()->closed)
    {
        door.open();
        findSegments(); // update segments for FOV raycast
    }
}

std::vector<Door>::const_iterator TileMap::getDoor(const sf::Vector2f& pos) const
{
    // find door
    auto door = std::find_if(
            m_doors.cbegin(), m_doors.cend(),
            [&](const Door& door)
            {
                return (Anchor::Global::getTopLeft(door.getSprite()) == pos);
            });

    // return door or nullptr if nothing found
    return door;
}

bool MapSystem::contains(RoomData* room, const sf::Vector2f& pos)
{
    return ((pos.x > room->x && pos.y > room->y) &&
            (pos.x < room->x + room->width && pos.y < room->y + room->height));
}

const MapSystem::RoomData* MapSystem::TileMap::findRoom(const sf::Vector2f& pos)
{
    const auto& rooms = getRooms();
    auto it = std::find_if(rooms.cbegin(), rooms.cend(), [&](const SharedRoom& room)
    {
        return MapSystem::contains(room.get(), pos);
    });
    return (it == rooms.cend()) ? nullptr : rooms[std::distance(rooms.cbegin(), it)].get();
}

void TileMap::initSpecialRooms(EntitySystem::Atlas* atlas, const TextureHolder& textures)
{
    using namespace Utils::Random;

    // init armory
    auto armory = getRoom(RoomType::Armory);
    if (armory)
    {
        // roll random military item
        auto id = atlas->getRandomPicker().rollArmory();

        // generate rolled item
        if (id.substr(0, std::string("weapon").size()) == "weapon")
        {
            // pop weapon in the "middle" of the room
            int x = armory->x + ((armory->width - (armory->width % 2)) / 2);
            int y = armory->y + ((armory->height - (armory->height % 2)) / 2);

            // will crash if texture is missing
            m_items.emplace_back(DrawableItem(atlas->getEntity<Weapon>(id),
                    textures.get(id), TileSize, gridToWorldPos(x, y)));
        }

        // generate armor
//        if (randomInt(100) >= 50)
        {
            int x = randomInt(armory->x + 1, armory->x + armory->width - 2);
            int y = randomInt(armory->y + 1, armory->y + armory->height - 2);

            // will crash if texture is missing
            m_items.emplace_back(DrawableItem(
                    atlas->getEntity<Armor>("armor_default"),
                    textures.get("armor_default"),
                    TileSize, gridToWorldPos(x, y)));
        }

    }

    // init infirmary
    auto infirmary = getRoom(RoomType::Infirmary);
    if (infirmary)
    {
        // generate medkit
//        if (randomInt(100) >= 50)
        {
            auto infirmary = getRoom(RoomType::Infirmary);
            int x = randomInt(infirmary->x + 1, infirmary->x + infirmary->width - 2);
            int y = randomInt(infirmary->y + 1, infirmary->y + infirmary->height - 2);

            // will crash if texture is missing
            m_items.emplace_back(DrawableItem(
                    atlas->getEntity<Medkit>("medkit_large"),
                    textures.get("item_medecine_medkit"),
                    TileSize, gridToWorldPos(x, y)));
        }
    }
}

void TileMap::dropItem(DrawableItem item)
{
    m_items.emplace_back(std::move(item));
}
