//
// Created by Pierre Roy on 02/05/18.
//

#ifndef BREMERHAVEN_PATHFINDER_HPP
#define BREMERHAVEN_PATHFINDER_HPP

#include <vector>
#include "../../Game/Utils/types.h"

namespace MapSystem
{
    typedef std::vector<SharedRoom> RoomRefVector;

    class Pathfinder
    {
    private:
        RoomRefVector m_visited;
        const RawMap& m_map;

    public:
        explicit Pathfinder(const RawMap& map);

        void reachabilityBFS();
    };
}

#endif //BREMERHAVEN_PATHFINDER_HPP
