//
// Created by Pierre Roy on 22/03/18.
//

#ifndef MAPGENERATION_RANDOMLYGENERATEDMAPSYSTEM_HPP
#define MAPGENERATION_RANDOMLYGENERATEDMAPSYSTEM_HPP

#include <random>

namespace MapSystem
{
    const unsigned int TextureSize = 320; // sprite format size = tilesize * zoom * 2
    const unsigned int TileSize = 32;  // texture size in pixels
    const float TextureScale = (float)TileSize / (float)TextureSize; // scaling ratio

    static const int ErrorLimit = 100;
    static const int RoomReserve = 100;

    enum Orientation { Horizontal, Vertical };
    enum Direction { North, South, West, East, Count };

    template <typename Enumeration>
    auto as_character(Enumeration const value) -> typename std::underlying_type<Enumeration>::type
    {
        return static_cast<typename std::underlying_type<Enumeration>::type>(value);
    }
};

#endif //MAPGENERATION_RANDOMLYGENERATEDMAPSYSTEM_HPP
