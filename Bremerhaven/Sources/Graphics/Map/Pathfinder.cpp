//
// Created by Pierre Roy on 02/05/18.
//

#include <list>

#include "Pathfinder.hpp"
#include "RoomData.hpp"
#include "RawMap.hpp"

MapSystem::Pathfinder::Pathfinder(const RawMap& map)
        : m_map(map)
{
    m_visited.reserve(m_map.getRooms().size());
}

void MapSystem::Pathfinder::reachabilityBFS()
{
    auto entrance = m_map.getRoom(RoomType::Entrance);
    auto start = m_map.getSharedRoom(sf::Vector2f(entrance->x, entrance->y));

    std::list<SharedRoom> graph;

    graph.emplace_back(start);
    while (!graph.empty())
    {
        auto current = graph.front(); // get current
        current->reachable = true;
        graph.pop_front();

        if (std::find(m_visited.begin(), m_visited.end(), current) != m_visited.end())
            continue ;

        m_visited.emplace_back(current);

        for (auto&& door : current->doors)
        {
            if (door->closed) continue;
            graph.emplace_back(door->rooms.first == current ?
                               door->rooms.second : door->rooms.first);
        }
    }
}