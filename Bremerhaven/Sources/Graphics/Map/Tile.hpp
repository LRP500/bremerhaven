//
// Created by Pierre Roy on 31/03/18.
//

#ifndef BASEENGINE_TILE_HPP
#define BASEENGINE_TILE_HPP

#include <unordered_map>

#include <SFML/Graphics/Transformable.hpp>

#include "../../Graphics/Map/MapSystem.hpp"
#include "../../Game/Utils/types.h"

namespace MapSystem
{
    enum class TileType
    {
        Unused = 0,
        Wall,
        Floor,
        Stairs,
        ClosedDoor,
        ClosedDoorMedical,
        ClosedDoorMilitary,
        Count
    };

    // definition of texture rects within texture
    const std::unordered_map<TileType, sf::IntRect> TextureRects = {
            { TileType::Unused, sf::IntRect(TextureSize * 2, 0, TextureSize, TextureSize) },
            { TileType::Wall, sf::IntRect(0, 0, TextureSize, TextureSize) },
            { TileType::Floor, sf::IntRect(TextureSize, 0, TextureSize, TextureSize) },
            { TileType::Stairs, sf::IntRect(TextureSize * 2, 0, TextureSize, TextureSize) },
            { TileType::ClosedDoor, sf::IntRect(0, TextureSize, TextureSize, TextureSize) },
            { TileType::ClosedDoorMedical, sf::IntRect(TextureSize, TextureSize, TextureSize, TextureSize) },
            { TileType::ClosedDoorMilitary, sf::IntRect(TextureSize * 2, TextureSize, TextureSize, TextureSize) },
    };

    // Tile Definition
    // Inherit from sf::Transformable to keep track of position, size and origin
    // Drawn by TileMap through vertices
    class Tile : public sf::Transformable
    {
    private:
        sf::IntRect m_textureRect;
        TileType m_type;

    public:
        explicit Tile(TileType type = TileType::Unused);

        void setTextureRect(const sf::IntRect& rectangle);

        sf::FloatRect getLocalBounds() const;
        sf::FloatRect getGlobalBounds() const;

        const TileType& getType() const { return m_type; }
        void setType(TileType type);
    };
}

#endif //BASEENGINE_TILE_HPP
