//
// Created by Pierre Roy on 22/03/18.
//

#ifndef MAPGENERATION_ROOM_HPP
#define MAPGENERATION_ROOM_HPP

#include "../../Game/Utils/types.h"

namespace MapSystem
{
    enum class RoomType
    {
        Empty,
        Entrance,
        Exit,
        Armory,
        Infirmary
    };

    struct RoomData
    {
        explicit RoomData()
                : x(0),
                  y(0),
                  width(0),
                  height(0),
                  reachable(false),
                  type(RoomType::Empty) {}
        RoomData(int x, int y, int w, int h, bool r = false,
                RoomType t = RoomType::Empty)
                : x(x),
                  y(y),
                  width(w),
                  height(h),
                  reachable(r),
                  type(t) {}
        int x;
        int y;
        int width;
        int height;
        bool reachable;
        RoomType type;
        std::vector<SharedDoor> doors;
    };

    typedef std::shared_ptr<RoomData> SharedRoom;
}

#include "../Map/DoorData.hpp"

#endif //MAPGENERATION_ROOM_HPP
