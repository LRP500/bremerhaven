//
// Created by Pierre Roy on 22/03/18.
//

#ifndef MAPGENERATION_MAP_HPP
#define MAPGENERATION_MAP_HPP

#include <SFML/System/Vector2.hpp>

#include "Tile.hpp"
#include "RoomData.hpp"
#include "MapSystem.hpp"
#include "../../Game/Utils/types.h"

namespace MapSystem
{
    static const unsigned DensityFluctuationRatio = 20; // in percentage
    static const unsigned DensityLevelIncrement = 20; // in percentage
    static const unsigned SpecialRoomAppearanceChance = 100;

    enum class TileAscii : char
    {
        Unused		 = ' ',
        Floor		 = '.',
        Corridor	 = ',',
        Wall		 = '#',
        ClosedDoor	 = '+',
        UpStairs	 = '<',
        DownStairs	 = '>',
        Creature     = 'c',
        Key          = 'k',
        KeyArmory    = 'a',
        KeyInfirmary = 'i'
    };

    struct Configuration
    {
        Configuration() : minRoomSize(6), maxRoomSize(12),
                          minCorridorLength(1), maxCorridorLength(5),
                          minDensity(4), density(5),
                          width(60), height(32),
                          hasCorridors(false)
        {}
        unsigned int minRoomSize;
        unsigned int maxRoomSize;
        unsigned int minCorridorLength;
        unsigned int maxCorridorLength;
        unsigned int minDensity;
        unsigned int density;
        unsigned int width;
        unsigned int height;
        bool hasCorridors;
    };

    class RawMap
    {
    private:
        std::vector<TileAscii> m_tiles;
        std::vector<RoomData> m_slots; // prospect slots where to generate new rooms
        std::vector<SharedRoom> m_rooms;
        std::vector<SharedDoor> m_doors;

        Configuration Config;

        bool m_isvalid = false; // set to true if map RawMap::generate is successful
        bool m_isDensityFluctuating = true; // allow randomized density

    public:
        RawMap();
        virtual ~RawMap() = default;

        virtual void generate(unsigned int level);
        void print() const;
        bool isValid() const { return m_isvalid; }
        sf::Vector2f placeObject(TileAscii, bool reachable = false);
        sf::Vector2f placeEnemy();

        const std::vector<SharedRoom>& getRooms() const { return m_rooms; }
        SharedRoom getSharedRoom(const sf::Vector2f&) const; // for storing
        SharedDoor getSharedDoor(const sf::Vector2f&) const;

        RoomData* getRoom(MapSystem::RoomType) const;
        sf::Vector2f getTilePos(TileAscii) const;
        sf::Vector2f getKeyPos(RoomType) const;

        const Configuration& getConfig() const { return Config; }

        TileAscii getTileAscii(int x, int y) const;
        void setTileAscii(int x, int y, TileAscii);

    private:
        bool createFeature();
        bool createFeature(int x, int y, Direction);
        bool makeRoom(int x, int y, Direction, bool firstRoom = false);
        bool makeCorridor(int x, int y, Direction);
        bool placeRoom(const RoomData &, TileAscii);
        unsigned int calculateDensity(unsigned int level);
        void assignDoors();
        void polish();

        // Special Rooms
        void generateSpecialRooms();
        // get pointers on dead ends for special room generation
        const std::vector<MapSystem::RoomData*> getDeadEnds() const;
    };
}

#include "DoorData.hpp"

#endif //MAPGENERATION_MAP_HPP
