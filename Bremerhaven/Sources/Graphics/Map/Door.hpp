//
// Created by Pierre Roy on 28/04/18.
//

#ifndef BREMERHAVEN_DOOR_HPP
#define BREMERHAVEN_DOOR_HPP

#include <SFML/Graphics/Sprite.hpp>

#include "../../Game/Collision/RectangleCollider.hpp"
#include "../../Game/Utils/types.h"

namespace MapSystem
{
    // Graphic wrapper for DoorData
    class Door
    {
    private:
        SharedDoor m_data;
        sf::Sprite m_sprite;
        RectangleCollider m_collider;

    public:
        explicit Door(SharedDoor data, const sf::Texture& mapTexture, const sf::Vector2f& pos);

        void draw(sf::RenderWindow& window);

        void open() const;

        const SharedDoor& getData() const { return m_data; }
        const RectangleCollider& getCollider() const { return m_collider; }
        const sf::Sprite& getSprite() const { return m_sprite; };
        sf::Sprite& getSprite();

        void setPosition(const sf::Vector2f& pos);
    };
}

#endif //BREMERHAVEN_DOOR_HPP
