//
// Created by Pierre Roy on 30/03/18.
//

#ifndef BASEENGINE_CONFIGURATION_H
#define BASEENGINE_CONFIGURATION_H

#include <SFML/Graphics/Color.hpp>

namespace Style
{
    namespace CustomColor
    {
        const sf::Color LIGHT_GREY(110, 110, 110, 255);
        const sf::Color GREY(95, 95, 95, 255);
        const sf::Color DARK_GREY(47, 47, 47, 255);
        const sf::Color ORANGE(249, 158, 20, 255);
        const sf::Color YELLOW(255, 215, 0, 255);
    }
}

#endif //BASEENGINE_CONFIGURATION_H
