//
// Created by Pierre Roy on 27/03/18.
//

#include <cstdlib>

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/System/Clock.hpp>

#include "AnimatedSprite.hpp"

AnimatedSprite::AnimatedSprite(sf::Time frameTime, bool paused, bool looped)
        : m_animation(nullptr),
          m_frameTime(frameTime), m_currentFrame(0),
          m_color(sf::Color::Transparent),
          m_isPaused(paused), m_isLooped(looped)
{}

void AnimatedSprite::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    if (m_animation && m_animation->getSpriteSheet())
    {
        states.transform *= getTransform();
        states.texture = m_animation->getSpriteSheet().get();
        target.draw(m_vertices, 4, sf::Quads, states);
    }
}

void AnimatedSprite::update(sf::Time dt)
{
    // if not paused and animation is valid
    if (!m_isPaused && m_animation)
    {
        // add delta time
        m_currentTime += dt;

        // if current time is bigger then the frame time advance one frame
        if (m_currentTime >= m_frameTime)
        {
            // reset time, but keep the remainder
            m_currentTime = sf::microseconds(m_currentTime.asMicroseconds() % m_frameTime.asMicroseconds());
            // get next Frame index
            if (m_currentFrame + 1 < m_animation->getSize())
            {
                m_currentFrame++;
            }
            else // animation has ended
            {
                // reset to start
                m_currentFrame = 0;
                if (!m_isLooped)
                {
                    m_isPaused = true;
                }
            }
            // set the current frame, not reseting the time
            setFrame(m_currentFrame, false);
        }
    }
}

void AnimatedSprite::play(Animation* animation, float frameTime)
{
    if (getAnimation() != animation)
    {
        setAnimation(animation);
    }
    setFrameTime(sf::seconds(frameTime));
    play();
}

void AnimatedSprite::stop()
{
    m_isPaused = true;
    m_currentFrame = 0;
    setFrame(m_currentFrame);
}

void AnimatedSprite::setAnimation(Animation* animation)
{
    m_animation = animation;
    m_currentFrame = 0;
    setFrame(m_currentFrame);
}

void AnimatedSprite::setColor(const sf::Color &color)
{
    m_color = color;
    m_vertices[0].color = m_color;
    m_vertices[1].color = m_color;
    m_vertices[2].color = m_color;
    m_vertices[3].color = m_color;
}

void AnimatedSprite::setTransparency(unsigned char alpha)
{
    m_color.a = alpha;
    m_vertices[0].color.a = alpha;
    m_vertices[1].color.a = alpha;
    m_vertices[2].color.a = alpha;
    m_vertices[3].color.a = alpha;
}

sf::FloatRect AnimatedSprite::getLocalBounds() const
{
    sf::IntRect rect = m_animation->getFrame(m_currentFrame);

    auto width = static_cast<float>(std::abs(rect.width));
    auto height = static_cast<float>(std::abs(rect.height));

    return {0.f, 0.f, width, height};
}

sf::FloatRect AnimatedSprite::getGlobalBounds() const
{
    return getTransform().transformRect(getLocalBounds());
}

void AnimatedSprite::setFrame(std::size_t newFrame, bool resetTime)
{
    if (m_animation)
    {
        //calculate new vertex positions and texture coordiantes
        sf::IntRect rect = m_animation->getFrame(newFrame);

        m_vertices[0].position = sf::Vector2f(0.f, 0.f);
        m_vertices[1].position = sf::Vector2f(0.f, static_cast<float>(rect.height));
        m_vertices[2].position = sf::Vector2f(static_cast<float>(rect.width), static_cast<float>(rect.height));
        m_vertices[3].position = sf::Vector2f(static_cast<float>(rect.width), 0.f);

        float left = static_cast<float>(rect.left) + 0.0001f;
        float right = left + static_cast<float>(rect.width);
        float top = static_cast<float>(rect.top);
        float bottom = top + static_cast<float>(rect.height);

        m_vertices[0].texCoords = sf::Vector2f(left, top);
        m_vertices[1].texCoords = sf::Vector2f(left, bottom);
        m_vertices[2].texCoords = sf::Vector2f(right, bottom);
        m_vertices[3].texCoords = sf::Vector2f(right, top);
    }

    if (resetTime)
    {
        m_currentTime = sf::Time::Zero;
    }
}

AnimatedSprite::operator sf::Sprite&()
{
    static sf::Sprite sprite;
    sprite.setTexture(*m_animation->getSpriteSheet());
    sprite.setTextureRect(m_animation->getFrame(m_currentFrame));
    sprite.setOrigin(getOrigin());
    sprite.setPosition(getPosition());
    sprite.setScale(getScale());
    return sprite;
}

void AnimatedSprite::alphaBlink(unsigned char low, unsigned char high, float rate)
{
    static sf::Clock clock;

    if (clock.getElapsedTime().asSeconds() >= rate)
    {
        setTransparency(getColor().a == low ? high : low);
        clock.restart();
    }
}
