//
// Created by Pierre Roy on 27/03/18.
//

#ifndef BASEENGINE_ANIMATION_HPP
#define BASEENGINE_ANIMATION_HPP

#include <SFML/Graphics/Rect.hpp>

#include "../../Game/Utils/types.h"

class Animation
{
private:
    std::vector<sf::IntRect> m_frames; // Vector of Rect defining each frame
    std::shared_ptr<const sf::Texture> m_sheet;

public:
    // CTORS
    Animation();
    explicit Animation(std::shared_ptr<const sf::Texture>);

    void addFrame(sf::IntRect&&);
    void setSpriteSheet(const std::shared_ptr<const sf::Texture>&);
    std::size_t getSize() const;
    std::shared_ptr<const sf::Texture> getSpriteSheet() const;
    const sf::IntRect& getFrame(std::size_t) const;
};


#endif //BASEENGINE_ANIMATION_HPP
