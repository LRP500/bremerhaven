//
// Created by Pierre Roy on 27/03/18.
//

#include "Animation.hpp"

Animation::Animation() : m_sheet(nullptr)
{}

Animation::Animation(std::shared_ptr<const sf::Texture> sheet)
        : m_sheet(std::move(sheet))
{}

void Animation::addFrame(sf::IntRect&& rect)
{
    m_frames.emplace_back(std::move(rect));
}


void Animation::setSpriteSheet(const std::shared_ptr<const sf::Texture>& sheet)
{
    m_sheet = sheet;
}

std::shared_ptr<const sf::Texture> Animation::getSpriteSheet() const
{
    return m_sheet;
}

const sf::IntRect &Animation::getFrame(std::size_t index) const
{
    return m_frames[index];
}

std::size_t Animation::getSize() const
{
    return m_frames.size();
}