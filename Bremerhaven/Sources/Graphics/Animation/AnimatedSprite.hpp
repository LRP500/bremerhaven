//
// Created by Pierre Roy on 27/03/18.
//

#ifndef BASEENGINE_ANIMATEDSPRITE_HPP
#define BASEENGINE_ANIMATEDSPRITE_HPP

#include <SFML/System/Time.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Vertex.hpp>

#include "Animation.hpp"

#include "../../Game/Utils/types.h"

// Inherit from sf::Drawable and sf::Transformable to behave as a regular sf::Sprite
class AnimatedSprite : public sf::Drawable, public sf::Transformable
{
private:
    static constexpr float DefaultFrameTime = 0.1;

private:
    Animation* m_animation; // holds currently played animation
    sf::Vertex m_vertices[4]; // needed for Drawable::draw override
    sf::Time m_frameTime; // time between each frame
    sf::Time m_currentTime; // time elapsed since last frame
    std::size_t m_currentFrame; // currently played frame

    sf::Color m_color;

    bool m_isPaused;
    bool m_isLooped;

public:
    // CTOR
    explicit AnimatedSprite(sf::Time frameTime = sf::seconds(DefaultFrameTime),
                            bool paused = false,
                            bool looped = true);

    // Override sf::Drawable method
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
    void update(sf::Time);

    void setFrame(std::size_t newFrame, bool resetTime = true); // Set next frame to play
    void setAnimation(Animation* animation); // set played animation
    Animation* getAnimation() const { return m_animation; } // get played animation
    sf::Vector2f getSize() const { return sf::Vector2f(getGlobalBounds().width, getGlobalBounds().height); }

    void setColor(const sf::Color& color); // Change all frames color
    const sf::Color& getColor() const { return m_color; }
    void setTransparency(unsigned char alpha);
    void alphaBlink(unsigned char low, unsigned char high, float rate);


    void play() { m_isPaused = false; } // play current animation
    void play(Animation* animation, float frameTime = DefaultFrameTime); // played specified animation and holds it
    void pause() { m_isPaused = true; } // stop but holds frame position
    void stop(); // stop and reset to initial first frame

    void setFrameTime(sf::Time time) { m_frameTime = time; }
    const sf::Time getFrameTime() const { return m_frameTime; }

    sf::FloatRect getLocalBounds() const; // get texture rect bounds
    sf::FloatRect getGlobalBounds() const; // get texture rect world

    bool isLooped() const { return m_isLooped; }
    bool isPlaying() const { return !m_isPaused; }
    void setLooped(bool looped) { m_isLooped = looped; }

public:
    operator sf::Sprite&(); // Conversion operator to sf::Sprite
};


#endif //BASEENGINE_ANIMATEDSPRITE_HPP
