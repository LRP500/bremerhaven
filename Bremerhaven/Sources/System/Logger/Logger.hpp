//
// Created by Pierre Roy on 23/03/18.
//

// MACRO CONFIGURATION
#ifdef ENABLE_LOGGER

#define LOGGER_START(MIN_PRIORITY, FILE) Logger::Start(MIN_PRIORITY, FILE);
#define LOGGER_STOP() Logger::Stop();
#define LOGGER_WRITE(PRIORITY, MESSAGE) Logger::Write(PRIORITY, MESSAGE);

#else

#define LOGGER_START(MIN_PRIORITY, FILE)
#define LOGGER_STOP()
#define LOGGER_WRITE(PRIORITY, MESSAGE)

#endif

// CLASS DEFINITION
#ifndef LOGGER_LOGGER_HPP
#define LOGGER_LOGGER_HPP

#include <iostream>
#include <string>
#include <fstream>

class Logger
{
public:
    enum Priority
    {
        INFO,
        CONFIG,
        DEBUG,
        WARNING,
        ERROR
    };

public:
    // MEMBER METHODS
    // Start/Stop logging. Messages with priority higher than minPriority are written in log
    // Default file parameter writes to standard output
    static void Start(Priority minPriority, const std::string& logFile = "");
    static void Stop();

    static void Write(Priority, const std::string& message);

private:
    // CTOR & DTOR
    // Adheres to the singleton design pattern, hence the private
    // constructor, copy constructor and assignment operator.
    Logger();
    Logger(const Logger&);
    Logger& operator=(const Logger&);

private:
    // MEMBER VARS
    std::ofstream m_fileStream;
    Priority m_minPriority;
    bool m_active;
    static const std::string PRIORITY_NAMES[]; // Describe items in enum Priority

    static Logger instance; // Singleton boi
};

#endif //LOGGER_LOGGER_HPP
