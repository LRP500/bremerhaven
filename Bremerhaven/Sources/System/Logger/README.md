The Logger class presented here allows to keep a running log. It can be written to a file, or be redirected to standard output.
Messages sent to Logger have a priority of type Logger::Priority. There are five priorities to describe the severity of the messages.
The logging level is set by the parameter minPriority of the Logger::Start method.

Reference: `https://wiki.calculquebec.ca/w/C%2B%2B_:_classe_Logger/en`
