//
// Created by Pierre Roy on 23/03/18.
//

#include "Logger.hpp"

// Static Instanciations
const std::string Logger::PRIORITY_NAMES[] = { "INFO", "CONFIG", "DEBUG", "WARNING", "ERROR" };

Logger Logger::instance; // Dat singelton boi

Logger::Logger() : m_active(true)
{}

void Logger::Start(Logger::Priority minPriority, const std::string &logFile)
{
    instance.m_active = true;
    instance.m_minPriority = minPriority;

    if (!logFile.empty())
    {
        instance.m_fileStream.open(logFile.c_str());
    }
}

void Logger::Stop()
{
    instance.m_active = false;

    if (instance.m_fileStream.is_open())
    {
        instance.m_fileStream.close();
    }
}

void Logger::Write(Priority priority, const std::string& message)
{
    if (instance.m_active && priority >= instance.m_minPriority)
    {
        std::ostream& stream = instance.m_fileStream.is_open() ? instance.m_fileStream : std::cout;
        stream  << PRIORITY_NAMES[priority]
                << ": "
                << message
                << std::endl;
    }
}