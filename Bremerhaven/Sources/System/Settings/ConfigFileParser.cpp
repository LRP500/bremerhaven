//
// Created by Pierre Roy on 11/08/2018.
//

#include <iostream>
#include <fstream>
#include <jansson.h>
#include "ConfigFileParser.hpp"
#include "../Logger/Logger.hpp"

System::ConfigFileParser::ConfigFileParser()
        : m_isChanged(false),
          m_data(),
          m_filename(std::string())
{}

bool System::ConfigFileParser::loadFromFile(const std::string& filename)
{
    m_filename = filename;
    return parse();
}

bool System::ConfigFileParser::parse()
{
    json_t *root;

    // open JSON root
    if (!(root = json_load_file(m_filename.c_str(), 0, nullptr)))
    {
        Logger::Write(Logger::ERROR, "Error: Unable to open settings file \"" + m_filename + "\" for reading");
        return false;
    }

    // Window
    json_t *res = json_object_get(root, "windowedResolution");
    m_data.windowedResolution.x = static_cast<unsigned int>(json_integer_value(json_array_get(res, 0)));
    m_data.windowedResolution.y = static_cast<unsigned int>(json_integer_value(json_array_get(res, 1)));
    m_data.windowTitle = json_string_value(json_object_get(root, "windowTitle"));
    m_data.fullscreenMode = json_boolean_value(json_object_get(root, "fullscreenMode"));
    m_data.guiScreenSidePadding = json_integer_value(json_object_get(root, "guiScreenSidePadding"));
    // Graphics
    m_data.verticalSyncEnabled = json_boolean_value(json_object_get(root, "verticalSyncEnabled"));

    // Mouse
    m_data.mouseCursorGrabbed = json_boolean_value(json_object_get(root, "mouseCursorGrabbed"));
    m_data.mouseCursorVisible = json_boolean_value(json_object_get(root, "mouseCursorVisible"));

    // Gameplay
    m_data.zoomLevel = static_cast<float>(json_real_value(json_object_get(root, "zoomLevel")));

    // System
    m_data.automaticSave = json_boolean_value(json_object_get(root, "automaticSave"));
    m_data.framerateLimit = static_cast<unsigned int>(json_real_value(json_object_get(root, "framerateLimit")));

    m_isChanged = false;

    return true;
}