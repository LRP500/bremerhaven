//
// Created by Pierre Roy on 11/08/2018.
//

#ifndef BREMERHAVEN_CONFIGFILEPARSER_HPP
#define BREMERHAVEN_CONFIGFILEPARSER_HPP

#include <string>
#include <unordered_map>

#include <SFML/System/Vector2.hpp>

// no default values
// if config file is not found the game won't start
struct SettingsData
{
    std::string windowTitle;
    float zoomLevel;
    bool automaticSave;
    bool fullscreenMode;
    bool verticalSyncEnabled;
    bool mouseCursorGrabbed;
    bool mouseCursorVisible;
    unsigned int framerateLimit;
    unsigned int guiScreenSidePadding;
    sf::Vector2u windowedResolution;
};

namespace System
{
    class ConfigFileParser
    {
    public:
        ConfigFileParser();

        bool loadFromFile(const std::string&);

        const SettingsData& getData() const { return m_data; }

    private:
        bool parse();

    private:
        bool m_isChanged;
        SettingsData m_data;
        std::string m_filename;
    };
}

#endif //BREMERHAVEN_CONFIGFILEPARSER_HPP
